module mod_eph
  use mod_eph_variables
  use mod_eph_sigma
  use mod_wannier
  use mod_wfint
  use mod_wfutil
  use mod_eph_scroot, only: eph_scroot

  implicit none

  logical :: reduce = .false.

! methods
  contains
    !
    subroutine eph_init
      integer :: iw, ip
      real(8), allocatable :: vpltmp(:,:)

      !call wannier_init
      eph_temp         = input%eph%temp  ! temperature (in K) for occupation
      eph_phfreq_const = input%eph%einstein/h2ev ! constant Einstein phonon frequency
      eph_ephmat_c     = input%eph%coupling/h2ev
      eph_eta          = input%eph%eta/h2ev     ! complex infinitesimal in Lehmann representation

      ! generate frequency grid
      call generate_freqgrid( eph_fset, input%eph%freqgrid%fgrid, input%eph%freqgrid%fconv, input%eph%freqgrid%nomeg, input%eph%freqgrid%freqmax)
      do iw = 1, eph_fset%nomeg
        eph_fset%freqs( iw)= eph_fset%freqs( iw)! - 0.5d0*input%eph%freqgrid%freqmax 
      end do
      
      ! generate G-vectors
      eph_Gset = wf_Gset

      ! source k-grid
      eph_kset_el = wf_kset
      eph_Gkset_el = wf_Gkset

      ! bzint k-grid (reducible)
      call generate_k_vectors( eph_qset, eph_kset_el%bvec, input%eph%ngridq, input%eph%vqloff, reduce, .false.)
      eph_qpset = eph_qset

      ! target k-set (path)
      eph_pset_nvp1d = size( input%eph%target%plot1d%path%pointarray)
      eph_pset_npp1d = input%eph%target%plot1d%path%steps
      allocate( eph_pset_dvp1d( eph_pset_nvp1d))
      allocate( eph_pset_dpp1d( eph_pset_npp1d))
      allocate( vpltmp( 3, eph_pset_npp1d))
      call connect( eph_kset_el%bvec, input%eph%target%plot1d, eph_pset_nvp1d, eph_pset_npp1d, vpltmp, eph_pset_dvp1d, eph_pset_dpp1d)
      call generate_k_vectors( eph_pset, eph_kset_el%bvec, (/1, 1, eph_pset_npp1d/), (/0.d0, 0.d0, 0.d0/), .false.)
      !write(*,*) wf_npp1d, tmp_kset%nkpt
      do ip = 1, eph_pset%nkpt
        eph_pset%vkl( :, ip) = vpltmp( :, ip)
        call r3mv( eph_kset_el%bvec, eph_pset%vkl( :, ip), eph_pset%vkc( :, ip))
      end do

      eph_nst = wf_nwf
      eph_fst = 1       ! in Wannier interpolated states
      eph_lst = eph_nst ! in Wannier interpolated states

      ! phonon k-grid
      if( eph_phfreq_const .le. 1.d-16) then
        call generate_k_vectors( eph_kset_ph, eph_kset_el%bvec, input%phonons%ngridq, (/0.d0, 0.d0, 0.d0/), input%phonons%reduceq, .false.)
        call generate_Gk_vectors( eph_Gkset_ph, eph_kset_ph, eph_Gset, eph_Gkset_el%gkmax)
      end if
      
      return
    end subroutine eph_init

!============================================================================
!============================================================================

    subroutine eph_do
      use m_getunit

      integer :: ip, iq, ipq, ist, is, ia, ias, iw, un, nu, test(3,3,4), test2(9,4), nroots, sdeg, ndeg
      real(8) :: eps, xrange(2), yrange(2), t1
      complex(8) :: sigmafmsc( 10, eph_fst:eph_lst), guess(2)
      character(256) :: fname
      complex(8), allocatable :: dynk(:,:,:), dynq(:,:,:), ev(:,:), roots(:), umnp(:,:,:)

      xrange = (/-3.d0,  3.d0/)
      yrange = (/-5.d0, 1.d0/)
      !xrange = (/-0.5d0, 0.25d0/)
      !xrange = xrange/h2ev
      !yrange = yrange/h2ev! + eph_eta

      ! find VBM and CBM
      !write(*,*) 'find gap'
      !call wfutil_find_bandgap
      !eph_efermi = wfint_evbm
      !eph_efermi = 0.4417235973279391
      eph_efermi = 0.1429505394838780

      ! get eigenvalues on target set
      if( allocated( eph_evalp)) deallocate( eph_evalp)
      allocate( eph_evalp( eph_fst:eph_lst, eph_pset%nkpt))
      allocate( umnp( eph_fst:eph_lst, eph_fst:eph_lst, eph_pset%nkpt))
      call wfint_init( eph_pset)
      eph_evalp = wfint_eval - eph_efermi
      umnp = wfint_transform
      !eph_fset%freqs = eph_fset%freqs*(maxval( eph_evalp) - minval( eph_evalp)) + 0.5*(maxval( eph_evalp) + minval( eph_evalp)) 

      ! get Fermi energy on integration grid
      !call wfint_init( eph_qset)
      !call wfint_interpolate_occupancy
      !eph_phfreq_const = 0.08*eph_efermi

      ! get phonon frequencies on integration grid
      !eph_qset = eph_pset
      if( allocated( eph_phfreq)) deallocate( eph_phfreq)
      allocate( eph_phfreq( 3*natmtot, eph_qset%nkpt))
      if( allocated( eph_phevec)) deallocate( eph_phevec)
      allocate( eph_phevec( 3, natmtot, 3*natmtot, eph_qset%nkpt))
      if( eph_phfreq_const .gt. 1.d-16) then
        eph_phfreq = eph_phfreq_const
        eph_phevec = zzero
      else
        allocate( dynk( 3*natmtot, 3*natmtot, eph_kset_ph%nkpt))
        allocate( dynq( 3*natmtot, 3*natmtot, eph_qset%nkpt))
        allocate( ev( 3*natmtot, 3*natmtot))
        call eph_read_dynmat( dynk)
        call eph_interpolate_dynmat( dynk, eph_qset, dynq)
        do iq = 1, eph_qset%nkpt
          call eph_diag_dynmat( dynq( :, :, iq), eph_phfreq( :, iq), ev)
          do iw = 1, 3*natmtot
            un = 0
            do is = 1, nspecies
              do ia = 1, natoms( is)
                ias = idxas( ia, is)
                do ip = 1, 3
                  un = un + 1
                  eph_phevec( ip, ias, iw, iq) = ev( un, iw)
                end do
              end do
            end do
          end do
          !write(*,'(i12,20f13.6)') iq, eph_phfreq( :, iq)
          !call plotmat( ev)
          !write(*,*)
        end do
        deallocate( dynk, dynq, ev)
        write(*,*) 'CALCULATED PHONON FREQUENCIES'
      end if

      write(*,'("                 Hartree             eV                   K")')
      write(*,'("Temperature:  ",3g20.10e3)') eph_temp*kboltz, eph_temp*kboltz*h2ev, eph_temp
      write(*,'("Phonon freq:  ",3g20.10e3)') eph_phfreq_const, eph_phfreq_const*h2ev, eph_phfreq_const/kboltz
      write(*,'("eph coupling: ",3g20.10e3)') eph_ephmat_c, eph_ephmat_c*h2ev, eph_ephmat_c/kboltz
      write(*,'("eta:          ",3g20.10e3)') eph_eta, eph_eta*h2ev, eph_eta/kboltz
      write(*,'("phonon occ:   ",g20.10e3)') 1.d0/(exp( eph_phfreq_const/(kboltz*eph_temp)) - 1.d0) 
      write(*,*)
      write(*,'(2g20.10e3)') eph_eta - eph_etaeps, eph_eta + eph_etaeps
      write(*,*)

      ! initialize eph matrix-elements
      !! generarte p+q grid
      !call findkptinset( (/0.d0, 0.d0, 0.d0/), eph_pset, is, ip)
      !call generate_kkqmt_vectors( eph_qqpset, eph_Gset, eph_qset%bvec, eph_qset%ngridk, eph_qset%vkloff, eph_qset%isreduced, eph_pset%vkl( :, ip), .false.)
      !do ipq = 1, eph_qqpset%kqmtset%nkpt
      !  eph_qqpset%kqmtset%vkl( :, ipq) = eph_qset%vkl( :, ipq) + eph_pset%vkl( :, ip)
      !  eph_qqpset%kqmtset%vkc( :, ipq) = eph_qset%vkc( :, ipq) + eph_pset%vkc( :, ip)
      !  eph_qqpset%ikqmt2ik( ipq) = ipq
      !end do

      !! get electron energies and Wannier transformation matrices on p+q set
      !if( allocated( eph_evalpq)) deallocate( eph_evalpq)
      !allocate( eph_evalpq( eph_fst:eph_lst, eph_qqpset%kqmtset%nkpt))
      !call wfint_init( eph_qqpset%kqmtset)
      !eph_evalpq = wfint_eval - eph_efermi

      !! generate eph matrix elements
      !if( allocated( eph_ephmat)) deallocate( eph_ephmat)
      !allocate( eph_ephmat( eph_fst:eph_lst, eph_fst:eph_lst, 3*natmtot, eph_qqpset%kqmtset%nkpt))
      !call eph_ephmat_lr( ip, umnp( :, :, ip), eph_ephmat)

      !do nu = 1, 3*natmtot
      !  write( fname, '("EPHMAT_B",i3.3,".DAT")') nu
      !  call getunit( un)
      !  open( un, file=trim( fname), action='write', status='unknown', form='formatted')
      !  do is = eph_fst, eph_lst
      !    do ip = 1, eph_pset%nkpt
      !      sdeg = 0
      !      do ist = eph_fst, eph_lst
      !        if( abs( eph_evalp( ist, ip) - eph_evalp( is, ip)) .lt. 1.d-4) then
      !          if( sdeg .eq. 0) sdeg = ist
      !          ndeg = ist
      !        end if
      !      end do
      !      t1 = sqrt( dble( sum( eph_ephmat( sdeg:ndeg, sdeg:ndeg, nu, ip)* &
      !                            conjg( eph_ephmat( sdeg:ndeg, sdeg:ndeg, nu, ip)))))
      !      write( un,'(f13.6,2g26.16,3f13.6)') eph_pset_dpp1d( ip), t1*h2ev*1.d3, &
      !               fourpi*(t1*h2ev*1.d3*norm2( eph_pset%vkc( :, ip)))**2, &
      !               eph_pset%vkl( :, ip)
      !    end do
      !    write( un, *)
      !    write( un, *)
      !  end do
      !  close( un)
      !end do

      !stop

      ! allocate self energy
      if( allocated( eph_sigmafm)) deallocate( eph_sigmafm)
      allocate( eph_sigmafm( eph_fst:eph_lst, eph_fset%nomeg, eph_pset%nkpt))

      ! loop over target points
      write( fname, '("BAND_T",i3.3,"_E",i3.3,".DAT")'), nint( eph_temp), nint( eph_eta*h2ev*100)
      call getunit( un)
      open( un, file=trim( fname), action='write', status='unknown', form='formatted')
      write( un, '("# all energies in eV")')
      write( un, '("#                  Hartree             eV                   K")')
      write( un, '("# Temperature:  ",3g20.10e3)') eph_temp*kboltz, eph_temp*kboltz*h2ev, eph_temp
      write( un, '("# Phonon freq:  ",3g20.10e3)') eph_phfreq_const, eph_phfreq_const*h2ev, eph_phfreq_const/kboltz
      write( un, '("# eph coupling: ",3g20.10e3)') eph_ephmat_c, eph_ephmat_c*h2ev, eph_ephmat_c/kboltz
      write( un, '("# eta:          ",3g20.10e3)') eph_eta, eph_eta*h2ev, eph_eta/kboltz
      write( un, '("# phonon occ:   ",g20.10e3)') 1.d0/(exp( eph_phfreq_const/(kboltz*eph_temp)) - 1.d0) 
      write( un, '("#           distance              energy         Re(sigmafm)         Im(sigmafm)")')

      t1 = 0.d0
      !do while( t1 .le. 4.d0+1.d-10)
      !  call eph_scroot( xrange, yrange, 3.d-1, 1.d-6, 1.d-10, 1, 1, nroots, roots, &
      !      phfreq=1.d0, etherm=1.d0, ek=t1)
      !  do ip = 1, nroots
      !    write(*,'(f13.6,i,100g26.16)') t1, nroots, roots( ip)
      !  end do
      !  t1 = t1 + 0.05d0
      !end do
      !stop
      do ip = 1, eph_pset%nkpt
        write(*,*) ip
        ! generarte p+q grid
        !call generate_kkqmt_vectors( eph_qqpset, eph_Gset, eph_kset_el%bvec, eph_qset%ngridk, eph_qset%vkloff, eph_qset%isreduced, eph_pset%vkl( :, ip), .false.)
        call eph_set_qpset( ip)

        ! get electron energies and Wannier transformation matrices on p+q set
        if( allocated( eph_evalpq)) deallocate( eph_evalpq)
        allocate( eph_evalpq( eph_fst:eph_lst, eph_qpset%nkpt))
        call wfint_init( eph_qpset)
        eph_evalpq = wfint_eval - eph_efermi

        call eph_gen_eliashbergfun( eph_evalp( :, 1), 1)
        stop

        ! generate eph matrix elements
        if( allocated( eph_ephmat)) deallocate( eph_ephmat)
        allocate( eph_ephmat( eph_fst:eph_lst, eph_fst:eph_lst, 3*natmtot, eph_qqpset%kqmtset%nkpt))
        call eph_ephmat_lr( ip, umnp( :, :, ip), eph_ephmat)

        ! calculate self-energy
        call eph_gen_sigmafm( cmplx( eph_fset%freqs, 0.d0, 8), eph_fset%nomeg, ip, eph_fst, eph_lst, eph_sigmafm( :, :, ip))

        ! calculate spectral function
        call eph_gen_specfun( eph_evalp( :, ip), ip)

        write(*,*) 'start'
        sigmafmsc = zzero
!#ifdef USEOMP
!!$omp parallel default( shared) private( ist, nroots, eps, roots, guess)
!!$omp do
!#endif
        do ist = eph_fst, eph_lst
          !write(*,*)
          write(*,*) ist
          !call eph_gen_sigmafm( cmplx( eph_fset%freqs, 0.d0, 8) + eph_evalp( ist, ip), eph_fset%nomeg, ip, ist, ist, eph_sigmafm( ist, :, ip))
          call eph_gen_sigmafm( (/cmplx( eph_evalp( ist, ip), 0.d0, 8)/), 1, ip, ist, ist, guess(2))
          !call eph_sigmafm2d_tofile( xrange, yrange, 500, 500, ip, ist, 'SIGMA2D.DAT')
          !write(*,'("GUESS: ",2g20.10e3)') guess
          !write(*,'("energy: ",f13.6)') eph_evalp( ist, ip)*h2ev
          write(*,'("guess:     ",2(2f23.16,5x))') guess(2)*h2ev, guess(2)
          iw = 0
          guess(1) = 1.d128
          do while( (abs( guess(1) - eph_evalp( ist, ip) - guess(2)) .gt. 1.d-10) .and. (iw .lt. 1000))
            iw = iw + 1
            guess(1) = eph_evalp( ist, ip) + guess(2)
            call eph_gen_sigmafm( guess(1), 1, ip, ist, ist, guess(2))
            !write(*,'(i,2f23.16,g20.10e3)') iw, guess(2), abs( guess(1) - eph_evalp( ist, ip) - guess(2))
          end do
          !write(*,'("solution:  ",2(2f23.16,5x))') guess(2)*h2ev, guess(2)
          if( iw .lt. 1000) then
            write(*,'("solution:  ",2(2f23.16,5x),"(",g13.6,")",i)') guess(2)*h2ev, guess(2), abs( guess(1) - eph_evalp( ist, ip) - guess(2))*h2ev, iw
            sigmafmsc(1, ist) = guess(2)
          else  
            nroots = 0
            eps = 10.d-3
            do while( nroots .eq. 0)
              !write(*,'(f13.6)') eps
              call eph_scroot( xrange, yrange, eps, 1.d-6, 1.d-10, ip, ist, nroots, roots)
              eps = 0.5*eps
            end do
            nroots = min( 10, nroots)
            sigmafmsc( 1:nroots, ist) = roots( 1:nroots)
            write(*,'("solution:  ",2(2f23.16,5x))') roots(1)*h2ev, roots(1)
          end if
        end do
!#ifdef USEOMP
!!$omp end do
!!$omp end parallel
!#endif
        do ist = eph_fst, eph_lst
          write( un, '(22g20.10e3)') eph_pset_dpp1d( ip), eph_evalp( ist, ip)*h2ev, &
                                                         (sigmafmsc( :, ist) + eph_evalp( ist, ip))*h2ev
        end do
        write( un, *)
      end do
      close( un)

      ! write results
      do ist = eph_fst, eph_lst
        write( fname, '("SIGMA_EPH_",i3.3,".DAT")') ist
        call getunit( un)
        open( un, file=trim( fname), action='write', status='unknown', form='formatted')
        write( un, '("# all energies in eV")')
        write( un, '("#                  Hartree             eV                   K")')
        write( un, '("# Temperature:  ",3g20.10e3)') eph_temp*kboltz, eph_temp*kboltz*h2ev, eph_temp
        write( un, '("# Phonon freq:  ",3g20.10e3)') eph_phfreq_const, eph_phfreq_const*h2ev, eph_phfreq_const/kboltz
        write( un, '("# eph coupling: ",3g20.10e3)') eph_ephmat_c, eph_ephmat_c*h2ev, eph_ephmat_c/kboltz
        write( un, '("# eta:          ",3g20.10e3)') eph_eta, eph_eta*h2ev, eph_eta/kboltz
        write( un, '("# phonon occ:   ",g20.10e3)') 1.d0/(exp( eph_phfreq_const/(kboltz*eph_temp)) - 1.d0) 
        write( un, '("#          frequency         Re(sigmafm)         Im(sigmafm)             specfun")')
        do ip = 1, eph_pset%nkpt
          write( un, '("# ip = ",i3,"  -  vpl = (",3f13.6,")  -  evalp = ",g20.10e3" eV")') ip, eph_pset%vkl( :, ip), eph_evalp( ist, ip)*h2ev
          do iw = 1, eph_fset%nomeg
            write( un, '(3g20.10e3)') eph_fset%freqs( iw)*h2ev, eph_sigmafm( ist, iw, ip)*h2ev
          end do
          write( un, *)
          write( un, *)
        end do
        close( un)
      end do

      write( fname, '("SPFUN_T",i3.3,"_C",i3.3,".DAT")'), nint( eph_temp), nint( eph_ephmat_c*h2ev*100)
      call getunit( un)
      open( un, file=trim( fname), action='write', status='unknown', form='formatted')
      write( un, '("# all energies in eV")')
      write( un, '("#                  Hartree             eV                   K")')
      write( un, '("# Temperature:  ",3g20.10e3)') eph_temp*kboltz, eph_temp*kboltz*h2ev, eph_temp
      write( un, '("# Phonon freq:  ",3g20.10e3)') eph_phfreq_const, eph_phfreq_const*h2ev, eph_phfreq_const/kboltz
      write( un, '("# eph coupling: ",3g20.10e3)') eph_ephmat_c, eph_ephmat_c*h2ev, eph_ephmat_c/kboltz
      write( un, '("# eta:          ",3g20.10e3)') eph_eta, eph_eta*h2ev, eph_eta/kboltz
      write( un, '("# phonon occ:   ",g20.10e3)') 1.d0/(exp( eph_phfreq_const/(kboltz*eph_temp)) - 1.d0) 
      write( un, '("#           distance              energy         Re(sigmafm)         Im(sigmafm)")')
      write( un, '(g20.10e3)', advance='no') 0.d0
      do iw = 1, eph_fset%nomeg
        write( un, '(g20.10e3)', advance='no') eph_fset%freqs( iw)*h2eV
      end do
      write( un, *)
      do ip = 1, eph_pset%nkpt
        write( un, '(g20.10e3)', advance='no') eph_pset_dpp1d( ip)
        do iw = 1, eph_fset%nomeg
          write( un, '(g20.10e3)', advance='no') eph_specfun( iw, ip)/h2ev
        end do
        write( un, *)
      end do
      close( un)

      return
    end subroutine eph_do

!============================================================================
!============================================================================

    subroutine eph_ephmat_lr( ip, umnp, ephmat, firstcall)
      use mod_lattice, only: omega
      integer, intent( in)           :: ip ! point in target set
      complex(8), intent( in)        :: umnp( eph_fst:eph_lst, eph_fst:eph_lst)
      complex(8), intent( out)       :: ephmat( eph_fst:eph_lst, eph_fst:eph_lst, 3*natmtot, eph_qqpset%kqmtset%nkpt)
      logical, optional, intent( in) :: firstcall
      
      complex(8), allocatable, save :: prefac(:,:)
      logical, save                 :: first = .true.
      real(8)                       :: alpha, gqmax

      integer :: ia, is, ias, iq, ipq, nu, g1, g2, g3
      real(8) :: t1, t2, vr(3), vgql(3), vgqc(3), gqegq
      real(8) :: born(3,3,natmtot), epsinf(3,3)
      complex(8) :: z1, ze( 3, natmtot), auxmat( eph_fst:eph_lst, eph_fst:eph_lst)

      alpha = 1.d0
      gqmax = 14.d0
      write(*,'("p-point: ",3f13.6)') eph_pset%vkl( :, ip)

      if( present( firstcall)) first = firstcall

      is = 1
      ia = 1
      ias = idxas( ia, is)
      born( 1, :, ias) = (/ 2.166,  0.000,  0.000/)
      born( 2, :, ias) = (/ 0.000,  2.166,  0.000/)
      born( 3, :, ias) = (/ 0.000,  0.000,  2.166/)
      is = 2
      ia = 1
      ias = idxas( ia, is)
      born( 1, :, ias) = (/-2.166,  0.000,  0.000/)
      born( 2, :, ias) = (/ 0.000, -2.166,  0.000/)
      born( 3, :, ias) = (/ 0.000,  0.000, -2.166/)

      epsinf(1,:) = (/ 9.225,  0.000,  0.000/)
      epsinf(2,:) = (/ 0.000,  9.225,  0.000/)
      epsinf(3,:) = (/ 0.000,  0.000,  9.225/)

      ! initialize the prefactors (p-independent)
      if( first) then
        if( allocated( prefac)) deallocate( prefac)
        allocate( prefac( 3*natmtot, eph_qset%nkpt))
        prefac = zzero
#ifdef USEOMP
!$omp parallel default( shared) private( nu, iq, ze, is, t1, ia, ias, vr, g1, g2, g3, vgql, vgqc, gqegq, z1)
!$omp do collapse( 2)
#endif
        do nu = 1, 3*natmtot
          do iq = 1, eph_qset%nkpt

            ! effective charge times eigenvector times prefactor
            ze = zzero
            if( eph_phfreq( nu, iq) .gt. 1.d-16) then
              do is = 1, nspecies
                t1 = sqrt( 2.d0*spmass( is)*eph_phfreq( nu, iq))
                !t1 = sqrt( 2.d0*spmass( is))
                do ia = 1, natoms( is)
                  ias = idxas( ia, is)
                  call r3mv( born( :, :, ias), dble( eph_phevec( :, ias, nu, iq)), vr)
                  ze( :, ias) = cmplx( vr, 0.d0, 8)
                  call r3mv( born( :, :, ias), aimag( eph_phevec( :, ias, nu, iq)), vr)
                  ze( :, ias) = cmplx( dble( ze( :, ias)), vr, 8)/t1
                end do
              end do
            end if

            do g1 = eph_Gset%intgv(1,1), eph_Gset%intgv(1,2)
              do g2 = eph_Gset%intgv(2,1), eph_Gset%intgv(2,2)
                do g3 = eph_Gset%intgv(3,1), eph_Gset%intgv(3,2)
                  vgql = dble( (/g1, g2, g3/)) + eph_qset%vkl( :, iq)
                  call r3mv( eph_qset%bvec, vgql, vgqc)
                  gqegq = vgqc(1)*(epsinf(1,1)*vgqc(1) + epsinf(1,2)*vgqc(2) + epsinf(1,3)*vgqc(3)) + &
                          vgqc(2)*(epsinf(2,1)*vgqc(1) + epsinf(2,2)*vgqc(2) + epsinf(2,3)*vgqc(3)) + &
                          vgqc(3)*(epsinf(3,1)*vgqc(1) + epsinf(3,2)*vgqc(2) + epsinf(3,3)*vgqc(3))

                  if( (gqegq .gt. 0.d0) .and. (gqegq/4.d0/alpha .lt. gqmax)) then
                    !write(*,'(3i,f26.16)') g1, g2, g3, gqegq
                    t1 = exp( -gqegq/4.d0/alpha)/gqegq
                    do is = 1, nspecies
                      do ia = 1, natoms( is)
                        ias = idxas( ia, is)
                        z1 = vgqc(1)*ze( 1, ias) + vgqc(2)*ze( 2, ias) + vgqc(3)*ze( 3, ias)
                        t2 = -dot_product( vgqc, atposc( :, ia, is))
                        z1 = z1*cmplx( cos( t2), sin( t2), 8)
                        prefac( nu, iq) = prefac( nu, iq) + z1*t1
                      end do
                    end do
                  end if

                end do
              end do
            end do
            !write(*,'(2i,2f26.16)') nu, iq, prefac( nu, iq)

          end do
        end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
        prefac = prefac*fourpi/omega*zi
        first = .false.
        do nu = 1, 3*natmtot
          write(*,'(i,2f26.16)') nu, maxval( dble( prefac( nu, :)), 1), maxval( aimag( prefac( nu, :)), 1)
        end do
      end if

      ! generate long-range coupling matrices
      ephmat = zzero
      do ipq = 1, eph_qqpset%kqmtset%nkpt
        iq = eph_qqpset%ikqmt2ik( ipq)
        call zgemm( 'c', 'n', eph_nst, eph_nst, eph_nst, zone, &
               wfint_transform( :, :, ipq), wf_nwf, &
               umnp, eph_nst, zzero, &
               auxmat, eph_nst)
        do nu = 1, 3*natmtot
          ephmat( :, :, nu, ipq) = prefac( nu, iq)*auxmat
        end do
      end do
      return
    end subroutine eph_ephmat_lr

!============================================================================
!============================================================================

    subroutine eph_gen_eliashbergfun( evalp, ip)
      real(8) :: evalp( eph_fst:eph_lst) ! eigenvalues at target point p (shifted by Fermi energy)
      integer :: ip                      ! index of target point p

      integer :: imode, ist, jst, iw, iq, ipq
      real(8) :: fac, sw, dw, t0, t1, ti, tp, te, ts
      real(8) :: wgtp( 3*natmtot, eph_qset%nkptnr, eph_fset%nomeg)
      real(8) :: wgte( eph_nst, eph_qset%nkptnr, eph_nst), eqp( eph_nst, eph_qset%nkptnr)
      real(8) :: auxmat( eph_fst:eph_lst, 3*natmtot, eph_qset%nkptnr)
      real(8) :: df( eph_fset%nomeg, 6), cf( 3, eph_fset%nomeg), r1

      real(8), allocatable :: xint(:), fint(:,:), cfint(:,:)
      
      real(8), external :: sdelta

      fac = 1.d4
      sw = 1.d-2

      if( .not. allocated( eph_eliashbergfun)) then
        allocate( eph_eliashbergfun( eph_fset%nomeg, eph_fst:eph_lst, eph_pset%nkpt))
        eph_eliashbergfun = 0.d0
      end if

      do iq = 1, eph_qset%nkptnr
      !  ipq = eph_qset%ik2ikp( iq)      ! from non-reduced q-set to reduced q-set
      !  ipq = eph_qqpset%ik2ikqmt( ipq) ! from reduced q-set to reduced q+p-set
        eqp( :, iq) = eph_evalpq( :, iq)
        write(*,'(2(i,3f13.6,5x),100f13.6)') iq, eph_qset%vkl( :, iq), iq, eph_qpset%vkl( :, iq), eqp(:,iq)
      end do

      call timesec( t1)
      call opt_tetra_init( eph_tetra, eph_qset, 2)
      call timesec( t0)
      ti = t0 - t1
      !call opt_tetra_int( eph_tetra, 'delta', eph_qset%nkptnr, 3*natmtot, eph_phfreq, eph_fset%freqs, eph_fset%nomeg, wgtp)
      wgtp = 1.d0
      call timesec( t1)
      tp = t1 - t0
      call opt_tetra_int( eph_tetra, 'delta', eph_qset%nkptnr, eph_nst, eqp, evalp, eph_nst, wgte)
      wgte = wgte!*eph_qset%nkptnr/dble( 3*natmtot)
      call timesec( t0)
      te = t0 - t1

      auxmat = 0.d0
      do iq = 1, eph_qset%nkptnr
        do imode = 1, 3*natmtot
          do ist = eph_fst, eph_lst
            do jst = eph_fst, eph_lst
              auxmat( ist, imode, iq) = auxmat( ist, imode, iq) + 1.d0*wgte( jst-eph_fst+1, iq, ist)
            end do
          end do
        end do
      end do
      do ist = eph_fst, eph_lst
        write(*,'(i,f13.6,g26.16)') ist, evalp( ist), sum( auxmat( ist, 1, :))
      end do
      stop
      
      call dgemm( 't', 't', eph_fset%nomeg, eph_nst, 3*natmtot*eph_qset%nkptnr, 1.d0, &
             wgtp, 3*natmtot*eph_qset%nkptnr, &
             auxmat, eph_nst, 0.d0, &
             eph_eliashbergfun( :, :, ip), eph_fset%nomeg)
        
      call timesec( t1)
      ts = t1 - t0
      write(*,'("initialization:   ",f13.6)') ti
      write(*,'("phonon weights:   ",f13.6)') tp
      write(*,'("electron weights: ",f13.6)') te
      write(*,'("summation:        ",f13.6)') ts

      ist = 3000
      allocate( xint( ist), fint( ist, 6), cfint( 3, ist))
      dw = (eph_fset%freqs( eph_fset%nomeg) - eph_fset%freqs(1))/(ist - 1)
      xint = eph_fset%freqs(1)
      do iw = 2, ist
        xint( iw) = xint( iw-1) + dw
      end do

      !df(:,1) = eph_eliashbergfun( :, 1, ip)/fac
      !call fderiv( -1, eph_fset%nomeg, fac*eph_fset%freqs, df(:,1)**2, df(:,2), cf)
      !write(*,'(g26.16)') df( eph_fset%nomeg, 2)
      !call fderiv(  1, eph_fset%nomeg, fac*eph_fset%freqs, df(:,1), df(:,2), cf)
      !call fderiv( -1, eph_fset%nomeg, fac*eph_fset%freqs, df(:,1), df(:,3), cf)
      !call fderiv( -1, eph_fset%nomeg, fac*eph_fset%freqs, df(:,2)*df(:,3), df(:,4), cf)
      !write(*,'(g26.16)') df( eph_fset%nomeg, 4)
      !call fderiv(  1, eph_fset%nomeg, fac*eph_fset%freqs, df(:,2), df(:,4), cf)
      !call fderiv( -1, eph_fset%nomeg, fac*eph_fset%freqs, df(:,3), df(:,5), cf)
      !call fderiv( -1, eph_fset%nomeg, fac*eph_fset%freqs, df(:,4)*df(:,5), df(:,6), cf)
      !write(*,'(g26.16)') df( eph_fset%nomeg, 6)
      !stop
      !call splineinterp( eph_fset%nomeg, fac*eph_fset%freqs, df(:,1), ist, fac*xint, fint(:,1))
      !call savitzkygolay( ist, fac*xint, fint(:,1), 2, 15, 0, fint(:,2))
      !call savitzkygolay( eph_fset%nomeg, fac*eph_fset%freqs, df(:,1), 2, 3, 0, df(:,2))
      !call fderiv(  -1, eph_fset%nomeg, fac*eph_fset%freqs, df(:,2), df(:,3), cf)
      !write(*,'(g26.16)') df( eph_fset%nomeg, 3)
      !call splineinterp( eph_fset%nomeg, fac*eph_fset%freqs, df(:,2), ist, fac*xint, fint(:,3))
      !call fderiv(  -1, ist, fac*xint, fint(:,3), fint(:,4), cfint)
      !write(*,'(g26.16)') fint( ist, 4)
      !stop
      !call fsmooth( 3, eph_fset%nomeg, 1, df(:,1))
      !call fderiv(  1, ist, fac*xint, fint(:,3), fint(:,4), cfint)
      !call fderiv(  2, eph_fset%nomeg, fac*eph_fset%freqs, df(:,2), df(:,4), cf)
      !call savitzkygolay( ist, fac*xint, fint(:,3), 2, 10, 1, fint(:,5))
      !call savitzkygolay( eph_fset%nomeg, fac*eph_fset%freqs, df(:,2), 2, 3, 1, df(:,3))
      do iw = 1, eph_fset%nomeg
        write(*,'(i,100g26.16)') iw, fac*eph_fset%freqs( iw), eph_eliashbergfun( iw, 1, ip)/fac!, df( iw, 2)
      end do
      !write(*,*)
      !write(*,*)
      !do iw = 1, ist
      !  write(*,'(i,100g26.16)') iw, fac*xint( iw), fint( iw, 1:3)
      !end do
    end subroutine eph_gen_eliashbergfun
!============================================================================
!============================================================================
!============================================================================
!============================================================================

    subroutine eph_gen_specfun( evalp, ip)
      real(8) :: evalp( eph_fst:eph_lst) ! eigenvalues at target point p (shifted by Fermi energy)
      integer :: ip                      ! index of target point p

      integer :: ist, iw
      real(8) :: fnp, w, e

      if( .not. allocated( eph_specfun)) then
        allocate( eph_specfun( eph_fset%nomeg, eph_pset%nkpt))
        eph_specfun = 0.d0
      end if

#ifdef USEOMP
!$omp parallel default( shared) private( ist, fnp, iw, w, e)
!$omp do
#endif
      do iw = 1, eph_fset%nomeg
        do ist = eph_fst, eph_lst
          e = evalp( ist)/(kboltz*eph_temp)
          if( e .gt. 7.d2) then
            fnp = 0.d0
          else
            fnp = 1.d0/(exp( e) + 1.d0)  ! Fermi-Dirac occupation of electron
          end if
          w = eph_fset%freqs( iw)
          eph_specfun( iw, ip) = eph_specfun( iw, ip) + 2.d0/pi*aimag( eph_sigmafm( ist, iw, ip))/ &
                                      (( w - evalp( ist) - dble( eph_sigmafm( ist, iw, ip)))**2 + aimag( eph_sigmafm( ist, iw, ip))**2)
        end do
      end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif

      return
    end subroutine eph_gen_specfun

!============================================================================
!============================================================================

    subroutine eph_read_dynmat( dynk, tsym)
      complex(8), intent( out)      :: dynk( 3*natmtot, 3*natmtot, eph_kset_ph%nkpt)
      logical, optional, intent(in) :: tsym

      logical :: exist, tsym_
      integer :: ik, ik0, is, js, ia, ja, ip, jp, i, j, un
      real(8) :: a, b, w( 3*natmtot)
      complex(8) :: tmp( 3*natmtot, 3*natmtot), ev( 3*natmtot, 3*natmtot)
      character(256) :: fext
      character(256) :: chdummy

      tsym_ = .true.
      if( present( tsym)) tsym_ = tsym

      do ik = 1, eph_kset_ph%nkpt
        i = 0
        do is = 1, nspecies
          do ia = 1, natoms (is)
            do ip = 1, 3
              i = i + 1
              call phfext( ik, is, ia, ip, 0, 1, chdummy, fext, chdummy)
              inquire( file='DYN'//trim(fext), exist=exist)
              if( .not. exist) then
                write(*,*)
                write(*, '("Error (eph_read_dynmat): file not found :")')
                write(*, '(A)') ' DYN' // trim (fext)
                write(*,*)
                stop
              end if
              call getunit( un)
              open( un, file='DYN'//trim(fext), action='read', status='old', form='formatted')
              j = 0
              do js = 1, nspecies
                do ja = 1, natoms (js)
                  do jp = 1, 3
                    j = j + 1
                    read( un, *) a, b
                    dynk( i, j, ik) = cmplx( a, b, 8)
                  end do
                end do
              end do
              close( un)
            end do
          end do
        end do

        ! symmetrise the dynamical matrix
        if( tsym_) Call dynsym( eph_kset_ph%vkl(:, ik), dynk(:, :, ik))
      end do

      ! apply accoustic sumrule
      do ik = 1, eph_kset_ph%nkpt
        if( norm2( eph_kset_ph%vkl( :, ik)) .lt. input%structure%epslat) exit
      end do
      if( ik .gt. eph_kset_ph%nkpt) then
        write(*,*)
        write(*,'("Error (eph_read_dynmat): Gamma point not sampled.")')
        stop
      end if
      tmp = dynk( :, :, ik)
      call hermitize( tmp, 'avg')
      call zhediag( tmp, w, ev)
      do ik = 1, eph_kset_ph%nkpt
        do i = 1, 3*natmtot
          do j = 1, 3*natmtot
            do ip = 1, 3
              dynk( i, j, ik) = dynk( i, j, ik) - w( ip)*ev( i, ip)*conjg( ev( j, ip))
            end do
          end do
        end do
      end do

      return
    end subroutine eph_read_dynmat

!============================================================================
!============================================================================

    subroutine eph_interpolate_dynmat( dynk, qset, dynq)
      use m_wsweight

      complex(8), intent( in)   :: dynk( 3*natmtot, 3*natmtot, eph_kset_ph%nkpt)  ! dynamical matrices on original grid
      type( k_set), intent( in) :: qset                                           ! k-points do interolate on
      complex(8), intent( out)  :: dynq( 3*natmtot, 3*natmtot, qset%nkpt)         ! dynamical matrices on interpolation grid

      integer :: ngridkph(3), i1, i2, i3, r1, r2, r3, ir, ik, iq, n, isym, lspl, iv(3), is, ia, js, ja
      real(8) :: v1(3), v2(3), v3(3), s(3,3), rdotk
      complex(8) :: dynr( 3*natmtot, 3*natmtot, eph_kset_ph%nkptnr), dyns( 3*natmtot, 3*natmtot), zwgt

      ngridkph = eph_kset_ph%ngridk

      ! get dynamical in real-space representation
      dynr = zzero
      do i1 = 0, ngridkph(1) - 1
        v1(1) = dble( i1)/dble( ngridkph(1))
        do i2 = 0, ngridkph(2) - 1
          v1(2) = dble( i2)/dble( ngridkph(2))
          do i3 = 0, ngridkph(3) - 1
            v1(3) = dble( i3)/dble( ngridkph(3))
            ik = eph_kset_ph%ikmap( i1, i2, i3)
            v2 = v1
            call vecfbz( input%structure%epslat, eph_kset_ph%bvec, v2, iv)
            n = 0
            dyns = zzero
            do isym = 1, nsymcrys
              lspl = lsplsymc( isym)
              s = dble( symlat( :, :, lspl))
              call r3mtv( s, eph_kset_ph%vkl(:, ik), v3)
              call vecfbz( input%structure%epslat, eph_kset_ph%bvec, v3, iv)
              if( norm2( v2 - v3) .lt. input%structure%epslat) then
                call dynsymapp( isym, eph_kset_ph%vkl( :, ik), dynk( :, :, ik), dyns)
                n = n + 1
              end If
            end do
            if( n .eq. 0) then
              write(*,*)
              write(*, '("Error( eph_interpolate_dynmat): vector ", 3G18.10)') v1
              write(*, '(" cannot be mapped to reduced q-point set.")')
              stop
            end if
            dyns = dyns/dble( n)
            ir = 0
            do r3 = ngridkph(3)/2 - ngridkph(3) + 1, ngridkph(3)/2
              do r2 = ngridkph(2)/2 - ngridkph(2) + 1, ngridkph(2)/2
                do r1 = ngridkph(1)/2 - ngridkph(1) + 1, ngridkph(1)/2
                  ir = ir + 1
                  rdotk = twopi*dot_product( v1, dble( (/r1, r2, r3/)))
                  dynr( :, :, ir) = dynr( :, :, ir) + cmplx( cos( rdotk), sin( rdotk), 8)*dyns
                end do
              end do
            end do
          end do
        end do
      end do
      dynr = dynr/product( ngridkph)

      ! do interpolation
      dynq = zzero
      do iq = 1, qset%nkpt
        i1 = 1
        do is = 1, nspecies
          do ia = 1, natoms( is)
            i2 = 1
            do js = 1, nspecies
              do ja = 1, natoms( js)
                ir = 0
                do r3 = ngridkph(3)/2 - ngridkph(3) + 1, ngridkph(3)/2
                  do r2 = ngridkph(2)/2 - ngridkph(2) + 1, ngridkph(2)/2
                    do r1 = ngridkph(1)/2 - ngridkph(1) + 1, ngridkph(1)/2
                      ir = ir + 1
                      v1 = dble( (/r1, r2, r3/))
                      v2 = v1 + input%structure%speciesarray(js)%species%atomarray(ja)%atom%coord(:) - &
                                input%structure%speciesarray(is)%species%atomarray(ia)%atom%coord(:)
                      call ws_weight( v1, v2, qset%vkl( :, iq), zwgt)
                      dynq( i1:(i1+2), i2:(i2+2), iq) = dynq( i1:(i1+2), i2:(i2+2), iq) + zwgt*dynr( i1:(i1+2), i2:(i2+2), ir)
                    end do
                  end do
                end do
                i2 = i2 + 3
              end do
            end do
            i1 = i1 + 3
          end do
        end do
      end do

      return        
    end subroutine eph_interpolate_dynmat

!============================================================================
!============================================================================

    subroutine eph_diag_dynmat( dyn, w, ev)
      complex(8), intent( in)  :: dyn( 3*natmtot, 3*natmtot)
      real(8), intent( out)    :: w( 3*natmtot)
      complex(8), intent( out) :: ev( 3*natmtot, 3*natmtot)

      integer :: i, is, ia, ip, j, js, ja, jp
      real(8) :: t1
      complex(8) :: tmp( 3*natmtot, 3*natmtot)

      tmp = zzero
      i = 0
      do is = 1, nspecies
        do ia = 1, natoms( is)
          do ip = 1, 3
            i = i + 1
            j = 0
            do js = 1, nspecies
              if( (spmass( is) .le. 0.d0) .or. (spmass( js) .le. 0.d0)) then
                t1 = 0.d0
              else
                t1 = 1.d0/dsqrt( spmass( is)*spmass( js))
              end if
              do ja = 1, natoms( js)
                do jp = 1, 3
                  j = j + 1
                  tmp( i, j) = 0.5d0*t1*(dyn( i, j) + conjg( dyn( j, i)))
                end do
              end do
            end do
          end do
        end do
      end do
      call zhediag( tmp, w, ev)
      do i = 1, 3*natmtot
        if( w(i) .ge. 0.d0) then
          w(i) = dsqrt( w(i))
        else
          w(i) = -dsqrt( abs( w(i)))
        end if
      end do

      return
    end subroutine eph_diag_dynmat

!============================================================================
!============================================================================

    subroutine eph_sigmafm2d_tofile( xrange, yrange, nx, ny, ip, ist, fname)
      use m_getunit
      real(8), intent( in) :: xrange(2), yrange(2)
      integer, intent( in) :: nx, ny, ip, ist
      character(*), intent( in) :: fname

      integer :: i, j, un
      real(8) :: dx, dy, x, y
      complex(8), allocatable :: v(:), f(:)

      dx = (xrange(2) - xrange(1))/dble( nx-1)
      dy = (yrange(2) - yrange(1))/dble( ny-1)

      allocate( v( nx), f( nx))
      call getunit( un)
      open( un, file=trim( fname), action='write', status='unknown', form='formatted')
      write( un, '("#    xrange                                  yrange                                  ip   ist  eval")')
      write( un, '("# ",4g20.10e3,2i5,g20.10e3)') xrange, yrange, ip, ist, eph_evalp( ist, ip)
      do j = 0, ny - 1
        y = yrange(1) + dble( j)*dy
        do i = 0, nx - 1
          x = xrange(1) + dble( i)*dx
          v( i+1) = cmplx( x, y, 8)
        end do
        call eph_gen_sigmafm( eph_evalp( ist, ip) + v, nx, ip, ist, ist, f)
        do i = 1, nx
          write( un, '(6g20.10e3)') f( i), abs( f( i)), atan2( aimag( f( i)), dble( f( i))), f( i) - v( i)
        end do
        write( un, *)
        write( un, *)
      end do
      close( un)

      deallocate( v, f)
      return
    end subroutine eph_sigmafm2d_tofile

    subroutine eph_set_qpset( ip)
      integer, intent( in) :: ip

      integer :: iq, vi(3)

      do iq = 1, eph_qset%nkpt
        eph_qpset%vkl( :, iq) = eph_qset%vkl( :, iq) + eph_pset%vkl( :, ip)
        call r3frac( 1.d-6, eph_qpset%vkl( :, iq), vi)
        call r3mv( eph_qpset%bvec, eph_qpset%vkl( :, iq), eph_qpset%vkc( :, iq))
      end do
    end subroutine eph_set_qpset
end module mod_eph
