module mod_eph_phonons
  use mod_atoms, only: nspecies, natmtot, idxas, spmass, atposc
  use modmain, only: natoms
  use mod_symmetry, only: nsymcrys, symlat, lsplsymc
  use mod_lattice, only: omega
  use mod_eph_variables

  implicit none

! methods
  contains

    ! interpolation wrapper
    subroutine eph_phonons_interpolate( kset, qset, phfreq, phevec, epsinf, zstar)
      type( k_set), intent( in)      :: kset, qset
      real(8), intent( out)          :: phfreq( 3*natmtot, qset%nkpt)
      complex(8), intent( out)       :: phevec( 3, natmtot, 3*natmtot, qset%nkpt)
      real(8), optional, intent( in) :: epsinf(3,3)
      real(8), optional, intent( in) :: zstar( 3, 3, natmtot)

      integer :: iq, i, imode, is, ia, ias, ip
      real(8) :: r
      type( k_set) :: tmpqset

      complex(8), allocatable :: dynk(:,:,:), dynq(:,:,:), ev(:,:)

      allocate( dynk( 3*natmtot, 3*natmtot, kset%nkpt))
      allocate( dynq( 3*natmtot, 3*natmtot, qset%nkpt))

      call eph_phonons_read_dynmat( kset, dynk, tsym=.true.)
      call eph_phonons_sumrule( kset, dynk)
      if( present( epsinf) .and. present( zstar)) then
        if( kset%nkpt .eq. 1) then
          if( norm2( kset%vkl(:,1)) .lt. 1.d-6) then
            tmpqset = qset
            do iq = 1, qset%nkpt
              r = norm2( tmpqset%vkc(:,iq))
              if( r .gt. 1.d-16) then
                tmpqset%vkl(:,iq) = tmpqset%vkl(:,iq)*1.d-16/r
                tmpqset%vkc(:,iq) = tmpqset%vkc(:,iq)*1.d-16/r
              end if
            end do
            call eph_phonons_interpolate_dynmat( kset, dynk, tmpqset, dynq, epsinf=epsinf, zstar=zstar)
          else
            do iq = 1, qset%nkpt
              dynq(:,:,iq) = dynk(:,:,1)
            end do
          end if
        else
          call eph_phonons_interpolate_dynmat( kset, dynk, qset, dynq, epsinf=epsinf, zstar=zstar)
        end if
      else
        if( kset%nkpt .eq. 1) then
          do iq = 1, qset%nkpt
            dynq(:,:,iq) = dynk(:,:,1)
          end do
        else
          call eph_phonons_interpolate_dynmat( kset, dynk, qset, dynq)
        end if
      end if
      deallocate( dynk)

      allocate( ev( 3*natmtot, 3*natmtot))
      do iq = 1, qset%nkpt
        call eph_phonons_diag_dynmat( dynq(:,:,iq), phfreq(:,iq), ev)
        do imode = 1, 3*natmtot
          i = 0
          do is = 1, nspecies
            do ia = 1, natoms( is)
              ias = idxas( ia, is)
              do ip = 1, 3
                i = i + 1
                phevec( ip, ias, imode, iq) = ev( i, imode)
              end do
            end do
          end do
        end do
      end do
      deallocate( dynq, ev)

      return
    end subroutine eph_phonons_interpolate
      
    ! read dynamical matrices from file
    subroutine eph_phonons_read_dynmat( kset, dynk, tsym)
      use mod_dynmat
      use m_getunit
      type( k_set), intent( in)     :: kset
      complex(8), intent( out)      :: dynk( 3*natmtot, 3*natmtot, kset%nkpt)
      logical, optional, intent(in) :: tsym

      logical :: exist, tsym_
      integer :: ik, ik0, is, js, ia, ja, ip, jp, i, j, un
      real(8) :: a, b, vc(3), vl(3), m(3,3),  w( 3*natmtot)
      character(256) :: fext
      character(256) :: chdummy

      !call readdyn_espresso( .true., dynk)
      !return

      if( eph_dfpt) then
        call read_dyn( 'dyn_mat_bGa2O3/beta')
        call r3minv( eph_kset_ph%bvec, m)
        do ik = 1, nqredtot
          call r3mv( m, xqcred(:,ik), vl)
          call findkptinset( vl, kset, is, ik0)
          !write(*,'(2(i,3f13.6))') ik, vl, ik0, kset%vkl(:,ik0)
          dynk(:,:,ik0) = cmplx( 0.5d0, 0.d0, 8)*dynmat(:,:,ik)
          call dynsym( kset%vkl(:,ik0), dynk(:,:,ik0))
        end do
        return
      end if

      tsym_ = .true.
      if( present( tsym)) tsym_ = tsym

      do ik = 1, kset%nkpt
        i = 0
        do is = 1, nspecies
          do ia = 1, natoms (is)
            do ip = 1, 3
              i = i + 1
              call phfext( ik, is, ia, ip, 0, 1, chdummy, fext, chdummy)
              inquire( file='DYN'//trim(fext), exist=exist)
              if( .not. exist) then
                write(*,*)
                write(*, '("Error (eph_read_dynmat): file not found :")')
                write(*, '(A)') ' DYN' // trim (fext)
                write(*,*)
                stop
              end if
              call getunit( un)
              open( un, file='DYN'//trim(fext), action='read', status='old', form='formatted')
              j = 0
              do js = 1, nspecies
                do ja = 1, natoms (js)
                  do jp = 1, 3
                    j = j + 1
                    read( un, *) a, b
                    dynk( i, j, ik) = cmplx( a, b, 8)
                  end do
                end do
              end do
              close( un)
            end do
          end do
        end do

        ! symmetrise the dynamical matrix
        if( tsym_) call dynsym( kset%vkl( :, ik), dynk( :, :, ik))
      end do

      return
    end subroutine eph_phonons_read_dynmat

    ! apply acoustic sum rule
    subroutine eph_phonons_sumrule( kset, dynk)
      use m_linalg, only: zhediag
      type( k_set), intent( in)  :: kset
      complex(8), intent( inout) :: dynk( 3*natmtot, 3*natmtot, kset%nkpt)

      integer :: ik, ik0, i, j, k
      real(8) :: eval( 3*natmtot)
      complex(8) :: dyn0( 3*natmtot, 3*natmtot), evec( 3*natmtot, 3*natmtot)

      call findkptinset( (/0.d0, 0.d0, 0.d0/), kset, i, ik0)
      dyn0 = 0.5d0*( dynk(:,:,ik0) + conjg( transpose( dynk(:,:,ik0))))
      call zhediag( dyn0, eval, evec)

      do ik = 1, kset%nkpt
        do i = 1, 3*natmtot
          do j = 1, 3*natmtot
            do k = 1, 3
              dynk( i, j, ik) = dynk( i, j, ik) - eval(k)*evec(i,k)*conjg( evec(j,k))
            end do
          end do
        end do
      end do

      return        
    end subroutine eph_phonons_sumrule

    ! interpolate dynamical matrices from original grid to interpolation grid
    subroutine eph_phonons_interpolate_dynmat( kset, dynk, qset, dynq, epsinf, zstar)
      use m_wsweight
      use m_plotmat
      use mod_lattice,   only: omega
      use m_getunit

      type( k_set), intent( in)      :: kset, qset                              ! coarse (original) and fine (interpolation) grid
      complex(8), intent( inout)     :: dynk( 3*natmtot, 3*natmtot, kset%nkpt)  ! dynamical matrices on original grid
      complex(8), intent( out)       :: dynq( 3*natmtot, 3*natmtot, qset%nkpt)  ! dynamical matrices on interpolation grid
      real(8), optional, intent( in) :: epsinf(3,3)                             ! clamped nuclei dielectric tensor
      real(8), optional, intent( in) :: zstar( 3, 3, natmtot)                   ! Born effective charge tensors

      logical :: nan
      integer :: nrpt
      real(8) :: atpos( 3, natmtot)
      integer, allocatable :: rvec(:,:), rmul(:), distvec(:,:,:,:,:), distmul(:,:,:)
      complex(8), allocatable :: pkr(:,:), dynlr(:,:,:), dynrc(:,:,:)
      real(8), allocatable :: dynr(:,:,:)

      integer :: ngridkph(3), i1, i2, i3, r1, r2, r3, ir, ik, iknr, iq, n, isym, lspl, iv(3), is, ia, js, ja, ias, jas, un
      real(8) :: v1(3), v2(3), v3(3), s(3,3), rdotk, w( 3*natmtot), wloto, d1
      complex(8) :: dyns( 3*natmtot, 3*natmtot), zwgt, ev( 3*natmtot, 3*natmtot)

      nan = .false.
      if( .true. .and. present( epsinf) .and. present( zstar)) nan = .true.
      ngridkph = kset%ngridk

      ! substract non-analytic part
      !if( nan) then
      !  call eph_phonons_gendynmat_lr2( kset, epsinf, zstar, -zone, dynk)
      !end if
      ! generate R-vectors

      ! get analytic dynamical matrix in real-space representation (force constants)
      nrpt = product( ngridkph)
      allocate( dynrc( 3*natmtot, 3*natmtot, nrpt))
      allocate( dynr( 3*natmtot, 3*natmtot, nrpt))
      dynrc = zzero
      do i1 = 0, ngridkph(1)-1
        v1(1) = dble( i1)/dble( ngridkph(1))
        do i2 = 0, ngridkph(2)-1
          v1(2) = dble( i2)/dble( ngridkph(2))
          do i3 = 0, ngridkph(3)-1
            v1(3) = dble( i3)/dble( ngridkph(3))
            ik = kset%ikmap( i1, i2, i3)
            v2 = v1
            call vecfbz( input%structure%epslat, kset%bvec, v2, iv)
            n = 0
            dyns = zzero
            do isym = 1, nsymcrys
              lspl = lsplsymc (isym)
              s = dble( symlat( :, :, lspl))
              call r3mtv (s, kset%vkl(:, ik), v3)
              call vecfbz (input%structure%epslat, kset%bvec, v3, iv)
              if (norm2(v2 - v3) .Lt. input%structure%epslat) Then
                Call dynsymapp (isym, kset%vkl(:, ik), dynk(:, :, ik), dyns)
                  n = n + 1
               End If
            End Do
            If (n .Eq. 0) Then
               Write (*,*)
               Write (*, '("Error(dynqtor): vector ", 3G18.10)') v1
               Write (*, '(" cannot be mapped to reduced q-point set&
              &")')
               Write (*,*)
               Stop
            End If
            dyns = dyns/dble( n)
            ir = 0
            !do r3 = ngridkph(3)/2-ngridkph(3)+1, ngridkph(3)/2
            !  do r2 = ngridkph(2)/2-ngridkph(2)+1, ngridkph(2)/2
            !    do r1 = ngridkph(1)/2-ngridkph(1)+1, ngridkph(1)/2
            do r3 = 0, ngridkph(3)-1
              do r2 = 0, ngridkph(2)-1
                do r1 = 0, ngridkph(1)-1
                  ir = ir + 1
                  rdotk = twopi*dot_product( kset%vkl(:,ik), dble( (/r1, r2, r3/)))
                  zwgt = cmplx( cos( rdotk), sin( rdotk), 8)
                  dynrc(:,:,ir) = dynrc(:,:,ir) + zwgt*dyns
                end do
              end do
            end do

          end do
        end do
      end do

      !allocate( dynr( 3*natmtot, 3*natmtot, nrpt))
      !dynr = zzero
      !do i1 = 0, ngridkph(1) - 1
      !  do i2 = 0, ngridkph(2) - 1
      !    do i3 = 0, ngridkph(3) - 1
      !      ik   = kset%ikmap( i1, i2, i3)
      !      iknr = kset%ikmapnr( i1, i2, i3)
      !      v1 = kset%vklnr( :, iknr)
      !      v2 = v1
      !      !call r3frac( input%structure%epslat, v2, iv)
      !      !n = 0
      !      !dyns = zzero
      !      !do isym = 1, nsymcrys
      !      !  lspl = lsplsymc( isym)
      !      !  s = dble( symlat( :, :, lspl))
      !      !  call r3mtv( s, kset%vkl(:, ik), v3)
      !      !  call r3frac( input%structure%epslat, v3, iv)
      !      !  if( norm2( v2 - v3) .lt. input%structure%epslat) then
      !      !    call dynsymapp( isym, kset%vkl( :, ik), dynk( :, :, ik), dyns)
      !      !    n = n + 1
      !      !  end if
      !      !  ! substract long range part
      !      !end do
      !      !write(*,'(i,2(3f13.6,3x))') n, kset%vklnr(:,iknr), kset%vkl(:,ik)
      !      !if( n .eq. 0) then
      !      !  write(*,*)
      !      !  write(*, '("Error( eph_interpolate_dynmat): vector ", 3G18.10)') v1
      !      !  write(*, '(" cannot be mapped to reduced q-point set.")')
      !      !  stop    
      !      !end if    
      !      !dyns = dyns/dble( n)
      !      dyns = dynk(:,:,ik)
      !      do ir = 1, nrpt
      !        dynr( :, :, ir) = dynr( :, :, ir) + conjg( pkr( ksetnr%ikmap( i1, i2, i3), ir))*dyns
      !      end do
      !    end do       
      !  end do         
      !end do           
                        
      dynrc = dynrc/product( ngridkph)
      dynr = dble( dynrc)
      !call getunit( un)
      !open( un, file='FORCE_CONSTANTS.DAT', action='write', form='formatted')
      !do i1 = 1, 3
      !  do i2 = 1, 3
      !    do ias = 1, natmtot
      !      do jas = 1, natmtot
      !        write(un,'(4i4)') i1, i2, ias, jas
      !        ir = 0
      !        do r3 = 1, ngridkph(3)
      !          do r2 = 1, ngridkph(2)
      !            do r1 = 1, ngridkph(1)
      !              ir = ir + 1
      !              write(un,'(3i4,2x,1pe18.11)') r1, r2, r3, 2.d0*dynr( (ias-1)*3+i1, (jas-1)*3+i2, ir)
      !            end do
      !          end do
      !        end do
      !      end do
      !    end do
      !  end do
      !end do
      !close( un)
      do ir = 1, nrpt   
        call r3mv( input%structure%crystal%basevect, dble( rvec(:,ir)), v1)
        write(*,'(i,2f13.6)') ir, maxval( abs( dble( dynrc(:,:,ir)))), maxval( abs( aimag( dynrc(:,:,ir))))
      end do

      call eph_phonons_symmetrize_dynr( eph_kset_ph%ngridk, dynr)
      ! generate long range part on fine grid
      if( nan) then
        allocate( dynlr( 3*natmtot, 3*natmtot, qset%nkpt))
        dynlr = zzero
        call eph_phonons_gendynmat_lr( qset, epsinf, zstar, zone, dynlr)
        dynlr = dynlr/product( ngridkph)
      end if

      ! do interpolation
      !call getunit( un)
      !open( un, file='DYN_BND.DAT', action='write', form='formatted')
      dynq = zzero
      do iq = 1, qset%nkpt
        !write( un, *)
        !write( un, '(5x,"Dynamical  Matrix in cartesian axes")')
        !write( un, *)
        !write( un, '(5x,"q = (",3f14.9,")")') qset%vkl(:,iq)
        !write( un, *)
        i1 = 1
        do is = 1, nspecies
          do ia = 1, natoms( is)
            i2 = 1
            do js = 1, nspecies
              do ja = 1, natoms( js)
                ir = 0
                !do r3 = ngridkph(3)/2-ngridkph(3)+1, ngridkph(3)/2
                !  do r2 = ngridkph(2)/2-ngridkph(2)+1, ngridkph(2)/2
                !    do r1 = ngridkph(1)/2-ngridkph(1)+1, ngridkph(1)/2
                do r3 = 0, ngridkph(3)-1
                  do r2 = 0, ngridkph(2)-1
                    do r1 = 0, ngridkph(1)-1
                      ir = ir + 1
                      v1 = dble( (/r1, r2, r3/))
                      if( eph_dfpt) then
                        v2 = v1 + input%structure%speciesarray(is)%species%atomarray(ia)%atom%coord(:) &
                                - input%structure%speciesarray(js)%species%atomarray(ja)%atom%coord(:)
                      else
                        v2 = v1 + input%structure%speciesarray(js)%species%atomarray(ja)%atom%coord(:) &
                                - input%structure%speciesarray(is)%species%atomarray(ia)%atom%coord(:)
                      end if
                      call ws_weight( v1, v2, qset%vkl(:,iq), zwgt)
                      dynq( i1:i1+2, i2:i2+2, iq) = dynq( i1:i1+2, i2:i2+2, iq) + zwgt*dynr( i1:i1+2, i2:i2+2, ir)
                      if( nan) dynq( i1:i1+2, i2:i2+2, iq) = dynq( i1:i1+2, i2:i2+2, iq) + zwgt*dynlr( i1:i1+2, i2:i2+2, iq)
                    end do
                  end do
                end do
                !do i3 = 0, 2
                !  write( un, '(3(2f12.8,2x))') 2.d0*dynq( i1+i3, i2:i2+2, iq)
                !end do
                i2 = i2 + 3
              end do
            end do
            i1 = i1 + 3
          end do
        end do
      end do
      !close( un)

      !dynq = zzero
      !do is = 1, nspecies
      !  do ia = 1, natoms( is)
      !    ias = idxas( ia, is)
      !    atpos( :, ias) = atposc( :, ia, is)
      !  end do
      !end do
      !call wannier_mindist( input%structure%crystal%basevect, kset, nrpt, rvec, natmtot, atpos, distvec, distmul)
      !do iq = 1, qset%nkpt
      !  do ir = 1, nrpt
      !    i1 = 1
      !    do is = 1, nspecies
      !      do ia = 1, natoms( is)
      !        ias = idxas( ia, is)
      !        i2 = 1
      !        do js = 1, nspecies
      !          do ja = 1, natoms( js)
      !            jas = idxas( ja, js)
      !            zwgt = zzero
      !            do n = 1, distmul( ias, jas, ir)
      !              rdotk = twopi*dot_product( qset%vkl( :, iq), dble( distvec( :, n, ias, jas, ir)))
      !              rdotk = rdotk + dot_product( qset%vkc( :, iq), atpos( :, jas) - atpos( :, ias))
      !              zwgt = zwgt + cmplx( cos( rdotk), sin( rdotk), 8)
      !            end do
      !            zwgt = zwgt/dble( rmul( ir)*distmul( ias, jas, ir))
      !            dynq( i1:(i1+2), i2:(i2+2), iq) = dynq( i1:(i1+2), i2:(i2+2), iq) + zwgt*dble( dynr( i1:(i1+2), i2:(i2+2), ir))
      !            if( nan) dynq( i1:(i1+2), i2:(i2+2), iq) = dynq( i1:(i1+2), i2:(i2+2), iq) + zwgt*dynlr( i1:(i1+2), i2:(i2+2), iq)
      !            i2 = i2 + 3
      !          end do
      !        end do
      !        i1 = i1 + 3
      !      end do
      !    end do

      !    !rdotk = twopi*dot_product( qset%vkl( :, iq), dble( rvec( :, ir)))
      !    !zwgt = cmplx( cos( rdotk), sin( rdotk), 8)/dble( rmul( ir))
      !    !dynq(:,:,iq) = dynq(:,:,iq) + zwgt*dynr(:,:,ir) 
      !  end do
      !  !write(*,'(i,3f13.6)') iq, qset%vkl( :, iq)
      !  !call plotmat( dyns)
      !  !write(*,*)
      !  !call dynsym( qset%vkl(:,iq), dynq(:,:,iq))
      !end do

      !dynq = zzero
      !do iq = 1, qset%nkpt
      !  i1 = 1
      !  do is = 1, nspecies
      !    do ia = 1, natoms( is)
      !      i2 = 1
      !      do js = 1, nspecies
      !        do ja = 1, natoms( js)
      !          do ir = 1, nrpt
      !            v1 = rvec( :, ir)
      !            v2 = v1 + input%structure%speciesarray(js)%species%atomarray(ja)%atom%coord(:) - &
      !                      input%structure%speciesarray(is)%species%atomarray(ia)%atom%coord(:)
      !            call ws_weight( v1, v2, qset%vkl( :, iq), zwgt)
      !            dynq( i1:(i1+2), i2:(i2+2), iq) = dynq( i1:(i1+2), i2:(i2+2), iq) + zwgt/dble( rmul( ir))*dble( dynr( i1:(i1+2), i2:(i2+2), ir))
      !          end do
      !          i2 = i2 + 3
      !        end do
      !      end do
      !      i1 = i1 + 3
      !    end do
      !  end do
      !end do

      !call delete_k_vectors( ksetnr)
      !deallocate( pkr, dynr, rvec, rmul, distvec, distmul)
      ! add non-analytic part
      if( nan) then
      !  call eph_phonons_gendynmat_lr2( qset, epsinf, zstar, zone, dynq)
        deallocate( dynlr)
      end if


      return        
    end subroutine eph_phonons_interpolate_dynmat

    subroutine eph_phonons_symmetrize_dynr( ngrid, dynr)
      ! apply symmetry constraints and impose acoustic sum rules on
      ! interatomic force constants according to Nicolas Mounet
      integer, intent( in)    :: ngrid(3)
      real(8), intent( inout) :: dynr( 3*natmtot, 3*natmtot, product( ngrid))

      integer :: i, j, l, n1, n2, n3, ias, jas, ia, ja, ir1, ir2, nw, nx, ndim, nvmax
      real(8) :: r1
      real(8) :: t1, t2
      logical :: next

      integer, allocatable :: wpos(:,:,:)
      real(8), allocatable :: wval(:,:), xvec(:,:,:,:), lvec1(:,:,:), lvec2(:,:,:)

      real(8), external :: ddot

      ndim = 9*natmtot*natmtot*product( ngrid)
      nvmax = 3*natmtot*((natmtot+1)*product( ngrid) + 2)

      nw = ndim
      allocate( wpos( 2, 3, nw), wval( 2, nw))
      wpos = 0
      wval = 0.d0

      !do ias = 1, natmtot
      !  do i = 1, 3
      !    ia = (ias-1)*3+i
      !    do jas = ias, natmtot
      !      do j = 1, 3
      !        ja = (jas-1)*3+j
      !        write(*,*) ia, ja
      !      end do
      !    end do
      !    write(*,*)
      !  end do
      !end do
      call timesec(t1)
      ! set up orthonormal vectors w
      nw = 0
      do n3 = 0, ngrid(3) - 1
        do n2 = 0, ngrid(2) - 1
          do n1 = 0, ngrid(1) - 1
            ir1 = n3*ngrid(2)*ngrid(1) + n2*ngrid(1) + n1 + 1
            ir2 = mod( ngrid(3)-n3, ngrid(3))*ngrid(2)*ngrid(1) + &
                  mod( ngrid(2)-n2, ngrid(2))*ngrid(1) + &
                  mod( ngrid(1)-n1, ngrid(1)) + 1
            do ias = 1, natmtot
              do i = 1, 3
                ia = (ias-1)*3+i
                do jas = ias, natmtot
                  do j = 1, 3
                    ja = (jas-1)*3+j
                    if( ir1 .eq. ir2 .and. ias .eq. jas .and. i .eq. j) cycle
                    next = .false.
                    do l = 1, nw
                      if( wpos(2,1,l) .eq. ia .and. &
                          wpos(2,2,l) .eq. ja .and. &
                          wpos(2,3,l) .eq. ir1) then
                        next = .true.
                        exit
                      end if
                    end do
                    if( next) cycle

                    nw = nw + 1
                    wpos(1,1,nw) = ia
                    wpos(1,2,nw) = ja
                    wpos(1,3,nw) = ir1
                    wval(1,nw)   = 1.d0/dsqrt(2.d0)
                    wpos(2,1,nw) = ja
                    wpos(2,2,nw) = ia
                    wpos(2,3,nw) = ir2
                    wval(2,nw)   = -1.d0/dsqrt( 2.d0)
                  end do
                end do
              end do
            end do
          end do
        end do
      end do
      ! find additional linearly independet vectors u
      ! and orthonormalize them
      allocate( lvec1( 3*natmtot, 3*natmtot, product( ngrid)))
      allocate( lvec2( 3*natmtot, 3*natmtot, product( ngrid)))
      allocate( xvec( 3*natmtot, 3*natmtot, product( ngrid), 9*natmtot))
      xvec = 0.d0
      nx = 0
      do ias = 1, natmtot
        do i = 1, 3
          do j = 1, 3
            lvec1 = 0.d0
            do jas = 1, natmtot
              lvec1( (ias-1)*3+i, (jas-1)*3+j, :) = 1.d0
            end do
            lvec2 = lvec1
            do l = 1, nw
              r1 = wdotg( wpos(:,:,l), wval(:,l), lvec1)
              lvec2( wpos(1,1,l), wpos(1,2,l), wpos(1,3,l)) = &
                lvec2( wpos(1,1,l), wpos(1,2,l), wpos(1,3,l)) - r1*wval(1,l)
              lvec2( wpos(2,1,l), wpos(2,2,l), wpos(2,3,l)) = &
                lvec2( wpos(2,1,l), wpos(2,2,l), wpos(2,3,l)) - r1*wval(2,l)
            end do
            do l = 1, nx
              r1 = ddot( ndim, xvec(:,:,:,l), 1, lvec1, 1)
              lvec2 = lvec2 - r1*xvec(:,:,:,l)
            end do
            r1 = dsqrt( ddot( ndim, lvec2, 1, lvec2, 1))
            if( r1 .gt. 1.d-10) then
              nx = nx + 1
              xvec(:,:,:,nx) = lvec2/r1
            end if
          end do
        end do
      end do
      call timesec(t2)
      write(*,*) "ASR"
      write(*,*) nw, nx, nvmax
      write(*,'("T BASIS",f13.6)') t2-t1
      ! calculate projection on subspace
      lvec1 = 0.d0
      do l = 1, nw
        r1 = wdotg( wpos(:,:,l), wval(:,l), dynr)
        lvec1( wpos(1,1,l), wpos(1,2,l), wpos(1,3,l)) = &
          lvec1( wpos(1,1,l), wpos(1,2,l), wpos(1,3,l)) + r1*wval(1,l)
        lvec1( wpos(2,1,l), wpos(2,2,l), wpos(2,3,l)) = &
          lvec1( wpos(2,1,l), wpos(2,2,l), wpos(2,3,l)) + r1*wval(2,l)
      end do
      do l = 1, nx
        r1 = ddot( ndim, xvec(:,:,:,l), 1, dynr, 1)
        lvec1 = lvec1 + r1*xvec(:,:,:,l)
      end do
      dynr = dynr - lvec1
      call timesec( t1)
      write(*,'("T PROJECTION",f13.6)') t1-t2
      write(*,'(2f13.6)') maxval( abs( lvec1)), dsqrt( ddot( ndim, lvec1, 1, lvec1, 1)/dble( ndim))
      deallocate( wpos, wval, lvec1, lvec2, xvec)
      return

      contains
        function wdotg( pos, val, vec) result( r)
          integer, intent( in) :: pos(2,3)
          real(8), intent( in) :: val(2), vec( 3*natmtot, 3*natmtot, product( ngrid))
          real(8) :: r

          r = vec( pos(1,1), pos(1,2), pos(1,3))*val(1) + vec( pos(2,1), pos(2,2), pos(2,3))*val(2)
          return
        end function wdotg
    end subroutine eph_phonons_symmetrize_dynr

    ! diagonalize dynamical matrix to obtain phonon frequencies and eigenvectors
    subroutine eph_phonons_diag_dynmat( dyn, w, ev)
      use m_linalg, only: zhediag
      complex(8), intent( in)  :: dyn( 3*natmtot, 3*natmtot)
      real(8), intent( out)    :: w( 3*natmtot)
      complex(8), intent( out) :: ev( 3*natmtot, 3*natmtot)

      integer :: i, is, ia, ip, j, js, ja, jp
      real(8) :: t1
      complex(8) :: tmp( 3*natmtot, 3*natmtot)

      tmp = zzero
      i = 0
      do is = 1, nspecies
        do ia = 1, natoms( is)
          do ip = 1, 3
            i = i + 1
            j = 0
            do js = 1, nspecies
              if( (spmass( is) .le. 0.d0) .or. (spmass( js) .le. 0.d0)) then
                t1 = 0.d0
              else
                t1 = 1.d0/dsqrt( spmass( is)*spmass( js))
              end if
              do ja = 1, natoms( js)
                do jp = 1, 3
                  j = j + 1
                  tmp( i, j) = 0.5d0*t1*(dyn( i, j) + conjg( dyn( j, i)))
                end do
              end do
            end do
          end do
        end do
      end do
      call zhediag( tmp, w, ev)
      do i = 1, 3*natmtot
        if( w(i) .ge. 0.d0) then
          w(i) = dsqrt( w(i))
        else
          w(i) = -dsqrt( abs( w(i)))
        end if
      end do

      return
    end subroutine eph_phonons_diag_dynmat

    ! add/substract non-nalaytic long-range term to/from dynamical matrices
    subroutine eph_phonons_gendynmat_lr( kset, epsinf, zstar, pref, dynk)
      type( k_set), intent( in)  :: kset
      real(8), intent( in)       :: epsinf(3,3)
      real(8), intent( in)       :: zstar( 3, 3, natmtot)
      complex(8), intent( in)    :: pref
      complex(8), intent( inout) :: dynk( 3*natmtot, 3*natmtot, kset%nkpt)

      real(8), parameter :: gmax = 44.d0
      real(8), parameter :: alpha = 1.d0

      integer :: ng(3)
      complex(8) :: dynk0( 3*natmtot, 3*natmtot)

      integer :: g1, g2, g3, i, j, is, ia, ias, js, ja, jas, ik
      real(8) :: g(3), vt1(3), vt2(3), gz(3), geg, v(3), dotp, tr( 3*natmtot)
      complex(8) :: z1, z2, gze(3), tz( 3*natmtot)

      real(8) :: w( 3*natmtot)
      complex(8) :: ev( 3*natmtot, 3*natmtot), tmp( 3*natmtot, 3*natmtot)

      z1 = pref*fourpi/omega

      do ik = 1, kset%nkpt
        g = kset%vkc(:,ik)
        dotp = norm2( g)
        if( dotp .lt. 1.d-32) cycle
        g = g/dotp
        geg = g(1)*(epsinf(1,1)*g(1) + epsinf(1,2)*g(2) + epsinf(1,3)*g(3)) + &
              g(2)*(epsinf(2,1)*g(1) + epsinf(2,2)*g(2) + epsinf(2,3)*g(3)) + &
              g(3)*(epsinf(3,1)*g(1) + epsinf(3,2)*g(2) + epsinf(3,3)*g(3))
        do is = 1, nspecies
          do ia = 1, natoms( is)
            ias = idxas( ia, is)
            vt1 = matmul( zstar(:,:,ias), g)
            ias = (ias - 1)*3 + 1
            do js = 1, nspecies
              do ja = 1, natoms( js)
                jas = idxas( ja, js)
                vt2 = matmul( zstar(:,:,jas), g)
                jas = (jas - 1)*3 + 1
                dynk( ias:(ias+2), jas:(jas+2), ik) = dynk( ias:(ias+2), jas:(jas+2), ik) + &
                                                      z1/geg*matmul( reshape( vt1, (/3,1/)), reshape( vt2, (/1,3/)))
              end do
            end do
          end do
        end do
      end do

      return
    end subroutine eph_phonons_gendynmat_lr

    subroutine eph_phonons_gendynmat_lr2( kset, epsinf, zstar, pref, dynk)
      type( k_set), intent( in)  :: kset
      real(8), intent( in)       :: epsinf(3,3)
      real(8), intent( in)       :: zstar( 3, 3, natmtot)
      complex(8), intent( in)    :: pref
      complex(8), intent( inout) :: dynk( 3*natmtot, 3*natmtot, kset%nkpt)

      real(8), parameter :: gmax = 44.d0
      real(8), parameter :: alpha = 1.d0

      integer :: ng(3)
      complex(8) :: dynk0( 3*natmtot, 3*natmtot)

      integer :: g1, g2, g3, i, j, is, ia, ias, js, ja, jas, ik
      real(8) :: g(3), vt1(3), vt2(3), gz(3), geg, v(3), dotp, tr( 3*natmtot)
      complex(8) :: z1, z2, gze(3), tz( 3*natmtot)

      real(8) :: w( 3*natmtot)
      complex(8) :: ev( 3*natmtot, 3*natmtot), tmp( 3*natmtot, 3*natmtot)

      z1 = pref*fourpi/omega

      ng(1) = ceiling( gmax/norm2( kset%bvec(:,1)))
      ng(2) = ceiling( gmax/norm2( kset%bvec(:,2)))
      ng(3) = ceiling( gmax/norm2( kset%bvec(:,3)))

      ! k=0 part
      dynk0 = zzero
      do g1 = -ng(1), ng(1)
        do g2 = -ng(2), ng(2)
          do g3 = -ng(3), ng(3)
            call r3mv( kset%bvec, dble((/g1, g2, g3/)), g)
            geg = g(1)*(epsinf(1,1)*g(1) + epsinf(1,2)*g(2) + epsinf(1,3)*g(3)) + &
                  g(2)*(epsinf(2,1)*g(1) + epsinf(2,2)*g(2) + epsinf(2,3)*g(3)) + &
                  g(3)*(epsinf(3,1)*g(1) + epsinf(3,2)*g(2) + epsinf(3,3)*g(3))
            if( (geg .gt. 1.d-18) .and. (geg/alpha/alpha/4.d0 .lt. gmax*gmax)) then
              z2 = z1*exp( -geg/alpha/4.d0)/geg
              call dgemv( 't', 3, 3*natmtot, 1.d0, &
                     zstar, 3, &
                     g, 1, 0.d0, &
                     tr, 1)
              do is = 1, nspecies
                do ia = 1, natoms( is)
                  ias = (idxas( ia, is) - 1)*3 + 1
                  dotp = dot_product( g, atposc( :, ia, is))
                  tz( ias:(ias+2)) = tr( ias:(ias+2))*cmplx( cos( dotp), sin( dotp), 8)
                end do
              end do
              call zgemm( 'c', 'n', 3*natmtot, 3*natmtot, 1, z2, &
                     tz, 1, &
                     tz, 1, zone, &
                     dynk0, 3*natmtot)
            end if
          end do
        end do
      end do

      ! k depdendet part
#ifdef USEOMP
!$omp parallel default( shared) private( ik, g1, g2, g3, vt1, vt2, g, geg, z2, is, ia, ias, tr, tz, gz, js, ja, jas, v, dotp, gze, i, j)
!$omp do
#endif
      do ik = 1, kset%nkpt
        dynk( :, :, ik) = dynk( :, :, ik) + dynk0
        do g1 = -ng(1), ng(1)
          vt1 = kset%vkc( :, ik) + dble( g1)*kset%bvec(:,1)
          do g2 = -ng(2), ng(2)
            vt2 = vt1 + dble( g2)*kset%bvec(:,2)
            do g3 = -ng(3), ng(3)
              g = vt2 + dble( g3)*kset%bvec(:,3)
              geg = g(1)*(epsinf(1,1)*g(1) + epsinf(1,2)*g(2) + epsinf(1,3)*g(3)) + &
                    g(2)*(epsinf(2,1)*g(1) + epsinf(2,2)*g(2) + epsinf(2,3)*g(3)) + &
                    g(3)*(epsinf(3,1)*g(1) + epsinf(3,2)*g(2) + epsinf(3,3)*g(3))
              if( (geg .gt. 1.d-18) .and. (geg/alpha/4.d0 .lt. gmax*gmax)) then
                z2 = z1*exp( -geg/alpha/4.d0)/geg
                call dgemv( 't', 3, 3*natmtot, 1.d0, &
                       zstar, 3, &
                       g, 1, 0.d0, &
                       tr, 1)
                do is = 1, nspecies
                  do ia = 1, natoms( is)
                    ias = (idxas( ia, is) - 1)*3 + 1
                    dotp = dot_product( g, atposc( :, ia, is))
                    tz( ias:(ias+2)) = tr( ias:(ias+2))*cmplx( cos( dotp), sin( dotp), 8)
                  end do
                end do
                call zgemm( 'c', 'n', 3*natmtot, 3*natmtot, 1, z2, &
                       tz, 1, &
                       tz, 1, zone, &
                       dynk( :, :, ik), 3*natmtot)
              end if
            end do
          end do
        end do
      end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif

      return
    end subroutine eph_phonons_gendynmat_lr2

    subroutine eph_phonons_output
      use m_getunit
      use modmpi
      integer :: iq, imode, un

      real(8), allocatable :: phfreq(:,:)
      complex(8), allocatable :: phevec(:,:,:,:)

      if( mpiglobal%rank .ne. 0) return

      allocate( phfreq( 3*natmtot, eph_pset%nkpt))
      allocate( phevec( 3, natmtot, 3*natmtot, eph_pset%nkpt))

      if( eph_polar) then
        call eph_phonons_interpolate( eph_kset_ph, eph_pset, phfreq, phevec, epsinf=eph_epsinf, zstar=eph_zstar)
      else
        call eph_phonons_interpolate( eph_kset_ph, eph_pset, phfreq, phevec)
      end if

      call getunit( un)
      open( un, file='EPH_PHDISP.DAT', action='write', form='formatted')
      do imode = 1, 3*natmtot
        do iq = 1, eph_pset%nkpt
          if( allocated( eph_pset_dpp1d)) write( un, '(g20.10e3)', advance='no') eph_pset_dpp1d( iq)
          write( un, '(g20.10e3,3f16.6)') phfreq( imode, iq), eph_pset%vkl( :, iq)
        end do
        write( un, *)
        write( un, *)
      end do
      close( un)

      if( allocated( eph_pset_dpp1d)) then
        call getunit( un)
        open( un, file='EPH_VTXLINES.DAT', action='write', form='formatted')
        do iq = 1, eph_pset_nvp1d
          write( un, '(2g20.10)') eph_pset_dvp1d( iq), minval( phfreq)
          write( un, '(2g20.10)') eph_pset_dvp1d( iq), maxval( phfreq)
          write( un, *)
        end do
        close( un)
      end if

      deallocate( phfreq, phevec)
      return
    end subroutine eph_phonons_output
end module mod_eph_phonons
