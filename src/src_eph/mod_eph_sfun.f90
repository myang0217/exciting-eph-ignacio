module mod_eph_sfun
  use mod_eph_variables
  use mod_eph_helper

  implicit none

! methods
  contains

    subroutine eph_gfun_gen( nfreq, freqs, sigma, gfun)
      integer, intent( in)     :: nfreq
      real(8), intent( in)     :: freqs( nfreq)
      complex(8), intent( in)  :: sigma( nfreq)
      complex(8), intent( out) :: gfun( nfreq)

      integer :: iw

      gfun = zzero
!#ifdef USEOMP
!!$omp parallel default( shared) private( iw)
!!$omp do
!#endif
      do iw = 1, nfreq
        gfun( iw) = zone/( cmplx( freqs( iw), 0.d0, 8) - sigma( iw))
      end do
!#ifdef USEOMP
!!$omp end do
!!$omp end parallel
!#endif

      return
    end subroutine eph_gfun_gen

    subroutine eph_sfun_gen( nfreq, freqs, sigma, sfun)
      integer, intent( in)    :: nfreq
      real(8), intent( in)    :: freqs( nfreq)
      complex(8), intent( in) :: sigma( nfreq)
      real(8), intent( out)   :: sfun( nfreq)

      integer :: iw
      real(8) :: t1, eps

      ! infinitesimal to get a peak eveb if Im Sigma == 0
      eps = sign( 1.d-5, aimag( sigma( maxloc( abs( aimag( sigma)), 1)))) 

      t1 = -1.d0/pi
      sfun = 0.d0
!#ifdef USEOMP
!!$omp parallel default( shared) private( iw)
!!$omp do
!#endif
      do iw = 1, nfreq
        sfun( iw) = t1*(aimag( sigma( iw))+eps)/((freqs( iw) - dble( sigma( iw)))**2 + (aimag( sigma( iw))+eps)**2)
      end do
!#ifdef USEOMP
!!$omp end do
!!$omp end parallel
!#endif

      return
    end subroutine eph_sfun_gen

    !subroutine eph_sfun_findqp( nfreq, freqs, sigma, nqpmax, eps, nqp, qp, diag)
    !  use m_getunit
    !  integer, intent( in)            :: nfreq
    !  real(8), intent( in)            :: freqs( nfreq)
    !  complex(8), intent( in)         :: sigma( nfreq)
    !  integer, intent( in)            :: nqpmax
    !  real(8), intent( in)            :: eps
    !  integer, intent( out)           :: nqp
    !  complex(8), intent( out)        :: qp( nqpmax, 2)
    !  real(8), optional, intent( out) :: diag(2)

    !  ! parameters
    !  integer :: ni    ! #interpolation points
    !  real(8) :: si    ! area unter spectral function in which QPs are searched (si \in (0,1])

    !  ! working varialbles
    !  real(8) :: xr(2)

    !  real(8), allocatable :: sfun(:), fi(:)
    !  complex(8), allocatable :: gi(:)

    !  ! auxilliary variables
    !  integer :: i, j, k, un
    !  real(8) :: r1

    !  real(8), allocatable :: r1d(:), r2d(:,:)
    !  complex(8), allocatable :: c2d(:,:)

    !  !******************************************
    !  !* SET PARAMETERS
    !  !******************************************
    !  si = 0.99d0
    !  ni = 400

    !  !******************************************
    !  !* FIND SEARCH INTERVAL
    !  !******************************************
    !  allocate( sfun( nfreq), r1d( nfreq), r2d( 3, nfreq))
    !  call eph_sfun_gen( nfreq, freqs, sigma, sfun)
    !  call fderiv( -1, nfreq, freqs, abs( sfun), r1d, r2d)
    !  r1d = r1d/r1d( nfreq)

    !  r1 = (1.d0 - si)*0.5d0
    !  i = minloc( abs( r1d-r1), 1)
    !  if( (r1d( i) .gt. r1) .or. (i .eq. nfreq)) i = i-1
    !  xr(1) = (r1 - r1d( i))*(freqs( i+1) - freqs( i))/(r1d( i+1) - r1d( i)) + freqs( i)

    !  r1 = (1.d0 + si)*0.5d0
    !  i = minloc( abs( r1d-r1), 1)
    !  if( (r1d( i) .ge. r1) .or. (i .eq. nfreq)) i = i-1
    !  xr(2) = (r1 - r1d( i))*(freqs( i+1) - freqs( i))/(r1d( i+1) - r1d( i)) + freqs( i)
    !  deallocate( r2d)

    !  !******************************************
    !  !* GENERATE INTERPOLATION GRID
    !  !******************************************
    !  ! we use the spectral function to set up an interpolation frequency grid
    !  ! that is densly sampled where the peaks are
    !  r1 = 0.5d0
    !  sfun = r1*sfun*dble( ni-1)/r1d( nfreq) + (1.d0-r1)*dble( ni-1)/(xr(2) - xr(1))
    !  do i = 1, 4
    !    call savitzkygolay( nfreq, freqs, sfun, 1, 3, 0, r1d)
    !    sfun = r1d
    !  end do
    !  allocate( fi( ni), r2d( ni, 2))
    !  call spacing( xr(1), xr(2), ni, r2d(:,1), 1)
    !  call interp1d( nfreq, freqs, sfun, ni, r2d(:,1), r2d(:,2), 'linear')
    !  call spacingFromDensity( ni, r2d(:,1), r2d(:,2), ni, fi)
    !  deallocate( r1d, r2d, sfun)

    !  !******************************************
    !  !* INTERPOLATE GREEN'S FUNCTION
    !  !******************************************
    !  ! It is numerically more stable to first interpolate Sigma and then recalculating
    !  ! the Green's function from it instead of interpolating the Green's function directly.
    !  allocate( gi( ni), r2d( ni, 2), c2d( ni, 2))
    !  call interp1d( nfreq, freqs, dble(  sigma), ni, fi, r2d(:,1), 'spline')
    !  call interp1d( nfreq, freqs, aimag( sigma), ni, fi, r2d(:,2), 'spline')
    !  c2d(:,1) = cmplx( r2d(:,1), r2d(:,2), 8)
    !  call eph_gfun_gen( ni, fi, cmplx( r2d(:,1), r2d(:,2), 8), gi)
    !  deallocate( r2d)

    !  !******************************************
    !  !* FIND POLES OF GREEN'S FUNCTION
    !  !******************************************
    !  if( present( diag)) then
    !    call eph_gfun_findqp( ni, fi, gi, nqpmax, eps, nqp, qp, diag)
    !    diag(1) = diag(1) + (1.d0 - si)
    !  else
    !    call eph_gfun_findqp( ni, fi, gi, nqpmax, eps, nqp, qp)
    !  end if

    !  do i = 1, nqp
    !    write(*,'(i,5f13.6)') i, qp(i,1), qp(i,2)
    !  end do

    !  c2d(:,2) = zzero
    !  do i = 1, nqp
    !    c2d(:,2) = c2d(:,2) + qp(i,2)/(fi - qp(i,1))
    !  end do
    !  call getunit( un)
    !  open( un, file='GAPPROX.DAT', action='write', form='formatted')
    !  do i = 1, ni
    !    write( un, '(9g20.10)') fi(i), gi(i), c2d(i,2), c2d(i,1), fi(i)-zone/c2d(i,2)
    !  end do
    !  close( un)
    !  deallocate( c2d)
    !end subroutine eph_sfun_findqp

    !subroutine eph_gfun_findqp( nfreq, freqs, gfun, nqpmax, eps, nqp, qp, diag)
    !  use mod_polynomials
    !  use mod_schlessinger
    !  !use mod_eph_sigma, only: eph_sigmafm2d_tofile2
    !  use m_getunit
    !  integer, intent( in)            :: nfreq
    !  real(8), intent( in)            :: freqs( nfreq)
    !  complex(8), intent( in)         :: gfun( nfreq)
    !  integer, intent( in)            :: nqpmax
    !  real(8), intent( in)            :: eps
    !  integer, intent( out)           :: nqp
    !  complex(8), intent( out)        :: qp( nqpmax, 2)
    !  real(8), optional, intent( out) :: diag(2)

    !  ! working varialbles
    !  real(8) :: xnorm, ynorm, xoff, sint
    !  real(8), allocatable :: fn(:)
    !  complex(8), allocatable :: gn(:)

    !  ! auxilliary variables
    !  integer :: i, un
    !  real(8) :: r1

    !  integer, allocatable :: idx(:)
    !  real(8), allocatable :: r1d1(:), r2d1(:,:)
    !  complex(8), allocatable :: c1d1(:), c1d2(:), c2d1(:,:), c2d2(:,:)
    !  complex(8), allocatable :: p(:), q(:)

    !  qp = zzero

    !  !******************************************
    !  !* NORMALIZE DATA
    !  !******************************************
    !  allocate( fn( nfreq), gn( nfreq))
    !  allocate( r1d1( nfreq), r2d1( 3, nfreq))
    !  xnorm = freqs(nfreq) - freqs(1)
    !  fn = freqs/xnorm
    !  xoff = fn(nfreq)
    !  call fderiv( -1, nfreq, fn, aimag( gfun), r1d1, r2d1)
    !  ynorm = r1d1( nfreq)
    !  gn = gfun/ynorm
    !  call fderiv( -1, nfreq, freqs, aimag( gfun), r1d1, r2d1)
    !  if( present( diag)) diag(1) = -r1d1( nfreq)/pi
    !  sint = trapz( nfreq, fn, dble(  gn)**2) + trapz( nfreq, fn, aimag( gn)**2)
    !  deallocate( r1d1, r2d1)

    !  !******************************************
    !  !* WRITE CONTINUATION TO FILE
    !  !******************************************
    !  i = 400
    !  call schlessinger_init( nfreq, cmplx( fn, 0.d0, 8), gn)
    !  !call eph_sigmafm2d_tofile2( (/fn(1), fn(nfreq)/), (/-0.5d0, 0.5d0/), 1001, 1001, 1, 1, 'GFUN2D.DAT', i)
    !  call getunit( un)
    !  open( un, file='GNORM.DAT', action='write', form='formatted')
    !  do i = 1, nfreq
    !    write( un, '(3g20.10e3)') fn(i), gn(i)
    !  end do
    !  close( un)

    !  !******************************************
    !  !* FIND POLES OF GREEN'S FUNCTION
    !  !******************************************
    !  ! We search for a rational function r(w) = p(w)/q(w) that best approximates the Green's function.
    !  ! p and q are polynomials of degree n-1 and n, respectively. This is equivalent to express the 
    !  ! Green's function as a sum of simple poles
    !  ! r(w) ~ G(w) = Sum_i^n A(i)/(w - QP(i))
    !  ! where QP represents the quasi-particle poles of the Green's function.
    !  ! Successively we increase the number of poles until the interpolant has reached the desired accuracy.
    !  ! An initial guess of the A(i) and QP(i) is found by a simple linearized least squares problem and 
    !  ! subsequently refined using a non-linear least squares optimization routine. During the optimization
    !  ! We force the poles to be in the lower half of the complex plane (since we are approximating the retarded
    !  ! Green's function) and we further force the sum rule to be fulfilled (i.e. the integral over the 
    !  ! spectral function A = -1/pi Im G has to be 1).
    !  
    !  allocate( c1d2( 2*nqpmax), r1d1( 4*nqpmax), c2d1( nfreq, 2*nqpmax), c2d2( 2*nqpmax, nfreq), p(0:nqpmax), q(0:nqpmax))
    !  q(0) = cmplx( 1.d0, 0.d0, 8)
    !  do nqp = 1, nqpmax
    !    ! solve linearized least squares problem
    !    c2d1(:,(nqp-1)*2+1) = cmplx( (fn + xoff)**(nqp-1), 0.d0, 8)
    !    c2d1(:,(nqp-1)*2+2) = -(gn)*(fn + xoff)**nqp
    !    call zpinv( c2d1(:,1:2*nqp), c2d2(1:2*nqp,:))
    !    c1d2(1:2*nqp) = matmul( c2d2(1:2*nqp,:), gn)
    !    do i = 1, nqp
    !      p(i-1) = c1d2((i-1)*2+1)
    !      q(i) = c1d2((i-1)*2+2)
    !    end do
    !    ! get initial guess from partial fraction decomposition
    !    call poly_sapart( nqp-1, p(0:nqp-1), nqp, q(0:nqp), qp(1:nqp,2), qp(1:nqp,1))
    !    do i = 1, nqp
    !      r1d1((i-1)*4+1) = dble(  qp(i,1)) - xoff
    !      r1d1((i-1)*4+2) = sqrt( abs( aimag( qp(i,1))))
    !      r1d1((i-1)*4+3) = sqrt( abs( dble(  qp(i,2))))
    !      r1d1((i-1)*4+4) = aimag( qp(i,2))
    !    end do
    !    ! perform non-linear optimization with constraints
    !    call praxis( 1.d-16, 1.d-2, 4*nqp, 0, r1d1(1:4*nqp), objective, r1)
    !    r1 = objective( r1d1(1:4*nqp), 4*nqp)
    !    if( r1 .lt. eps) exit
    !  end do
    !  if( nqp .gt. nqpmax) nqp = nqpmax
    !  ! renorm the found solutions
    !  do i = 1, nqp
    !    qp(i,1) = cmplx( xnorm*r1d1((i-1)*4+1), -xnorm*r1d1((i-1)*4+2)**2, 8)
    !    qp(i,2) = cmplx( -xnorm*ynorm*r1d1((i-1)*4+3)**2, xnorm*ynorm*r1d1((i-1)*4+4), 8)
    !  end do
    !  ! sort QPs with decreasing strength
    !  allocate( idx( nqp))
    !  call sortidx( nqp, -dble( qp(1:nqp,2)), idx)
    !  qp(1:nqp,1) = qp(idx,1)
    !  qp(1:nqp,2) = qp(idx,2)

    !  deallocate( idx, c1d2, r1d1, c2d1, c2d2, p, q, fn, gn)

    !  if( present( diag)) then
    !    allocate( c1d1( nfreq))
    !    c1d1 = zzero
    !    do i = 1, nqp
    !      c1d1 = c1d1 + qp(i,2)/(freqs - qp(i,1))
    !    end do
    !    diag(2) = sqrt( (trapz( nfreq, freqs, dble(  c1d1-gfun)**2) + trapz( nfreq, freqs, aimag( c1d1-gfun)**2))/ &
    !                    (trapz( nfreq, freqs, dble(  gfun)**2) + trapz( nfreq, freqs, aimag( gfun)**2)))
    !    deallocate( c1d1)
    !  end if
    !  return

    !  contains
    !    function objective( x, n) result( f)
    !      integer(4) :: n
    !      real(8)    :: x(n)
    !      real(8)    :: f

    !      integer :: i, m
    !      real(8) :: s
    !      complex(8) :: a, z, y(nfreq)

    !      m = n/4
    !      y = gn
    !      s = 0.d0
    !      do i = 1, m
    !        z = cmplx( x((i-1)*4+1), -(x((i-1)*4+2))**2, 8)  ! force Im(z)<0
    !        a = cmplx( -x((i-1)*4+3)**2, x((i-1)*4+4), 8)
    !        s = s + dble( a)
    !        y = y - a/(fn - z)
    !      end do
    !      ! the second term forces the sum rule to be fullfilled (i.e. the integral of the spectral function is 1)
    !      f = sqrt( (trapz( nfreq, fn, dble( y)**2) + trapz( nfreq, fn, aimag( y)**2))/sint) + 1.d3*(1.d0 - xnorm*ynorm*s)**2
    !      return
    !    end function objective
    !    
    !    function trapz( n, x, f) result( t)
    !      integer :: n
    !      real(8) :: x(n)
    !      real(8) :: f(n)
    !      real(8) :: t

    !      integer :: i

    !      t = 0.d0
    !      do i = 1, n-1
    !        t = t + 0.5d0*(f(i) + f(i+1))*(x(i+1) - x(i))
    !      end do
    !      return
    !    end function trapz
    !end subroutine eph_gfun_findqp

    subroutine eph_sfun_nthmoment( n, nfreq, freqs, eval, sigma, mom)
      use m_plotmat
      integer, intent( in)    :: n
      integer, intent( in)    :: nfreq
      real(8), intent( in)    :: freqs( nfreq), eval
      complex(8), intent( in) :: sigma( nfreq)
      real(8), intent( out)   :: mom

      ! parameters
      integer :: ni    ! #interpolation points
      real(8) :: si    ! area unter spectral function in which QPs are searched (si \in (0,1])

      ! working varialbles
      real(8), allocatable :: sfun(:), fi(:)

      ! auxilliary variables
      integer :: i, i1, i2
      real(8) :: r1

      real(8), allocatable :: r1d(:), r2d(:,:)

      !******************************************
      !* SET PARAMETERS
      !******************************************
      si = 1.d0 - 1.d-4
      ni = 2000

      !******************************************
      !* FIND SEARCH INTERVAL
      !******************************************
      allocate( sfun( nfreq), r1d( nfreq), r2d( 3, nfreq))
      call eph_sfun_gen( nfreq, freqs-eval, sigma, sfun)
      sfun = sfun*(abs(freqs**n) + 1.d0)
      call fderiv( -1, nfreq, freqs, abs( sfun), r1d, r2d)
      r1d = r1d/maxval( r1d)

      r1 = (1.d0 - si)*0.5d0
      i1 = minloc( abs( r1d-r1), 1)
      if( (r1d( i1) .gt. r1) .or. (i1 .eq. nfreq)) i1 = i1-1

      r1 = (1.d0 + si)*0.5d0
      i2 = minloc( abs( r1d-r1), 1)
      if( (r1d( i2) .ge. r1) .or. (i2 .eq. nfreq)) i2 = i2-1
      deallocate( r2d)

      !******************************************
      !* GENERATE INTERPOLATION GRID
      !******************************************
      ! we use the spectral function to set up an interpolation frequency grid
      ! that is densly sampled where the peaks are
      r1 = 0.69d0
      sfun = r1*sfun*dble( ni-1)/r1d( nfreq) + (1.d0-r1)*dble( ni-1)/(freqs(i2) - freqs(i1))
      do i = 1, 10
        call savitzkygolay( nfreq, freqs, sfun, 1, 3, 0, r1d)
        sfun = r1d
      end do
      allocate( fi( ni))
      call spacingFromDensity( i2-i1+1, freqs(i1:i2), sfun(i1:i2), ni, fi)
      deallocate( r1d, sfun)

      !******************************************
      !* INTERPOLATE SPECTRAL FUNCTION
      !******************************************
      allocate( sfun( ni), r2d( ni, 2))
      call interp1d( nfreq, freqs, dble(  sigma), ni, fi, r2d(:,1), 'spline')
      call interp1d( nfreq, freqs, aimag( sigma), ni, fi, r2d(:,2), 'spline')
      call eph_sfun_gen( ni, fi-eval, cmplx( r2d(:,1), r2d(:,2), 8), sfun)
      !call writematlab( reshape( cmplx( fi, sfun, 8), (/ni, 1/)), 'SI')
      deallocate( r2d)

      !******************************************
      !* CALCULATE N-TH MOMENT
      !******************************************
      allocate( r1d( ni), r2d( 3, ni))
      sfun = sfun*fi**n
      call fderiv( -1, ni, fi, sfun, r1d, r2d)
      mom = r1d( ni)
      deallocate( sfun, r1d, r2d)

      return
    end subroutine eph_sfun_nthmoment

    subroutine eph_sfun_resample( nfreqi, freqsi, nfreqo, freqso, sigma, sfun)
      use m_plotmat
      integer, intent( in)   :: nfreqi, nfreqo
      real(8), intent( in)   :: freqsi( nfreqi)
      complex(8),intent( in) :: sigma( nfreqi)
      real(8), intent( out)  :: freqso( nfreqo)
      real(8), intent( out)  :: sfun( nfreqo)

      ! auxilliary variables
      integer :: i
      real(8) :: r1

      real(8), allocatable :: r1d(:), r2d(:,:)

      !******************************************
      !* GENERATE INTERPOLATION GRID
      !******************************************
      ! we use the spectral function to set up an interpolation frequency grid
      ! that is densly sampled where the peaks are
      allocate( r1d( nfreqi), r2d( nfreqi, 2))
      call eph_sfun_gen( nfreqi, freqsi, sigma, r2d(:,1))
      r1 = 0.69d0
      r2d(:,1) = r1*r2d(:,1)*dble( nfreqo-1) + (1.d0-r1)*dble( nfreqo-1)/(freqsi( nfreqi) - freqsi(1))
      do i = 1, 10
        call savitzkygolay( nfreqi, freqsi, r2d(:,1), 1, 3, 0, r2d(:,2))
        r2d(:,1) = r2d(:,2)
      end do
      r1d = r2d(:,1)
      deallocate( r2d)
      allocate( r2d( nfreqo, 2))
      call spacingFromDensity( nfreqi, freqsi, r1d, nfreqo, freqso)
      deallocate( r1d)

      !******************************************
      !* INTERPOLATE SPECTRAL FUNCTION
      !******************************************
      call interp1d( nfreqi, freqsi, dble(  sigma), nfreqo, freqso, r2d(:,1), 'spline')
      call interp1d( nfreqi, freqsi, aimag( sigma), nfreqo, freqso, r2d(:,2), 'spline')
      call eph_sfun_gen( nfreqo, freqso, cmplx( r2d(:,1), r2d(:,2), 8), sfun)
      deallocate( r2d)

      return
    end subroutine eph_sfun_resample

    subroutine eph_gfun_resample( nfreqi, freqsi, nfreqo, freqso, sigma, gfun)
      use m_plotmat
      integer, intent( in)      :: nfreqi, nfreqo
      real(8), intent( in)      :: freqsi( nfreqi)
      complex(8),intent( in)    :: sigma( nfreqi)
      real(8), intent( out)     :: freqso( nfreqo)
      complex(8), intent( out)  :: gfun( nfreqo)

      ! auxilliary variables
      integer :: i
      real(8) :: r1

      real(8), allocatable :: r1d(:), r2d(:,:)

      !******************************************
      !* GENERATE INTERPOLATION GRID
      !******************************************
      ! we use the spectral function to set up an interpolation frequency grid
      ! that is densly sampled where the peaks are
      allocate( r1d( nfreqi), r2d( nfreqi, 2))
      call eph_sfun_gen( nfreqi, freqsi, sigma, r2d(:,1))
      r1 = 0.69d0
      r2d(:,1) = r1*r2d(:,1)*dble( nfreqo-1) + (1.d0-r1)*dble( nfreqo-1)/(freqsi( nfreqi) - freqsi(1))
      do i = 1, 10
        call savitzkygolay( nfreqi, freqsi, r2d(:,1), 1, 3, 0, r2d(:,2))
        r2d(:,1) = r2d(:,2)
      end do
      r1d = r2d(:,1)
      deallocate( r2d)
      allocate( r2d( nfreqo, 2))
      call spacingFromDensity( nfreqi, freqsi, r1d, nfreqo, freqso)
      deallocate( r1d)

      !******************************************
      !* INTERPOLATE SPECTRAL FUNCTION
      !******************************************
      call interp1d( nfreqi, freqsi, dble(  sigma), nfreqo, freqso, r2d(:,1), 'spline')
      call interp1d( nfreqi, freqsi, aimag( sigma), nfreqo, freqso, r2d(:,2), 'spline')
      call eph_gfun_gen( nfreqo, freqso, cmplx( r2d(:,1), r2d(:,2), 8), gfun)
      deallocate( r2d)

      return
    end subroutine eph_gfun_resample

    subroutine eph_gfun_findqp( nfreq, freqs, eval0, sigma, nqp, qp)
      use mod_ratfun
      integer, intent( in)     :: nfreq
      integer, intent( inout)  :: nqp
      real(8), intent( in)     :: freqs( nfreq)
      real(8), intent( in)     :: eval0
      complex(8), intent( in)  :: sigma( nfreq)
      complex(8), intent( out) :: qp(2,nqp)

      ! parameter
      integer :: nfreqi  ! number of interpolation frequencies
      real(8) :: sfarea  ! area under spectral function in search interval

      ! working variables
      integer :: level
      real(8) :: escale, eps, err
      real(8), allocatable :: freqsi(:)
      complex(8), allocatable :: gfun(:), gfuni(:)

      ! auxilliary variables
      integer :: i, j, k, i1, i2
      real(8) :: r1, xr(2)

      real(8), allocatable :: r1d(:), r2d(:,:), tmp(:)

      !******************************************
      !* SET PARAMETERS
      !******************************************
      nfreqi = 1000
      sfarea = 0.95d0

      !******************************************
      !* GENERATE INTERPOLATION GRID
      !******************************************
      ! we use the spectral function to set up an interpolation frequency grid
      ! that is densly sampled where the peaks are
      allocate( r1d( nfreq), r2d( nfreq, 2))
      allocate( freqsi( nfreqi))
      call eph_sfun_gen( nfreq, freqs(:)-eval0, sigma, r2d(:,1))
      r1 = 0.69d0
      r2d(:,1) = r1*r2d(:,1)*dble( nfreqi-1) + (1.d0-r1)*dble( nfreqi-1)/(freqs( nfreq) - freqs(1))
      do i = 1, 10
        call savitzkygolay( nfreq, freqs, r2d(:,1), 1, 3, 0, r2d(:,2))
        r2d(:,1) = r2d(:,2)
      end do
      r1d = r2d(:,1)
      deallocate( r2d)
      allocate( r2d( nfreqi, 2))
      call spacingFromDensity( nfreq, freqs(:)-eval0, r1d, nfreqi, freqsi)
      deallocate( r1d)

      !******************************************
      !* INTERPOLATE GREENS FUNCTION
      !******************************************
      allocate( gfun( nfreqi))
      call interp1d( nfreq, freqs(:)-eval0, dble(  sigma), nfreqi, freqsi, r2d(:,1), 'spline')
      call interp1d( nfreq, freqs(:)-eval0, aimag( sigma), nfreqi, freqsi, r2d(:,2), 'spline')
      call eph_gfun_gen( nfreqi, freqsi, cmplx( r2d(:,1), r2d(:,2), 8), gfun)
      deallocate( r2d)

      !******************************************
      !* FIND SEARCH INTERVAL
      !******************************************
      allocate( r1d( nfreqi), r2d( 3, nfreqi), tmp( nfreqi))
      call fderiv( -1, nfreqi, freqsi, aimag( gfun), r1d, r2d)
      r1d = r1d/r1d( nfreqi)

      call fderiv( 1, nfreqi, r1d, freqsi, tmp, r2d)
      i1 = minloc( abs( r1d-(1.d0-sfarea)), 1)
      if( (r1d( i1) .gt. sfarea)) i1 = i1-1
      call interp1d( nfreqi, r1d, tmp, i1, sfarea+r1d(1:i1), r2d(1,1:i1), 'spline')
      call interp1d( i1, abs( aimag( gfun(1:i1)))*r2d(1,1:i1), freqsi(1:i1), 1, (/1.d0/), xr(1), 'spline')
      call interp1d( nfreqi, freqsi, r1d, 1, xr(1), xr(2), 'spline')
      call interp1d( nfreqi, r1d, freqsi, 1, sfarea+xr(2), xr(2), 'spline')
      i1 = minloc( abs( freqsi-xr(1)), 1); if( freqsi(i1) .gt. xr(1)) i1 = i1-1
      i2 = minloc( abs( freqsi-xr(2)), 1); if( freqsi(i2) .lt. xr(2)) i2 = i2+1
      escale = ((i2-i1)*(freqsi(i2)-freqsi(i1))/sum( aimag( gfun(i1:i2))**2))**0.25d0
      freqsi = freqsi/escale
      gfun = gfun*escale
      deallocate( r1d, r2d, tmp)

      !******************************************
      !* FIND RATIONAL INTERPOLANT
      !******************************************
      !do i = 1, nfreqi
      !  write(*,'(3g20.10)') freqsi(i), gfun(i)
      !end do
      !stop
      write(*,'(i)',advance='no') 0
      level = 0

      ! multipole expansion
      if( level == 0) then
        r1 = 0.d0; eps = 1.d-20; j = nqp
        do i = 1, j
          err = eps; k = i
          call ratfun_multipole( nfreqi, freqsi, gfun, err, k, qp(1,1:i), qp(2,1:i))
          if( err < eps) exit
          write(*,'(g20.10)',advance='no') 0.d0
        end do
        j = min( i, j)
        qp(1,1:j) = eval0 + qp(1,1:j)*escale
      else
        ! z0 = e0
        if( level/10 == 1) then
          j = 1
          r1 = eval0
          call interp1d( nfreq, freqs, dble(  sigma), 1, [r1], xr(1), 'spline')
          call interp1d( nfreq, freqs, aimag( sigma), 1, [r1], xr(2), 'spline')
          qp(1,j) = cmplx( xr(1), xr(2), 8)
          qp(2,j) = r1
          write(*,'(g20.10)',advance='no') r1
        ! z0 - e0 = Re Sigma(z0)
        else if( level/10 == 2) then
          j = 0
          do i = 2, nfreq-2
            if( (dble( sigma(i)) - freqs(i) + eval0) * (dble( sigma(i+1)) - freqs(i+1) + eval0) > 0.d0) cycle
            j = j + 1
            call interp1d( 4, dble( sigma((i-1):(i+2)))-freqs((i-1):(i+2)), freqs((i-1):(i+2)), 1, [eval0], xr(1), 'spline')
            r1 = xr(1)
            call interp1d( nfreq, freqs, dble(  sigma), 1, [r1], xr(1), 'spline')
            call interp1d( nfreq, freqs, aimag( sigma), 1, [r1], xr(2), 'spline')
            qp(1,j) = cmplx( xr(1), xr(2), 8)
            qp(2,j) = r1
            write(*,'(g20.10)',advance='no') r1
            if( j == nqp) exit
          end do
        end if
        do i = 1, j
          r1 = dble( qp(2,i))
          ! no renormalization
          if( mod( level, 10) == 1) then
            qp(2,i) = zone
          ! real renormalization
          else if( mod( level, 10) == 2) then
            allocate( r1d( nfreq))
            call savitzkygolay( nfreq, freqs, dble( sigma), 1, 3, 1, r1d)
            call interp1d( nfreq, freqs, r1d, 1, [r1], xr(1), 'spline')
            qp(2,i) = cmplx( 1.d0/(1.d0 - xr(1)), 0.d0, 8)
            deallocate( r1d)
          ! complex renormalization
          else if( mod( level, 10) == 3) then
            allocate( r1d( nfreq))
            call savitzkygolay( nfreq, freqs, dble( sigma), 1, 3, 1, r1d)
            call interp1d( nfreq, freqs, r1d, 1, [r1], xr(1), 'spline')
            call savitzkygolay( nfreq, freqs, aimag( sigma), 1, 3, 1, r1d)
            call interp1d( nfreq, freqs, r1d, 1, [r1], xr(2), 'spline')
            qp(2,i) = zone/cmplx( 1.d0 - xr(1), -xr(2), 8)
            deallocate( r1d)
          end if
          qp(1,i) = r1 + qp(2,i)*(qp(1,i) + eval0 - r1)
        end do
      end if
      do i = j+1, max( 3, nqp)
        write(*,'(g20.10)',advance='no') 0.d0
      end do
      nqp = j

      freqsi = freqsi*escale; gfun = gfun/escale
      allocate( gfuni( nfreqi), r1d( nfreqi), r2d(3,nfreqi))
      gfuni(:) = zzero
      do i = 1, nqp
        gfuni(:) = gfuni(:) + qp(2,i) / (freqsi(:) + eval0 - qp(1,i))
      end do
      call fderiv( -1, nfreqi, freqsi, abs( gfun - gfuni)**2, r1d, r2d)
      err = r1d( nfreqi)
      call fderiv( -1, nfreqi, freqsi, abs( gfun)**2, r1d, r2d)
      err = sqrt( err/r1d( nfreqi))
      eps = maxval( abs( gfun - gfuni))/sqrt( r1d( nfreqi))
      write(*,'(g20.10)') err
      !do i = 1, nfreqi
      !  write(*,'(5g20.10)') freqsi(i), gfun(i), gfuni(i)
      !end do
      deallocate( freqsi, gfun, gfuni, r1d, r2d)

      return
    end subroutine eph_gfun_findqp

    subroutine eph_congfun( nfreqi, freqsi, nfreqo, sigma)
      use m_plotmat
      use mod_schlessinger
      integer, intent( in)   :: nfreqi, nfreqo
      real(8), intent( in)   :: freqsi( nfreqi)
      complex(8),intent( in) :: sigma( nfreqi)

      ! parameter
      integer :: nir, nii
      real(8) :: sa, ilim(2)

      ! working variables
      real(8) :: escale
      real(8), allocatable :: freqso(:)
      complex(8), allocatable :: gfun(:), zi(:)

      ! auxilliary variables
      integer :: i, j, k, i1, i2
      real(8) :: r1, xr(2)

      real(8), allocatable :: r1d(:), r2d(:,:), tmp(:)

      sa = 0.95d0
      nir = 501
      ilim = (/-0.15, 0.05/)

      !******************************************
      !* GENERATE INTERPOLATION GRID
      !******************************************
      ! we use the spectral function to set up an interpolation frequency grid
      ! that is densly sampled where the peaks are
      allocate( r1d( nfreqi), r2d( nfreqi, 2))
      allocate( freqso( nfreqo))
      call eph_sfun_gen( nfreqi, freqsi, sigma, r2d(:,1))
      r1 = 0.69d0
      r2d(:,1) = r1*r2d(:,1)*dble( nfreqo-1) + (1.d0-r1)*dble( nfreqo-1)/(freqsi( nfreqi) - freqsi(1))
      do i = 1, 10
        call savitzkygolay( nfreqi, freqsi, r2d(:,1), 1, 3, 0, r2d(:,2))
        r2d(:,1) = r2d(:,2)
      end do
      r1d = r2d(:,1)
      deallocate( r2d)
      allocate( r2d( nfreqo, 2))
      call spacingFromDensity( nfreqi, freqsi, r1d, nfreqo, freqso)
      deallocate( r1d)

      !******************************************
      !* INTERPOLATE GREENS FUNCTION
      !******************************************
      allocate( gfun( nfreqo))
      call interp1d( nfreqi, freqsi, dble(  sigma), nfreqo, freqso, r2d(:,1), 'spline')
      call interp1d( nfreqi, freqsi, aimag( sigma), nfreqo, freqso, r2d(:,2), 'spline')
      call eph_gfun_gen( nfreqo, freqso, cmplx( r2d(:,1), r2d(:,2), 8), gfun)
      deallocate( r2d)

      !******************************************
      !* FIND SEARCH INTERVAL
      !******************************************
      allocate( r1d( nfreqo), r2d( 3, nfreqo), tmp( nfreqo))
      call fderiv( -1, nfreqo, freqso, aimag( gfun), r1d, r2d)
      r1d = r1d/r1d( nfreqo)

      call fderiv( 1, nfreqo, r1d, freqso, tmp, r2d)
      i1 = minloc( abs( r1d-(1.d0-sa)), 1)
      if( (r1d( i1) .gt. sa)) i1 = i1-1
      call interp1d( nfreqo, r1d, tmp, i1, sa+r1d(1:i1), r2d(1,1:i1), 'spline')
      call interp1d( i1, abs( aimag( gfun(1:i1)))*r2d(1,1:i1), freqso(1:i1), 1, (/1.d0/), xr(1), 'spline')
      call interp1d( nfreqo, freqso, r1d, 1, xr(1), xr(2), 'spline')
      call interp1d( nfreqo, r1d, freqso, 1, sa+xr(2), xr(2), 'spline')
      i1 = minloc( abs( freqso-xr(1)), 1); if( freqso(i1) .gt. xr(1)) i1 = i1-1
      i2 = minloc( abs( freqso-xr(2)), 1); if( freqso(i2) .lt. xr(2)) i2 = i2+1
      escale = ((i2-i1)*(freqso(i2)-freqso(i1))/sum( aimag( gfun(i1:i2))**2))**0.25d0
      freqso = freqso/escale
      deallocate( r2d, tmp)

      !******************************************
      !* SETUP SCHLESSINGER INTERPOLANT
      !******************************************
      call schlessinger_init( i2-i1+1, cmplx( freqso(i1:i2), 0.d0, 8), gfun(i1:i2)*escale, 1.d-2)
      deallocate( gfun)

      !******************************************
      !* INTERPOLATE IN COMPLEX PLANE
      !******************************************
      nii = nir*(ilim(2)-ilim(1))/(freqso(i2)-freqso(i1))
      nii = nir
      allocate( zi( nir*nii), gfun( nir*nii))
      k = 0
      do j = 1, nir
        do i = 1, nii
          k = k + 1
          zi(k) = cmplx( freqso(i1)+(freqso(i2)-freqso(i1))*(j-1)/(nir-1), ilim(1)+(ilim(2)-ilim(1))*(i-1)/(nii-1), 8)
        end do
      end do
      call schlessinger_interp( nfreqo, nir*nii, zi, gfun)
      call writematlab( reshape( zi, (/nii, nir/)), 'GINTERPZ')
      call writematlab( reshape( gfun, (/nii, nir/)), 'GINTERPG')
      deallocate( zi, gfun)
      
      return
    end subroutine eph_congfun

    subroutine eph_sfun_output( nfreq, freqs, fst, lst, ntemp, temps, nppt, sfun)
      use m_getunit
      use modmpi
      integer, intent( in) :: nfreq, fst, lst, ntemp, nppt
      real(8), intent( in) :: freqs( nfreq, nppt), temps( ntemp)
      real(8), intent( in) :: sfun( nfreq, fst:lst, ntemp, nppt)

      integer :: ip, ist, iw, it, un
      character(256) :: fname

      if( mpiglobal%rank .ne. 0) return

      do it = 1, ntemp
        write( fname, '("EPH_SFUN_Q",i2.2,"_T",i4.4,"_S",i3.3,".DAT")'), eph_qset%ngridk(1), nint( temps( it)), nint( eph_eta*h2ev*1.d3)
        call getunit( un)
        open( un, file=trim( fname), action='write', form='formatted')
        do ip = 1, nppt
          do iw = 1, nfreq
            write( un, '(2g20.10e3)', advance='no') freqs( iw, ip), sum( sfun( iw, :, it, ip))
            do ist = fst, lst
              write( un, '(g20.10e3)', advance='no') sfun( iw, ist, it, ip)
            end do
            write( un, *)
          end do
          write( un, *)
          write( un, *)
        end do
        close( un)
      end do
      return
    end subroutine eph_sfun_output

    subroutine eph_sfun_output_resampled( ip, nfreqi, freqsi, nfreqo, fst, lst, ntemp, temps, sigma)
      use m_getunit
      use modmpi
      integer,    intent( in) :: ip, nfreqi, nfreqo, fst, lst, ntemp
      real(8),    intent( in) :: freqsi( nfreqi), temps( ntemp)
      complex(8), intent( in) :: sigma(:,:,:,:)

      integer :: ist, jst, i, j, iw, it, un
      real(8) :: freqso( nfreqo, fst:lst), sfun( nfreqo, fst:lst)
      character(256) :: fname

      if( mpiglobal%rank .ne. 0) return

      if( eph_diag) then
        i = 1; j = 1
      else
        i = fst; j = lst
      end if

      do it = 1, ntemp
        write( fname, '("EPH_SFUN_RSMPLD_P",i3.3,"_Q",i2.2,"_T",i4.4,"_S",i3.3,".DAT")'), ip, eph_qset%ngridk(1), nint( temps( it)), nint( eph_eta*h2ev*1.d3)
        call getunit( un)
        open( un, file=trim( fname), action='write', form='formatted')
        do jst = i, j
          do ist = fst, lst
            call eph_sfun_resample( nfreqi, freqsi-eph_evalp( ist, ip), nfreqo, freqso(:,ist), sigma( :, ist, jst, it), sfun(:,ist))
          end do
          do iw = 1, nfreqo
            do ist = fst, lst
              write( un, '(2g20.10e3)', advance='no') freqso( iw, ist)+eph_evalp( ist, ip), sfun( iw, ist)
            end do
            write( un, *)
          end do
          write( un, *)
          write( un, *)
        end do
        close( un)
      end do
      return
    end subroutine eph_sfun_output_resampled

    subroutine eph_gfun_output_resampled( ip, nfreqi, freqsi, nfreqo, fst, lst, ntemp, temps, sigma)
      use m_getunit
      use modmpi
      integer,    intent( in) :: ip, nfreqi, nfreqo, fst, lst, ntemp
      real(8),    intent( in) :: freqsi( nfreqi), temps( ntemp)
      complex(8), intent( in) :: sigma(:,:,:,:)

      integer :: ist, jst, i, j, iw, it, un
      real(8) :: freqso( nfreqo, fst:lst)
      complex(8) :: gfun( nfreqo, fst:lst)
      character(256) :: fname

      if( mpiglobal%rank .ne. 0) return

      if( eph_diag) then
        i = 1; j = 1
      else
        i = fst; j = lst
      end if

      do it = 1, ntemp
        write( fname, '("EPH_GFUN_RSMPLD_P",i3.3,"_Q",i2.2,"_T",i4.4,"_S",i3.3,".DAT")'), ip, eph_qset%ngridk(1), nint( temps( it)), nint( eph_eta*h2ev*1.d3)
        call getunit( un)
        open( un, file=trim( fname), action='write', form='formatted')
        do jst = i, j
          do ist = fst, lst
            call eph_gfun_resample( nfreqi, freqsi-eph_evalp( ist, ip), nfreqo, freqso(:,ist), sigma( :, ist, jst, it), gfun(:,ist))
          end do
          do iw = 1, nfreqo
            do ist = fst, lst
              write( un, '(3g20.10e3)', advance='no') freqso( iw, ist)+eph_evalp( ist, ip), gfun( iw, ist)
            end do
            write( un, *)
          end do
          write( un, *)
          write( un, *)
        end do
        close( un)
      end do
      return
    end subroutine eph_gfun_output_resampled
end module mod_eph_sfun
