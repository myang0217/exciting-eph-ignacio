module mod_eph
  use mod_eph_variables
  use mod_eph_helper
  use mod_eph_electrons
  use mod_eph_phonons
  use mod_eph_ephmat
  use mod_eph_sigma
  use mod_eph_sfun
  use mod_eph_a2F
  use mod_wannier
  use mod_wannier_interpolate
  use mod_wannier_util
  use mod_eph_scroot, only: eph_scroot

  implicit none

  logical :: reduce = .false.

! methods
  contains
    !
    subroutine eph_init
      use m_getunit
      integer :: iw, ip, i, j, k, ik, isym
      real(8) :: vr(3)
      character(256) :: fname
      logical :: exist

      real(8), allocatable :: x(:), fr(:), fi(:), gr(:)

      ! read parameters from input
      vr = input%eph%tempset
      if( norm2( vr) .gt. 1.d-16) then
        eph_ntemp = floor( (vr(2)-vr(1))/vr(3)) + 1
        if( allocated( eph_temps)) deallocate( eph_temps)
        allocate( eph_temps( eph_ntemp))
        do j = 1, eph_ntemp
          eph_temps(j) = max( 1.d-16, vr(1) + dble( j-1)*vr(3))
        end do
      else
        eph_ntemp = 1
        if( allocated( eph_temps)) deallocate( eph_temps)
        allocate( eph_temps( eph_ntemp))
        eph_temps = max( 1.d-16, abs( input%eph%temp))
      end if
      write(*,*) 'NTEMP', eph_ntemp
      eph_eta          = input%eph%eta/h2ev                 ! complex infinitesimal in Lehmann representation
      eph_scissor      = input%eph%scissor
      eph_nmode        = 3*natmtot
      eph_nqpmax       = 5

      write( fname, '("EPSINFZSTAR.OUT")')
      inquire( file=trim( fname), exist=exist)
      if( exist) then
        allocate( eph_zstar( 3, 3, natmtot))
        allocate( eph_epsinf(3,3))
        call eph_read_epsinf_zstar( fname, eph_epsinf, eph_zstar)
        eph_polar = .true.
      end if

      ! generate G-vectors
      eph_Gset = wf_Gset

      ! electron k-grid
      eph_kset_el = wf_kset
      eph_Gkset_el = wf_Gkset

      ! Auxiliary k-grid for testing
      Call generate_k_vectors(eph_kset_aux, eph_kset_el%bvec, &
                             (/ 1, 1, 1 /), &
                             (/0.d0, 0.d0, 0.d0/), &
                             .false. , .false.) 

      ! phonon k-grid
      if( .not. eph_einstein .and. .not. eph_debye) then
        if( eph_dfpt) then
          call generate_k_vectors( eph_kset_ph, eph_kset_el%bvec, input%phonons%ngridq, (/0.d0, 0.d0, 0.d0/), .false., .false.)
        else
          call generate_k_vectors( eph_kset_ph, eph_kset_el%bvec, input%phonons%ngridq, (/0.d0, 0.d0, 0.d0/), input%phonons%reduceq, .false.)
        end if
      end if

      ! Auxiliary k-grid for testing
      Call generate_k_vectors(eph_kset_ph_aux, eph_kset_el%bvec, &
                             input%phonons%ngridq, &
                             (/0.d0, 0.d0, 0.d0/), &
                             .false. , .false.) 

      ! bzint k-grid (reducible)
      call generate_k_vectors( eph_qset, eph_kset_el%bvec, input%eph%ngridq, input%eph%vqloff, reduce, .false.)

      ! target k-set (path or grid)
      call eph_gen_pset

      !write(*,*) wf_npp1d, tmp_kset%nkpt

      ! allocate energy/frequency grids
      eph_nfreq = input%eph%nfreq
      allocate( eph_freqs( eph_nfreq, eph_pset%nkpt))
      
      ! for printing matrix elements
      eph_ist = input%eph%ibeph
      eph_jst = input%eph%nbeph

      ! set band ranges
      ! Note: We use states expressed in the Wannier functions basis
      eph_nst = wf_nwf
      eph_fst = 1       ! in Wannier interpolated states
      eph_lst = eph_nst ! in Wannier interpolated states

      ! change settings for Einstein mode phonon
      if( eph_einstein .or. eph_debye) then
        eph_polar = .false.
        eph_nmode = 1
        eph_fst = 1; eph_lst = 1; eph_nst = 1

        eph_einstein_phfreq = 0.3d0/h2ev
        eph_einstein_ephmat = 0.5d0*eph_einstein_phfreq
        eph_einstein_eldisp = 5.d0*eph_einstein_phfreq/norm2( 0.5d0*sum( eph_kset_el%bvec, 2))**2
        eph_efermi = 3.d0*eph_einstein_phfreq

        write(*,'("TEMPERATURE:                 ",f13.6)') eph_temps(1)*kboltz*h2ev
        write(*,'("EINSTEIN MODE FREQUENCY:     ",f13.6)') eph_einstein_phfreq*h2ev
        write(*,'("EINSTEIN COUPLING STRENGTH:  ",f13.6)') eph_einstein_ephmat*h2ev
        write(*,'("EINSTEIN ELECTRON DISPERSION:",f13.6)') eph_einstein_eldisp*h2ev
        write(*,'("EINSTEIN FERMI ENERGY:       ",f13.6)') eph_efermi*h2ev
      end if 
      return
    end subroutine eph_init

!============================================================================
!============================================================================

    subroutine eph_do
      use m_getunit

      ! tasks
      logical :: task_gensigma, task_genspecfun, task_writeephmat, task_phdisp, task_eldisp, task_findqp, task_gena2F, task_congfun, task_eldos, task_ephshort

      ! parameters
      integer :: nqpmax
      real(8) :: qpeps

      ! working variables
      integer :: ip, iq, is, ia, ias, imode, ist, jst, nst, iw, it, nqp
      real(8) :: qpdiag(2), mom
      logical :: success

      real(8), allocatable :: occ(:,:), dos(:)
      complex(8), allocatable :: dynk(:,:,:), dynq(:,:,:), ev(:,:), umnp(:,:,:), qp(:,:), sig0(:,:,:)

      ! auxilliary variables
      integer :: un
      real(8) :: r1
      character(256) :: fname

      task_ephshort    = .true.
      task_gensigma    = .false.
      task_genspecfun  = .false.
      task_writeephmat = .false.
      task_phdisp      = .true.
      task_eldisp      = .true.
      task_eldos       = .false.
      task_findqp      = .false.
      task_gena2F      = .false.
      task_congfun     = .false.
      qpeps = 0.05d0
      nqpmax = 2

      nst = eph_nst
      if( .not. eph_diag) nst = nst*(nst+1)/2

      ! find VBM and CBM
      if( .not. eph_einstein .and. .not. eph_debye) then
        eph_efermi = input%eph%efermi
        if( eph_efermi .eq. 0.d0) call eph_electrons_efermi( eph_efermi)
        write(*,'("EFERMI: ",f26.16)') eph_efermi
      end if

      !******************************************
      !* GET EIGENVALUES ON TARGET SET
      !******************************************
      if( allocated( eph_evalp)) deallocate( eph_evalp)
      allocate( eph_evalp( eph_fst:eph_lst, eph_pset%nkpt))
      allocate( umnp( eph_fst:eph_lst, eph_fst:eph_lst, eph_pset%nkpt))
      ! store Wannier transformation matrices on p-set for polar coupling
      call eph_electrons_interpolate( eph_pset, eph_evalp, evec=umnp)

      !******************************************
      !* COMPUTE ELECTRONIC DOS
      !******************************************
      if( task_eldos) then
        ip = 1
        ! get phonon frequencies and eigenvectors on integration grid
        if( allocated( eph_phfreq)) deallocate( eph_phfreq)
        allocate( eph_phfreq( eph_nmode, eph_qset%nkpt))
        if( allocated( eph_phevec)) deallocate( eph_phevec)
        allocate( eph_phevec( 3, natmtot, eph_nmode, eph_qset%nkpt))
        if( eph_polar) then
          call eph_phonons_interpolate( eph_kset_ph, eph_qset, eph_phfreq, eph_phevec, epsinf=eph_epsinf, zstar=eph_zstar)
        else
          call eph_phonons_interpolate( eph_kset_ph, eph_qset, eph_phfreq, eph_phevec)
        end if

        ! generate p+q set and tetrahedra
        call eph_init_ppoint( ip)

        ! get electron energies and Wannier transformation matrices on p+q set
        if( allocated( eph_evalqp)) deallocate( eph_evalqp)
        allocate( eph_evalqp( eph_fst:eph_lst, eph_qpset%nkpt))
        call eph_electrons_interpolate( eph_qpset, eph_evalqp, serial=.true.)

        ! set frequencies
        call eph_genfreqs( ip)

        ! get DOS
        allocate( dos( eph_nfreq))
        call eph_electrons_dos( eph_nfreq, eph_freqs(:,ip), dos)

        do is = 1, eph_nfreq
          write(*,'(2g20.10)') eph_freqs( is, ip), dos( is)
        end do
      end if

      !******************************************
      !* COMPUTE SELFENERGY
      !******************************************
      ! do preparation if needed
      if( task_gensigma) then
        ! get phonon frequencies and eigenvectors on integration grid
        if( allocated( eph_phfreq)) deallocate( eph_phfreq)
        allocate( eph_phfreq( eph_nmode, eph_qset%nkpt))
        if( allocated( eph_phevec)) deallocate( eph_phevec)
        allocate( eph_phevec( 3, natmtot, eph_nmode, eph_qset%nkpt))
        if( allocated( eph_sigmafm)) deallocate( eph_sigmafm)
        if( eph_diag) then
          allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, 1, eph_ntemp))
        else
          allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, eph_fst:eph_lst, eph_ntemp))
        end if
        eph_sigmafm = zzero
        if( eph_polar) then
          call eph_phonons_interpolate( eph_kset_ph, eph_qset, eph_phfreq, eph_phevec, epsinf=eph_epsinf, zstar=eph_zstar)
        else
          call eph_phonons_interpolate( eph_kset_ph, eph_qset, eph_phfreq, eph_phevec)
        end if

        allocate( sig0( eph_nfreq, nst, eph_ntemp))

        ! calculate self-energy for each p-point
        do ip = firstofset( mpiglobal%rank, eph_pset%nkpt), lastofset( mpiglobal%rank, eph_pset%nkpt)
          write(*,'("P-POINT",i,3f16.6)') ip, eph_pset%vkl(:,ip)
          ! generate p+q set and tetrahedra
          call eph_init_ppoint( ip)

          ! get electron energies and Wannier transformation matrices on p+q set
          if( allocated( eph_evalqp)) deallocate( eph_evalqp)
          allocate( eph_evalqp( eph_fst:eph_lst, eph_qpset%nkpt))
          call eph_electrons_interpolate( eph_qpset, eph_evalqp, serial=.true.)

          ! set frequencies
          call eph_genfreqs( ip)

          ! generate eph matrix elements
          if( allocated( eph_ephmat)) deallocate( eph_ephmat)
          allocate( eph_ephmat( eph_fst:eph_lst, eph_fst:eph_lst, eph_nmode, eph_qpset%nkpt))
          eph_ephmat = zzero
          if( eph_polar) call eph_ephmat_gen_lr( eph_qset, eph_qpset, eph_iqp2iq, eph_phfreq, eph_phevec, &
                                                 eph_epsinf, eph_zstar, wfint_transform, umnp(:,:,ip), zone, eph_ephmat)
          if( eph_einstein) eph_ephmat = cmplx( eph_einstein_ephmat, 0.d0, 8)
          if( eph_debye) eph_ephmat = cmplx( eph_debye_ephmat, 0.d0, 8)

          ! calculate self-energy
          call eph_sigma_fm0( eph_nfreq, eph_freqs( :, ip), eph_ntemp, eph_temps, sig0, diag=eph_diag)
          call eph_sigma_fm0tofile( ip, eph_nfreq, eph_freqs( :, ip), eph_ntemp, eph_temps, nst, sig0, success)
          !call eph_sigma_genim_naive( eph_nfreq, eph_freqs(:,ip), eph_ntemp, eph_temps, eph_sigmafm)
          !call eph_sigma_output( ip, eph_nfreq, eph_freqs(:,ip), eph_fst, eph_lst, eph_ntemp, eph_temps, eph_sigmafm)
        end do
        deallocate( sig0)
      end if

      !******************************************
      !* COMPUTE SPECTRAL FUNCTION
      !******************************************
      if( task_genspecfun) then
        ! allocate self energy
        if( allocated( eph_specfun_band)) deallocate( eph_specfun_band)
        allocate( eph_specfun_band( eph_nfreq, eph_fst:eph_lst, eph_ntemp, eph_pset%nkpt))
        if( allocated( eph_sigmafm)) deallocate( eph_sigmafm)
        if( eph_diag) then
          allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, 1, eph_ntemp))
        else
          allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, eph_fst:eph_lst, eph_ntemp))
        end if
        eph_sigmafm = zzero
        eph_freqs = 0.d0

        allocate( sig0( eph_nfreq, eph_fst:eph_lst, eph_ntemp))
  
        jst = 1
        if( eph_diag) jst = 1

        ! loop over target points
        do ip = firstofset( mpiglobal%rank, eph_pset%nkpt), lastofset( mpiglobal%rank, eph_pset%nkpt)
          write(*,'("P-POINT",i,6f16.6)') ip, eph_pset%vkl(:,ip)
          call eph_sigma_get( ip, eph_sigmafm)
          sig0 = eph_sigmafm( :, :, jst, :)
  
#ifdef USEOMP
!$omp parallel default( shared) private( it, ist)
!$omp do collapse(2)
#endif
          do it = 1, eph_ntemp
            do ist = eph_fst, eph_lst
              call eph_sfun_gen( eph_nfreq, eph_freqs( :, ip)-eph_evalp( ist, ip), sig0(:,ist,it), eph_specfun_band( :, ist, it, ip))
              call eph_sfun_nthmoment( 0, eph_nfreq, eph_freqs( :, ip), eph_evalp( ist, ip), sig0(:,ist,it), r1)
              write(*,'(i,f26.16)') ist, r1
              !call eph_sfun_findqp( eph_nfreq, eph_freqs( :, ip)-eph_evalp( ist, ip), eph_sigmafm( :, ist, ip), nqpmax, qpeps, nqp, qp, qpdiag)
              !write(*,'(f4.2,f4.1)') qpdiag(1), qpdiag(2)*1.d2
            end do
          end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
          call eph_sigma_output( ip, eph_nfreq, eph_freqs(:,ip), eph_fst, eph_lst, eph_ntemp, eph_temps, eph_sigmafm)
          call eph_sfun_output_resampled( ip, eph_nfreq, eph_freqs(:,ip), eph_nfreq, eph_fst, eph_lst, eph_ntemp, eph_temps, eph_sigmafm)
          !call eph_gfun_output_resampled( ip, eph_nfreq, eph_freqs(:,ip), eph_nfreq, eph_fst, eph_lst, eph_ntemp, eph_temps, eph_sigmafm)
        end do
        !call eph_sfun_output( eph_nfreq, eph_freqs, eph_fst, eph_lst, eph_ntemp, eph_temps, eph_pset%nkpt, eph_specfun_band)
      end if

      !******************************************
      !* CONTINUE GREENS FUNCTION
      !******************************************
      if( task_congfun) then
        if( allocated( eph_sigmafm)) deallocate( eph_sigmafm)
        if( eph_diag) then
          allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, 1, eph_ntemp))
        else
          allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, eph_fst:eph_lst, eph_ntemp))
        end if
        eph_sigmafm = zzero
        eph_freqs = 0.d0

        ip = 1; ist = 1; it = 1;
        call eph_sigma_get( ip, eph_sigmafm)
        call eph_congfun( eph_nfreq, eph_freqs(:,ip)-eph_evalp( ist, ip), eph_nfreq, eph_sigmafm(:,ist,1,it))

        !do ist = eph_fst, eph_lst
        !  call eph_sfun_nthmoment( 0, eph_nfreq, eph_freqs, eph_evalp( ist, ip), eph_sigmafm(:,ist,it,ip), mom)
        !  write(*,'(i,2f13.6)') ist, mom, eph_evalp( ist, ip)
        !end do
      end if

      !******************************************
      !* FIND QUASI PARTICLE ENERGIES
      !******************************************
      if( task_findqp) then
        allocate( qp( 2, nqpmax))
        if( allocated( eph_evalp_qp)) deallocate( eph_evalp_qp)
        allocate( eph_evalp_qp( eph_nqpmax, eph_fst:eph_lst, eph_pset%nkpt))
        if( allocated( eph_sigmafm)) deallocate( eph_sigmafm)
        if( eph_diag) then
          allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, 1, eph_ntemp))
        else
          allocate( eph_sigmafm( eph_nfreq, eph_fst:eph_lst, eph_fst:eph_lst, eph_ntemp))
        end if
        eph_sigmafm = zzero
        eph_freqs = 0.d0
        
        do ip = firstofset( mpiglobal%rank, eph_pset%nkpt), lastofset( mpiglobal%rank, eph_pset%nkpt)
          write(*,'("# P-POINT",i,3f16.6)') ip, eph_pset%vkl(:,ip)
          call eph_sigma_get( ip, eph_sigmafm)
          do it = 1, eph_ntemp
            do ist = eph_fst, eph_lst
              !write(*,*) ist, '-------------------'
              !write(*,'(2i,f26.16)') ip, ist, eph_evalp( ist, ip)
              nqp = nqpmax; qp = zzero;
              call eph_gfun_findqp( eph_nfreq, eph_freqs( :, ip), eph_evalp( ist, ip), eph_sigmafm(:,ist,1,it), nqp, qp)
              do is = 1, nqpmax
                write(*,'(i,4g20.10)') is, qp(:,is)
              end do
            end do
          end do
          write(*,*)
        end do
      end if

      !******************************************
      !* WRITE COUPLING MATRIX ELEMENTS
      !******************************************
      if( task_writeephmat) then
        call eph_ephmat_output( (/0.d0, 0.d0, 0.d0/), eph_fst-1+5)
      end if

      !*********************************************
      !* WRITE COUPLING MATRIX ELEMENTS SHORT RANGE
      !*********************************************
      if( task_ephshort) then
        if( allocated( eph_ephmat_kq)) deallocate( eph_ephmat_kq)
        allocate( eph_ephmat_kq( wf_fst:wf_lst, wf_fst:wf_lst, eph_nmode, eph_kset_el%nkptnr, eph_kset_ph%nkptnr))
        call eph_ephmat_ephmatrix(eph_kset_el, eph_kset_ph, eph_ephmat_kq)
        if( allocated( eph_ephmat_kp)) deallocate( eph_ephmat_kp)
        allocate( eph_ephmat_kp( wf_nwf, wf_nwf, eph_nmode, eph_kset_aux%nkptnr, eph_pset%nkptnr))
        call eph_ephmat_interpolate(eph_kset_el, eph_kset_ph, eph_kset_aux, eph_pset, eph_ephmat_kq, eph_ephmat_kp) 
      end if

      !******************************************
      !* WRITE PHONON DISPERSION
      !******************************************
      if( task_phdisp) then
        call eph_phonons_output
      end if

      !******************************************
      !* WRITE ELECTRON DISPERSION
      !******************************************
      if( task_eldisp) then
        call eph_electrons_output
      end if

      !******************************************
      !* COMPUTE ELIASHBERG FUNCTION
      !******************************************
      if( task_gena2F) then
        ! get phonon frequencies and eigenvectors on integration grid
        if( allocated( eph_phfreq)) deallocate( eph_phfreq)
        allocate( eph_phfreq( eph_nmode, eph_qset%nkpt))
        if( allocated( eph_phevec)) deallocate( eph_phevec)
        allocate( eph_phevec( 3, natmtot, eph_nmode, eph_qset%nkpt))
        if( eph_polar) then
          call eph_phonons_interpolate( eph_kset_ph, eph_qset, eph_phfreq, eph_phevec, epsinf=eph_epsinf, zstar=eph_zstar)
        else
          call eph_phonons_interpolate( eph_kset_ph, eph_qset, eph_phfreq, eph_phevec)
        end if

        eph_a2F_nefreq = input%eph%eliashbergfun%nefreq
        eph_a2F_npfreq = input%eph%eliashbergfun%npfreq

        if( allocated( eph_a2F)) deallocate( eph_a2F)
        allocate( eph_a2F( eph_a2F_npfreq, eph_fst:eph_lst, eph_fst:eph_lst, eph_pset%nkpt))

        ! set the phonon frequencies at which a2F is calculated
        call eph_a2F_genfreqs

        do ip = 1, 1
          ! generate p+q set and tetrahedra
          call eph_init_ppoint( ip)

          ! get electron energies and Wannier transformation matrices on p+q set
          allocate( eph_evalqp( eph_fst:eph_lst, eph_qpset%nkpt))
          call eph_electrons_interpolate( eph_qpset, eph_evalqp, serial=.true.)

          ! generate eph matrix elements
          allocate( eph_ephmat( eph_fst:eph_lst, eph_fst:eph_lst, eph_nmode, eph_qpset%nkpt))
          eph_ephmat = zzero
          if( eph_polar) call eph_ephmat_gen_lr( eph_qset, eph_qpset, eph_iqp2iq, eph_phfreq, eph_phevec, &
                                                 eph_epsinf, eph_zstar, wfint_transform, umnp(:,:,ip), zone, eph_ephmat)

          ! calculate Eliashberg function
          call eph_a2F_gen( eph_nst, eph_evalp(:,ip), eph_a2F_npfreq, eph_a2F_pfreqs, eph_a2F(:,:,:,ip))
          write(*,*) 'done'
        end do

        ip = 1
        call eph_a2F_output( eph_nst, eph_evalp(:,ip), eph_a2F_npfreq, eph_a2F_pfreqs, eph_a2F(:,:,:,ip))

      end if

      return
    end subroutine eph_do

!============================================================================
!============================================================================


!============================================================================
!============================================================================

    !subroutine eph_gen_eliashbergfun( evalp, ip)
    !  use m_getunit
    !  use m_lorentzfit
    !  real(8) :: evalp( eph_fst:eph_lst) ! eigenvalues at target point p (shifted by Fermi energy)
    !  integer :: ip                      ! index of target point p

    !  integer :: imode, ist, jst, ie, iw, iq, iqp, un, Nox, Noy, ns
    !  real(8) :: fac, t0, t1, ti, tp, te, ts, tio, e
    !  real(8) :: wgtp( eph_nmode, eph_qset%nkpt, eph_a2f_pfset%nomeg), wq( eph_nmode, eph_qset%nkpt)
    !  real(8) :: wgte( eph_nst, eph_qset%nkpt, eph_a2f_efset%nomeg), eqp( eph_nst, eph_qset%nkpt)
    !  real(8) :: auxmat( eph_a2f_efset%nomeg, eph_nmode, eph_qset%nkpt)
    !  real(8), allocatable :: px(:,:), py(:,:), c(:,:), a2f(:,:), a2fs(:,:)
    !  character( 256) :: fname

    !  if( .not. allocated( eph_eliashbergfun)) then
    !    allocate( eph_eliashbergfun( eph_a2f_Nbe+2, eph_a2f_Nbp+2, eph_fst:eph_lst, eph_pset%nkpt))
    !    eph_eliashbergfun = 0.d0
    !  end if

    !  do iq = 1, eph_qset%nkpt
    !    wq( :, iq) = eph_phfreq( :, iq)
    !    iqp = eph_iq2iqp( iq)        ! from non-reduced q-set to non-reduced q+p-set
    !    !iqp = eph_qqpset%kqmtset%ik2ikp( iqp)    ! from non-reduced q+p-set to reduced q+p-set
    !    eqp( :, iq) = eph_evalqp( :, iqp)
    !  end do

    !  write(*,'("nkpt:",2i)') eph_qset%nkpt, eph_qset%nkptnr

    !  call timesec( t1)
    !  call opt_tetra_init( eph_tetra, eph_qset, tetra_type=2, reduce=.true.)
    !  call timesec( t0)
    !  ti = t0 - t1
    !  call opt_tetra_int( eph_tetra, 2, eph_qset%nkpt, eph_nmode, wq, eph_a2f_pfset%freqs, eph_a2f_pfset%nomeg, wgtp)
    !  call timesec( t1)
    !  tp = t1 - t0
    !  call opt_tetra_int( eph_tetra, 2, eph_qset%nkpt, eph_nst, eqp, eph_a2f_efset%freqs, eph_a2f_efset%nomeg, wgte)
    !  !wgte = wgte*2.d0*eph_qset%nkpt
    !  do iq = 1, eph_qset%nkpt
    !    wgte( :, iq, :) = 2.d0*wgte( :, iq, :)/eph_qset%wkpt(iq) ! 2 for spin
    !  end do
    !  call timesec( t0)
    !  te = t0 - t1

    !  allocate( a2f( eph_a2f_efset%nomeg, eph_a2f_pfset%nomeg))
    !  allocate( a2fs( eph_a2f_efset%nomeg, eph_a2f_pfset%nomeg))
    !  write(*,*) 'start loop'
    !  do ist = eph_fst, eph_lst
    !    auxmat = 0.d0
    !    a2f = 0.d0
    !    do iq = 1, eph_qset%nkpt
    !      !iqp = eph_qqpset%ik2ikqmt_nr( iq)        ! from non-reduced q-set to non-reduced q+p-set
    !      !iqp = eph_qqpset%kqmtset%ik2ikp( iqp)    ! from non-reduced q+p-set to reduced q+p-set
    !      ! CHECK THIS IF NEEDED
    !      iqp = eph_iq2iqp( iq)
    !      do imode = 1, eph_nmode
    !        do ie = 1, eph_a2f_efset%nomeg
    !          do jst = eph_fst, eph_lst
    !            auxmat( ie, imode, iq) = auxmat( ie, imode, iq) + abs( eph_ephmat( ist, jst, imode, iqp))**2*wgte( jst-eph_fst+1, iq, ie)
    !          end do
    !        end do
    !      end do
    !    end do
    !    
    !    call dgemm( 'n', 'n', eph_a2f_efset%nomeg, eph_a2f_pfset%nomeg, eph_nmode*eph_qset%nkpt, 1.d0, &
    !           auxmat, eph_a2f_efset%nomeg, &
    !           wgtp, eph_nmode*eph_qset%nkpt, 0.d0, &
    !           a2f, eph_a2f_efset%nomeg)

    !    write( fname, '("ELIASHBERG_P",i3.3,"_B",i3.3)') ip, ist
    !    call writematlab( cmplx( a2f, 0.d0, 8), fname)
    !    
    !    ! Lorentzify
    !    write(*,*) "Lorentzification", ist
    !    ns = -1
    !    e = 1.d0
    !    do while( (e .gt. 0.1) .and. (ns .lt. 10))
    !      call multipleLorentzFit2D( eph_a2f_efset%freqs, eph_a2f_pfset%freqs, a2f, &
    !                                 eph_a2f_efset%nomeg, eph_a2f_pfset%nomeg, px, py, c, Nox, Noy, e, &
    !                                 epsRel=1.d-4, Nbasex=eph_a2f_Nbe, Nbasey=eph_a2f_Nbp, Nred=50, epsLD=1.d-3)
    !      call savgol2( eph_a2f_efset%nomeg, eph_a2f_efset%freqs, &
    !                    eph_a2f_pfset%nomeg, eph_a2f_pfset%freqs, &
    !                    a2f, 1, 1, 1, 1, 0, 0, a2fs)
    !      a2f = a2fs
    !      ns = ns + 1
    !    end do
    !    eph_eliashbergfun( 1:eph_a2f_Nbe, 1:eph_a2f_Nbp, ist, ip) = c
    !    eph_eliashbergfun( 1:eph_a2f_Nbe, (eph_a2f_Nbp+1):(eph_a2f_Nbp+2), ist, ip) = transpose( px)
    !    eph_eliashbergfun( (eph_a2f_Nbe+1):(eph_a2f_Nbe+2), 1:eph_a2f_Nbp, ist, ip) = py
    !    write(*,*) e, ns, Nox, Noy
    !  end do
    !    
    !  call timesec( t1)
    !  ts = t1 - t0
    !  call eph_write_eliashberg( ip, eph_eliashbergfun(:,:,:,ip))
    !  call writematlab( cmplx( reshape( eph_a2f_efset%freqs, (/eph_a2f_efset%nomeg, 1/)), 0.d0, 8), 'efreq')
    !  call writematlab( cmplx( reshape( eph_a2f_pfset%freqs, (/eph_a2f_pfset%nomeg, 1/)), 0.d0, 8), 'pfreq')
    !  call timesec( t0)
    !  tio = t0 - t1

    !  write(*,'("initialization:   ",f13.6)') ti
    !  write(*,'("phonon weights:   ",f13.6)') tp
    !  write(*,'("electron weights: ",f13.6)') te
    !  write(*,'("summation:        ",f13.6)') ts
    !  write(*,'("I/O:              ",f13.6)') tio
    !end subroutine eph_gen_eliashbergfun

!============================================================================
!============================================================================

    !subroutine eph_write_eliashberg( ip, a2f)
    !  integer, intent( in) :: ip
    !  real(8), intent( in) :: a2f( eph_a2f_Nbe+2, eph_a2f_Nbp+2, eph_fst:eph_lst)

    !  integer :: un, recl, ist
    !  character(256) :: fname

    !  if( mpiglobal%rank .gt. 0) return

    !  inquire( iolength=recl) eph_pset%vkl( :, ip), eph_a2f_Nbe, eph_a2f_Nbp, eph_fst, eph_lst, a2f
    !  call getunit( un)
    !  open( un, file="ELIASHBERG"//trim( filext), action='write', form='unformatted', access='direct', recl=recl)
    !  write( un, rec=ip) eph_pset%vkl( :, ip), eph_a2f_Nbe, eph_a2f_Nbp, eph_fst, eph_lst, a2f
    !  close( un)
    !  do ist = eph_fst, eph_lst
    !    write( fname, '("ELIASHBERGL_P",i3.3,"_B",i3.3)') ip, ist
    !    call writematlab( cmplx( a2f( :, :, ist), 0.d0, 8), fname)
    !  end do
    !  return
    !end subroutine eph_write_eliashberg

    !subroutine eph_read_eliashberg( ip, a2f, success)
    !  integer, intent( in) :: ip
    !  real(8), intent( out) :: a2f( eph_a2f_Nbe+2, eph_a2f_Nbp+2, eph_fst:eph_lst)
    !  logical, intent( out) :: success

    !  integer :: un, recl, Nbe, Nbp, fst, lst
    !  real(8) :: vl(3)

    !  success = .false.
    !  if( mpiglobal%rank .gt. 0) return

    !  inquire( file="ELIASHBERG"//trim( filext), exist=success)
    !  if( .not. success) then
    !    write(*,*)
    !    write(*,'("Error (eph_read_eliashberg): File ELIASHBERG'//trim( filext)//' does not exist.")')
    !    return
    !  end if

    !  inquire( iolength=recl) vl, Nbe, Nbp, fst, lst, a2f
    !  call getunit( un)
    !  open( un, file="ELIASHBERG"//trim( filext), action='read', form='unformatted', access='direct', recl=recl)
    !  read( un, rec=1) vl, Nbe, Nbp, fst, lst
    !  
    !  if( Nbe .ne. eph_a2f_Nbe) then
    !    write(*,*)
    !    write(*,'("Error( eph_read_eliashberg): Different number of electron basis functions.")')
    !    write(*,'(" current       :",i)') eph_a2f_Nbe
    !    write(*,'(" ELIASHBERG'//trim( filext)//':",i)') Nbe
    !    stop
    !  end if
    !  if( Nbp .ne. eph_a2f_Nbp) then
    !    write(*,*)
    !    write(*,'("Error( eph_read_eliashberg): Different number of phonon basis functions.")')
    !    write(*,'(" current       :",i)') eph_a2f_Nbp
    !    write(*,'(" ELIASHBERG'//trim( filext)//':",i)') Nbp
    !    stop
    !  end if
    !  if( (fst .ne. eph_fst) .or. (lst .ne. eph_lst)) then
    !    write(*,*)
    !    write(*,'("Error( eph_read_eliashberg): Different band ranges.")')
    !    write(*,'(" current       :",2i)') eph_fst, eph_lst
    !    write(*,'(" ELIASHBERG'//trim( filext)//':",2i)') fst, lst
    !    stop
    !  end if

    !  read( un, rec=ip) vl, Nbe, Nbp, fst, lst, a2f
    !  if( abs( norm2( vl - eph_pset%vkl( :, ip))) .gt. input%structure%epslat) then
    !    write(*,*)
    !    write(*,'("Error( eph_read_eliashberg): Differing vectors for p-point ",i)') ip
    !    write(*,'(" current       :",3g18.10)') eph_pset%vkl( :, ip)
    !    write(*,'(" ELIASHBERG'//trim( filext)//':",3g18.10)') vl
    !    stop
    !  end if
    !  close( un)
    !  success = .true.
    !  return
    !end subroutine eph_read_eliashberg
end module mod_eph
