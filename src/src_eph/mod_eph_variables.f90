module mod_eph_variables
  use mod_kpointset
  use mod_constants
  use modinput
  use mod_opt_tetra
  use mod_my_tetra

  implicit none

! module variables
  logical                 :: eph_initialized = .false.
  real(8)                 :: eph_t0
  character(265)          :: eph_filename

  ! electron quantities
  type( k_set)            :: eph_kset_el                  ! (coarse) original/support grid of electrons
  type( Gk_set)           :: eph_Gkset_el                 ! (coarse) original/support grid of electrons
  integer                 :: eph_fst                      ! lowest band
  integer                 :: eph_lst                      ! highest band
  integer                 :: eph_nst                      ! number of bands
  integer                 :: eph_nqpmax                   ! maximum number of QP peaks per band
  real(8)                 :: eph_efermi                   ! Fermi-level (VBM if no metal)
  real(8)                 :: eph_scissor                  ! scissor operator
  real(8), allocatable    :: eph_evalp(:,:)               ! energies at target points
  complex(8), allocatable :: eph_evalp_qp(:,:,:)          ! QP energies at target points
  real(8), allocatable    :: eph_evalqp(:,:)              ! energies at target q+p grid

  ! phonon quantities
  integer                 :: eph_nmode                    ! number of phonon modes
  type( k_set)            :: eph_kset_ph                  ! (coarse) original/support grid of phonons
  type( Gk_set)           :: eph_Gkset_ph                 ! (coarse) original/support grid of phonons
  real(8), allocatable    :: eph_phfreq(:,:)              ! phonon frequencies on integration grid
  complex(8), allocatable :: eph_phevec(:,:,:,:)          ! phonon eigenvectors on integration grid

  ! electron-phonon quantities
  logical                 :: eph_diag = .true.
  integer                 :: eph_nfreq                    ! number of frequencies
  integer                 :: eph_ist                      ! initial electronic state to print e-ph matrix elements
  integer                 :: eph_jst                      ! final electronic state to print e-ph matrix elemetns
  real(8), allocatable    :: eph_freqs(:,:)               ! frequency grid for each p-point
  complex(8), allocatable :: eph_ephmat(:,:,:,:)          ! electron-phonon matrix elements
  complex(8), allocatable :: eph_ephmat_kq(:,:,:,:,:)     ! electron-phonon matrix elements coarse grid
  complex(8), allocatable :: eph_ephmat_kp(:,:,:,:,:)     ! electron-phonon matrix elements fine grid
  complex(8), allocatable :: eph_sigmafm(:,:,:,:)         ! Fan-Migdal electron self-energy
  real(8), allocatable    :: eph_specfun(:,:,:)           ! total spectral function
  real(8), allocatable    :: eph_specfun_band(:,:,:,:)    ! spectral function (for each band)
  real(8), allocatable    :: eph_a2F(:,:,:,:)             ! Eliashberg function
  
  ! BZ integration
  type( k_set)            :: eph_qset                     ! (dense) grid for BZ integrals
  type( k_set)            :: eph_qpset                    ! q+p set
  integer, allocatable    :: eph_iqp2iq(:)                ! index map (reduced) q+p point -> (reduced) q point
  integer, allocatable    :: eph_iq2iqp(:)                ! index map (reduced) q point -> (reduced) q+p point
  type( t_set)            :: eph_tetra                    ! tetrahedra for tetrahedron integration

  ! misc
  type( k_set)            :: eph_kset_aux                 ! Auxiliary k-set for electrons
  type( k_set)            :: eph_kset_ph_aux              ! Auxiliary k-set for phonons 
  type( G_set)            :: eph_Gset

  ! target k-path quantities
  type( k_set)            :: eph_pset                      ! k-point set for target quantities
  integer                 :: eph_pset_nvp1d                ! number of vertices along path
  integer                 :: eph_pset_npp1d                ! number of k-points along path
  real(8), allocatable    :: eph_pset_dvp1d(:)             ! cummulative distance of vertices
  real(8), allocatable    :: eph_pset_dpp1d(:)             ! cummulative distance of k-points
  type( point_type), allocatable, target :: eph_pset_verts(:) ! vertices along path
  integer                 :: eph_pset_ngp1d_el             ! number of electron grid points that lie along the path
  integer, allocatable    :: eph_pset_igp1d_el(:)          ! electron grid points that lie along the path
  real(8), allocatable    :: eph_pset_dgp1d_el(:)          ! cummulative distance of electron grid points
  integer                 :: eph_pset_ngp1d_ph             ! number of phonon grid points that lie along the path
  integer, allocatable    :: eph_pset_igp1d_ph(:)          ! phonon grid points that lie along the path
  real(8), allocatable    :: eph_pset_dgp1d_ph(:)          ! cummulative distance of phonon grid points

  ! numerical parameters
  integer                 :: eph_ntemp                    ! number of temperatures
  real(8), allocatable    :: eph_temps(:)                 ! temperatures (in K) for occupation
  real(8)                 :: eph_eta                      ! complex infinitesimal in Lehmann representation
  real(8)                 :: eph_etaeps = 0.000d0/h2ev

  ! Eliashberg function
  real(8), allocatable    :: eph_a2f_efreqs(:)
  real(8), allocatable    :: eph_a2f_pfreqs(:)
  integer                 :: eph_a2f_nefreq = 200
  integer                 :: eph_a2f_npfreq = 200

  ! polar parameters
  logical                 :: eph_polar = .false.                    
  real(8), allocatable    :: eph_epsinf(:,:)              ! high-frequency dielectric tensor (at clamped nuclei)
  real(8), allocatable    :: eph_zstar(:,:,:)             ! Born effective charge tensor for each atom

  ! quantities that are constant now but which have to be replaced when DFPT is available
  ! Einstein model
  logical                 :: eph_einstein = .false.
  real(8)                 :: eph_einstein_phfreq          ! constant phonon frequency
  real(8)                 :: eph_einstein_ephmat          ! eph coupling strength
  real(8)                 :: eph_einstein_eldisp          ! gradient of linearly dispersing electron
  ! Debye model
  logical                 :: eph_debye = .false.
  real(8)                 :: eph_debye_ephmat             ! eph coupling strength
  real(8)                 :: eph_debye_phdisp             ! gradient of linearly dispersing phonon
  real(8)                 :: eph_debye_phfreq             ! maximum phonon frequency
  real(8)                 :: eph_debye_elengy             ! constant electron energy

  logical                 :: eph_dfpt = .false.

! methods
  contains
    subroutine eph_destroy
      if( allocated( eph_freqs)) deallocate( eph_freqs)
      if( allocated( eph_sigmafm)) deallocate( eph_sigmafm)
      if( allocated( eph_specfun)) deallocate( eph_specfun)
      if( allocated( eph_evalp)) deallocate( eph_evalp)
      if( allocated( eph_evalqp)) deallocate( eph_evalqp)
      if( allocated( eph_phfreq)) deallocate( eph_phfreq)
      if( allocated( eph_iqp2iq)) deallocate( eph_iqp2iq)
      if( allocated( eph_iq2iqp)) deallocate( eph_iq2iqp)
      if( allocated( eph_pset_dvp1d)) deallocate( eph_pset_dvp1d)
      if( allocated( eph_pset_dpp1d)) deallocate( eph_pset_dpp1d)
      if( allocated( eph_pset_igp1d_el)) deallocate( eph_pset_igp1d_el)
      if( allocated( eph_pset_dgp1d_el)) deallocate( eph_pset_dgp1d_el)
      if( allocated( eph_pset_igp1d_ph)) deallocate( eph_pset_igp1d_ph)
      if( allocated( eph_pset_dgp1d_ph)) deallocate( eph_pset_dgp1d_ph)
      call delete_G_vectors( eph_Gset)
      call delete_k_vectors( eph_kset_el)
      call delete_k_vectors( eph_kset_ph)
      call delete_k_vectors( eph_qset)
      call delete_k_vectors( eph_pset)
      call delete_k_vectors( eph_qpset)
      call delete_Gk_vectors( eph_Gkset_el)
      call delete_Gk_vectors( eph_Gkset_ph)
      eph_initialized = .false.
      call delete_k_vectors( eph_kset_aux)
      call delete_k_vectors( eph_kset_ph_aux)
    end subroutine eph_destroy
end module mod_eph_variables
