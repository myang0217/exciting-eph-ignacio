subroutine ephlauncher
    use modinput
    use mod_eph

    implicit none

    if( associated( input%eph)) then
      ! generate Wannier functions
      call eph_init
      call eph_do
      call eph_destroy
    end if

    return
end subroutine ephlauncher
