module mod_schlessinger
  use mod_polynomials
  implicit none
  private

  real(8), parameter    :: eps = 1.d-18
  complex(8), parameter :: zero = cmplx( 0.d0, 0.d0, 8)
  complex(8), parameter :: one = cmplx( 1.d0, 0.d0, 8)

  integer :: nval, mordmax
  complex(8), allocatable :: afrac(:), absc(:)

  public :: schlessinger_init, schlessinger_interp, schlessinger_getpoly
  contains
    !BOP
    ! !ROUTINE: schlessinger_init
    ! !INTERFACE:
    subroutine schlessinger_init( n, z, f, eps, maxord)
      use m_plotmat
      ! !INPUT PARAMETERS:
      !   n    : number of input points (in, integer)
      !   z    : abscissa values (in, complex(n))
      !   f    : function values( in, complex(n))
      !
      ! !DESCRIPTION:
      ! Uses the Schlessinger point method to find a fractional function 
      ! $\tilde{f}(z) = \frac{p(z)}{q(z)}$ that passes through the given 
      ! function values $f_i = f(z_i)$ and interpolates between them. 
      ! $p$ and $q$ are polynomials ${p(z)=\sum_{k=0}^{\texttt{n}/2} p_k z^k}$.
      ! When \texttt{n} is odd, $p(z)$ and $q(z)$ are of order ${(\texttt{n}-1)/2}$. 
      ! When \texttt{n} is even, $p(z)$ is of order ${\texttt{n}/2-1}$ and $q(z)$ is 
      ! of order ${\texttt{n}/2}$.
      ! The interpolating function can also be used as an analytic continuation
      ! of the provided function values.
      !
      ! !REVISION HISTORY:
      !   Created July 2019 (SeTi)
      !EOP
      !BOC
      integer, intent( in)              :: n         ! # input points
      complex(8), intent( in)           :: z( n)     ! abscissa values
      complex(8), intent( in)           :: f( n)     ! function values
      real(8), optional, intent( in)    :: eps       ! aimed interpolation error
      integer, optional, intent( in)    :: maxord    ! maximum number of fitting points
    
      real(8) :: e
      complex(8) :: fc( n)
    
      nval = n
      mordmax = n
      e = 1.d-16
      if( present( eps)) e = eps
      if( present( maxord)) mordmax = min( mordmax, maxord)

      ! make copy of input
      if( allocated( absc)) deallocate( absc)
      allocate( absc( nval))
      if( allocated( afrac)) deallocate( afrac)
      allocate( afrac( 0:nval-1))
      absc = z
      fc = f
    
      ! get coefficients of continued fraction
      call fraccoeff( nval, absc, fc, e, afrac)
      !write(*,*) 'MORDMAX', mordmax
      !write(*,*) 'FRAC COEFF'
      !do i = 0, n-1
      !  write(*,'(i,2f26.16)') i, a(i)
      !end do
      !call writematlab( reshape( a, (/n, 1/)), 'AFRAC')
      call writematlab( reshape( z, (/nval, 1/)), 'ABSCISS')
      call writematlab( reshape( f, (/nval, 1/)), 'FUN0')
    
      return
    end subroutine schlessinger_init
    !EOC

    subroutine schlessinger_interp( m, n, z, f, poly)
      integer, intent( in) :: m
      integer, intent( in) :: n
      complex(8), intent( in) :: z(n)
      complex(8), intent( out) :: f(n)
      logical, optional, intent( in) :: poly

      logical :: pol
      integer :: i, j, ord
      complex(8) :: c
      complex(8), allocatable :: p(:), q(:), t(:,:)

      pol = .false.
      if( present( poly)) pol = poly
      if( mod( m, 2) .eq. mod( mordmax, 2)) then
        ord = min( m, mordmax)
      else
        ord = min( m, mordmax-1)
      end if

      if( pol) then
        allocate( p( 0:ord/2), q( 0:ord/2), t(n,2))
        call schlessinger_getpoly( ord, p, q)
        call poly_seval( ord/2, p, n, z, t(:,1))
        call poly_seval( ord/2, q, n, z, t(:,2))
        f = t(:,1)/t(:,2)
        deallocate( p, q, t)
      else
        do i = 1, n
          c = one
          do j = ord-1, 1, -1
            c = one + afrac(j)*(z(i) - absc(j))/c
          end do
          f(i) = afrac(0)/c
        end do
      end if
    end subroutine schlessinger_interp
    
    subroutine schlessinger_getpoly( m, p, q)
      integer, intent( in) :: m
      complex(8), intent( out) :: p( 0:m/2), q( 0:m/2)

      integer :: i, np, nq, nt
      complex(8) :: s(0:1), t( 0:m/2), c1, c2

      ! get coefficients of polynomials
      p = zero
      q = zero
    
      np = 0
      p(0) = one
      nq = 1
      q(0) = one - afrac(m-1)*absc(m-1)
      q(1) = afrac(m-1)
    
      do i = m-2, 1, -1
        s(0) = -afrac(i)*absc(i)
        s(1) = afrac(i)
        nt = np + 1
        call poly_smul( 1, s, np, p(0:np), t(0:nt))
        np = nq
        p(0:np) = q(0:nq)
        nq = max( np, nt)
        call poly_sadd( np, p(0:np), nt, t(0:nt), q(0:nq))
        !c1 = p( maxloc( abs( p(0:np)), 1))
        !c2 = q( maxloc( abs( q(0:nq)), 1))
        !if( abs( c2) .gt. abs( c1)) c1 = c2
        !if( abs( c2) .gt. eps) then
        !  p = p/c2
        !  q = q/c2
        !end if
      end do
      p = afrac(0)*p
      q = q/p(0)
      p = p/p(0)
      return 
    end subroutine schlessinger_getpoly

    ! coefficients of contineous fraction
    subroutine fraccoeff( n, z, f, err, a)
      use m_getunit
      integer, intent( in)              :: n
      complex(8), intent( inout)        :: z(n)
      complex(8), intent( inout)        :: f(n)
      real(8), intent( in)              :: err
      complex(8), intent( out)          :: a( 0:n-1)
    
      integer :: i, j, k, kmin, idx(n), idxt(n), un
      real(8) :: dist(n), e, emin
      complex(8) :: amin, b, z0(n), f0(n), fi(n)
    
      ! use all points. order does not matter
      if( err .le. 0.d0) then
        call sortidx( n, -abs( f), idx)
        z = z( idx)
        f = f( idx)
    
        a = zero
        a(0) = f(1)
        if( abs( f(2)) .le. eps) return
        a(1) = (f(1)/f(2) - one)/(z(2) - z(1))
        do i = 2, n-1
          b = one
          if( abs( f(1)-f(i+1)) .gt. eps) then
            if( abs( f(i+1)) .gt. eps) b = one + a(1)*(z(i+1) - z(1))/(one - f(1)/f(i+1))
            do j = 2, i-1
                b = one + a(j)*(z(i+1) - z(j))/b
            end do
          end if
          if( abs( b) .gt. eps) a(i) = b/(z(i) - z(i+1))
        end do
        return
      end if

      ! find optimal order
      z0 = z
      f0 = f
      do i = 1, n
        idx( i) = i
      end do
      a = zero
      i = 1
      !call getunit( un)
      !open( un, file='SCHLESSINGER.DAT', action='write', status='unknown', form='formatted')

      b = sum( f)/dble( n)
      kmin = minloc( abs(f - b), 1)
      call updateIdx( i-1, n, idx, kmin)
      f = f0( idx)
      z = z0( idx)
      a(0) = f(1)
      call schlessinger_interp( i, n, z, fi)
      emin = dble( sqrt( dot_product( f-fi, f-fi)/dot_product( f, f)))
      if( emin .lt. err) mordmax = i
      if( mordmax .eq. i) return
      !write(*,'(3i,f13.6)') i, kmin, idx(i), emin
      i = i + 1

      !do j = 1, n
      !  write( un, '(6g20.10)') z(j), f(j), fi(j)
      !end do
      !write( un, *)
      !write( un, *)

      emin = 1.d128
      kmin = i
      amin = zero
      do k = i, n
        idxt = idx
        call updateIdx( i-1, n, idxt, k)
        f = f0( idxt)
        z = z0( idxt)

        a(1) = (f(1)/f(2) - one)/(z(2) - z(1))

        call schlessinger_interp( i, n, z, fi)
        e = dble( sqrt( dot_product( f-fi, f-fi)/dot_product( f, f)))
        if( e .lt. emin) then
          kmin = k
          emin = e
          amin = a(1)
        end if
      end do
      call updateIdx( i-1, n, idx, kmin)
      f = f0 (idx)
      z = z0( idx)
      a(1) = amin
      call schlessinger_interp( i, n, z, fi)
      emin = dble( sqrt( dot_product( f-fi, f-fi)/dot_product( f, f)))
      !write(*,'(3i,f13.6)') i, kmin, idx(i), emin
      if( emin .lt. err) mordmax = i
      if( mordmax .eq. i) return
      i = i + 1

      !do j = 1, n
      !  write( un, '(6g20.10)') z(j), f(j), fi(j)
      !end do
      !write( un, *)
      !write( un, *)

      do i = 3, mordmax

        emin = 1.d128
        kmin = i
        amin = zero
        do k = i, n
          idxt = idx
          call updateIdx( i-1, n, idxt, k)
          f = f0( idxt)
          z = z0( idxt)

          if( abs( f(1) - f(i)) .gt. eps) then
            if( abs( f(i)) .gt. eps) then
              b = one + a(1)*(z(i) - z(1))/(one - f(1)/f(i))
            else
              b = one
            end if
          else
            b = cmplx( 1.d0/eps, 0.d0, 8)
          end if
          do j = 2, i-2
            b = one + a(j)*(z(i) - z(j))/b
          end do
          if( abs( b) .gt. eps) a(i-1) = b/(z(i-1) - z(i))

          call schlessinger_interp( i, n, z, fi)
          e = dble( sqrt( dot_product( f-fi, f-fi)/dot_product( f, f)))
          if( e .lt. emin) then
            kmin = k
            emin = e
            amin = a(i-1)
          end if
        end do
        !write(*,'(3i,f13.6)') i, kmin, idx(i), emin
        
        call updateIdx( i-1, n, idx, kmin)
        f = f0( idx)
        z = z0( idx)
        a(i-1) = amin
        if( emin .lt. err) mordmax = i
        if( mordmax .eq. i) return
        call schlessinger_interp( i, n, z, fi)

        !do j = 1, n
        !  write( un, '(6g20.10)') z(j), f(j), fi(j)
        !end do
        !write( un, *)
        !write( un, *)
        !write(*,'(i,2g26.16)') i, a(i)
        !write(*,*)
      end do
      !close( un)
      return

      contains
        subroutine updateIdx( m, n, idx, j)
          integer, intent( in) :: m, n, j
          integer, intent( inout) :: idx(n)

          integer :: i
          i = minloc( abs( idx - j), 1)
          if( (i .gt. m) .and. (idx(i) .eq. j)) then
            idx( (m+2):i) = idx( (m+1):(i-1))
            idx( m+1) = j
          end if
          return
        end subroutine updateIdx
        
        subroutine getDistance( m, n, z, d)
          integer, intent( in) :: m, n
          complex(8), intent( in) :: z(n)
          real(8), intent( out) :: d(n)

          integer :: i
          
          d(1:m) = 0.d0
          do i = m + 1, n
            d(i) = minval( abs( z(1:m) - z(i)))
          end do
          return
        end subroutine getDistance
    end subroutine fraccoeff

end module mod_schlessinger
