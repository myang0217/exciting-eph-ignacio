!
!
!
!
! Copyright (C) 2002-2005 J. K. Dewhurst, S. Sharma and C. Ambrosch-Draxl.
! This file is distributed under the terms of the GNU Lesser General Public
! License. See the file COPYING for license details.
!
!BOP
! !ROUTINE: gridptsonpath
! !INTERFACE:
!
!
subroutine gridptsonpath( nv, verts, pset, eps, np, pts, dist)
! !INPUT/OUTPUT PARAMETERS:
!   nv    : number of vertices (in,integer)
!   verts : vertices (in,point_type(nv))
!   pset  : k-point set (in,k_set)
!   eps   : tolerance
!   np    : number of grid points along path (out,integer)
!   pts   : indices of grid points along path (out,integer(*))
!   dist  : cummulative distance of each point (out,real(*))
! !DESCRIPTION:
!   Find the points on a grid (or its symmetry equivalent points) that lie along
!   a path given by a set of vertices as well as the cummulative distance of each
!   point measured from the first vertex.
!
! !REVISION HISTORY:
!   Created July 2020 (SeTi)
!EOP
!BOC
  use modinput
  use mod_kpointset
  implicit none
! arguments
  integer, intent( in)                 :: nv
  type( point_type_array), intent( in) :: verts( nv)
  type( k_set), intent( in)            :: pset
  real(8), intent( in)                 :: eps
  integer, intent( out)                :: np
  integer, intent( out)                :: pts(*)
  real(8), intent( out)                :: dist(*)
! local variables
  integer :: iv, i, j, k
  real(8) :: s, d, d0, a(3), b(3), r(3)
  character(len=80) :: err
! alloctable arrays

  np = 0; d = 0.d0
  do iv = 1, nv-1
    a = verts( iv)%point%coord*pset%ngridk - pset%vkloff
    if( verts( iv)%point%breakafter) then
      if( norm2( (a - nint( a))/pset%ngridk) > eps) cycle
      np = np + 1
      r = ( nint( a) + pset%vkloff)/pset%ngridk
      call findkptinset( r, pset, k, pts( np))
      dist( np) = d
    else
      b = verts( iv+1)%point%coord - verts( iv)%point%coord
      call r3mv( pset%bvec, b, r)
      b = b*pset%ngridk
      d0 = norm2( r)
      j = minloc( abs( b), dim=1, mask=(abs(b) > eps))
      do i = 0, floor( abs( b(j)))
        if( b(j) > 0.d0) k = ceiling( a(j)-eps+i)
        if( b(j) < 0.d0) k = floor( a(j)+eps-i)
        s = (k - a(j))/b(j)
        if( (0.d0 > s+eps) .or. (s+eps > 1.d0)) cycle
        r = a + s*b
        if( norm2( (r - nint( r))/pset%ngridk) < eps) then
          np = np + 1
          r = ( nint( r) + pset%vkloff)/pset%ngridk
          call findkptinset( r, pset, k, pts( np))
          dist( np) = d + s*d0
        end if
      end do
      d = d + d0
    end if
  end do
  a = verts( iv)%point%coord*pset%ngridk - pset%vkloff
  if( norm2( (a - nint( a))/pset%ngridk) < eps) then
    np = np + 1
    r = ( nint( a) + pset%vkloff)/pset%ngridk
    call findkptinset( r, pset, k, pts( np))
    dist( np) = d
  end if
end subroutine
!EOC
