subroutine savitzkygolay( nx, x, f, p, n, d, g)
  use m_linalg
  implicit none

  integer, intent( in)  :: nx      ! # of points
  real(8), intent( in)  :: x( nx)  ! the x-values
  real(8), intent( in)  :: f( nx)  ! the function values
  integer, intent( in)  :: p       ! degree of the fitting polynomial
  integer, intent( in)  :: n       ! number of neighbors 2n+1 > p
  integer, intent( in)  :: d       ! order of derivative that is returned d <= p
  real(8), intent( out) :: g( nx)  ! the smoothed (interpolated) function/derivative

  integer :: m, k, ix
  real(8) :: dx, dfac, a, b
  real(8) :: j( 0:p, -n:n), c( -n:n, 0:p), jjti( 0:p, 0:p), y( -n:n)
  logical :: equi

  if( 2*n+1 .le. p) then
    write(*,*)
    write(*,'("Error (svitzkygolay): 2*n+1 > p violated for n = ",i3," and p = ",i3,".")') n, p
    stop
  end if
  if( d .gt. p) then
    write(*,*)
    write(*,'("Error (svitzkygolay): The derivative order must not be larger than the polynomial degree.")')
    stop
  end if
  equi = .true.
  dx = (x(nx) - x(1))/dble( nx -1)
  do ix = 1, nx-1
    if( abs( x( ix+1) - x( ix) - dx) .gt. 1.d-12) then
      equi = .false.
      exit
    end if
  end do

  dfac = 1.d0
  if( equi) then
    j = 1.d0
    do m = 1, p
      do k = -n, n
        j( m, k) = dble( k)**m
      end do
    end do
    call rpinv( matmul( j, transpose( j)), jjti)
    c = transpose( matmul( jjti, dble( j)))
    do k = 1, d
      dfac = dfac*(dble( k)/dx)
    end do
  end if

  a = (f( nx) - f(1))/(x( nx) - x(1))
  b = f(1) - a*x(1)
  
  do ix = 0, nx-1
    if( .not. equi) then
      j = 1.d0
      y = x( ix+1)
      do k = max( ix+1-n, 1), min( ix+1+n, nx)
        y( k-ix-1) = x(k)
      end do
      do m = 1, p
        do k = -n, n
          j( m, k) = (y(k) - y(0))**m
        end do
      end do
      call rpinv( j, c)
    end if
    do k = -n, n
      if( ix+k .lt. 0) then
        m = -(ix + k) + 1
        y( k) = -f( m) + a*x( m) + b
      else if( ix+k .ge. nx) then
        m = 2*nx - 1 - (ix + k)
        y( k) = -f( m) + a*x( m) + b
      else
        m = ix + k + 1
        y( k) = f( m) - a*x( m) - b
      end if
    end do
    g( ix+1) = dot_product( c( :, d), y)*dfac
  end do

  if( d .eq. 0) then
    do ix = 1, nx
      g( ix) = g( ix) + a*x( ix) + b
    end do
  else if( d .eq. 1) then
    g = g + a
  end if
  return
end subroutine savitzkygolay
