subroutine interp1d( nin, xin, fin, nout, xout, fout, method)
  implicit none

  integer, intent( in)      :: nin          ! # of input points
  real(8), intent( in)      :: xin( nin)    ! the input x-values
  real(8), intent( in)      :: fin( nin)    ! the input function values
  integer, intent( in)      :: nout         ! # of input points
  real(8), intent( in)      :: xout( nout)  ! the input x-values
  real(8), intent( out)     :: fout( nout)  ! the input function values
  character(*), intent( in) :: method       ! 'linear', 'spline'

  integer :: idx( 0:nout), i, j
  real(8) :: cf( 3, nin), x

  idx = 0
  outer:do i = 1, nout
    if( xout( i) .ge. xin( nin)) then
      idx( i) = nin
      cycle outer
    end if
    inner:do j = max( idx( i-1), 1), nin - 1
      if( (xout( i) .ge. xin( j)) .and. (xout( i) .lt. xin( j+1))) then
        idx( i) = j
        exit inner
      end if
    end do inner
  end do outer

  fout = 0.d0
  if( trim( method) .eq. 'linear') then
    !write(*,'(2f13.6)') xin(1), xin( nin)
    do i = 1, nout
      if( idx( i) .eq. 0) then
        fout( i) = fin(1)
      else if( idx( i) .eq. nin) then
        fout( i) = fin( nin)
      else
        x = xout( i) - xin( idx( i))
        fout( i) = fin( idx( i)) + x*(fin( idx( i)+1) - fin( idx( i)))/(xin( idx( i)+1)-xin( idx( i)))
      end if
    end do
    return
  else if( trim( method) .eq. 'spline') then
    call spline( nin, xin, 1, fin, cf)
    do i = 1, nout
      if( idx( i) .eq. 0) then
        fout( i) = fin(1)
      else if( idx( i) .eq. nin) then
        fout( i) = fin( nin)
      else
        x = xout( i) - xin( idx( i))
        fout( i) = fin( idx( i)) + x*(cf( 1, idx( i)) + x*(cf( 2, idx( i)) + x*cf( 3, idx( i))))
      end if
    end do
    return
  else
    write(*,*)
    write(*,'("Error (interp): invalid interpolation method.")')
    stop
  end if

  return
end subroutine interp1d
