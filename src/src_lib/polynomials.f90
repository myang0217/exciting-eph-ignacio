module mod_polynomials
  implicit none

  contains

    subroutine poly_sadd( m, p1, n, p2, q)
      integer, intent( in)     :: m, n
      complex(8), intent( in)  :: p1(0:m), p2(0:n)
      complex(8), intent( out) :: q( 0:max(m,n))

      integer :: i

      if( m .gt. n) then
        q = p1
        do i = 0, n
          q(i) = q(i) + p2(i)
        end do
      else
        q = p2
        do i = 0, m
          q(i) = q(i) + p1(i)
        end do
      end if

      return
    end subroutine poly_sadd

    subroutine poly_fadd( m, p1, n, p2, q)
      integer, intent( in)     :: m, n
      complex(8), intent( in)  :: p1(0:m), p2(0:n)
      complex(8), intent( out) :: q( 0:max(m,n))

      complex(8) :: p1s(0:m), p2s(0:n), qs(0:max(m,n))

      call poly_f2s( m, p1, p1s)
      call poly_f2s( n, p2, p2s)
      call poly_sadd( m, p1s, n, p2s, qs)
      call poly_s2f( max(m,n), qs, q)

      return
    end subroutine poly_fadd

    subroutine poly_smul( m, p1, n, p2, q)
      integer, intent( in)     :: m, n
      complex(8), intent( in)  :: p1(0:m), p2(0:n)
      complex(8), intent( out) :: q(0:m+n)

      integer :: i, j, k, l
      
      q = cmplx( 0.d0, 0.d0, 8)
      do i = 0, m+n
        k = min( m+n-i, min( i, min( m, n)))
        l = max( 0, i-n)
        do j = 0, k
          q(i) = q(i) + p1(l+j)*p2(i-j-l)
        end do
      end do

      return
    end subroutine poly_smul

    subroutine poly_fmul( m, p1, n, p2, q)
      integer, intent( in)     :: m, n
      complex(8), intent( in)  :: p1(0:m), p2(0:n)
      complex(8), intent( out) :: q(0:m+n)

      integer :: idx( m+n)

      q(0) = p1(0)*p2(0)
      q(1:m) = p1
      q(m+1:m+n) = p2
      call sortidx( m+n, dble( q(1:m+n)), idx)
      q(1:m+n) = q(idx)

      return
    end subroutine poly_fmul

    subroutine poly_sdiv( m, p1, n, p2, q, s)
      integer, intent( in)     :: m, n
      complex(8), intent( in)  :: p1(0:m), p2(0:n)
      complex(8), intent( out) :: q(0:m)
      integer, intent( out)    :: s
      ! q(0:s)   : remainder r of division
      ! q(s+1:m) : result q of division
      ! as equation: p1/p2 = q + r/p2

      integer :: i, j
      complex(8) :: norm, c

      q = p1
      s = m
      if( m .lt. n) return

      s = n-1
      norm = p2(n)
      do i = 0, m-n
        q(m-i) = q(m-i)/norm
        c = q(m-i)
        if( abs( c) .gt. 1.d-18) then
          do j = 1, n
            q(m-i-j) = q(m-i-j) - c*p2(n-j)
          end do
        end if
      end do

      return
    end subroutine poly_sdiv

    subroutine poly_sapart( m, p1, n, p2, a, z)
      use m_linalg
      integer, intent( in)       :: m, n
      complex(8), intent( inout) :: p1(0:m)
      complex(8), intent( in)    :: p2(0:n)
      complex(8), intent( out)   :: a(n), z(n)

      integer :: i, j, k, s
      complex(8) :: q(0:m), p2f(0:n), qrf(0:n-1)
      complex(8) :: c(n,n), ci(n,n)

      call poly_sdiv( m, p1/p2(n), n, p2/p2(n), q, s)
      if( m .gt. n) then
        p1 = cmplx( 0.d0, 0.d0, 8)
        p1(0:m-n) = q(s+1:m)
      end if

      call poly_s2f( n, p2, p2f)
      z = p2f(1:n)

      qrf(0) = cmplx( 1.d0, 0.d0, 8)
      do i = 1, n
        k = 0
        do j = 1, n
          if( j .eq. i) cycle
          k = k + 1
          qrf(k) = z(j)
        end do
        call poly_f2s( n-1, qrf, c(:,i))
      end do

      call zpinv( c, ci)
      a = matmul( ci, q(0:s))

      return
    end subroutine poly_sapart

    subroutine poly_s2f( n, p, q)
      use poly_zeroes
      integer, intent( in)     :: n
      complex(8), intent( in)  :: p(0:n)
      complex(8), intent( out) :: q(0:n)

      integer :: i, idx(n)
      real(8) :: radius(n)
      logical :: error(n+1)

      q(0) = p(n)
      call polzeros( n, p, 2.d0**(-53), 2.d0**1023, 2.d0**(-1074), 30, q(1:n), radius, error, i)
      call sortidx( n, dble( q(1:n)), idx)
      q(1:n) = q(idx)

      return
    end subroutine poly_s2f

    subroutine poly_f2s( n, p, q)
      integer, intent( in)     :: n
      complex(8), intent( in)  :: p(0:n)
      complex(8), intent( out) :: q(0:n)

      integer :: i, j

      q = cmplx( 0.d0, 0.d0, 8)
      q(0) = cmplx( 1.d0, 0.d0, 8)

      do i = 1, n
        do j = i, 1, -1
          q(j) = q(j-1) - q(j)*p(i)
        end do
        q(0) = -q(0)*p(i)
      end do
      q = q*p(0)

      return
    end subroutine poly_f2s

    subroutine poly_seval( n, p, nz, z, f)
      integer, intent( in)     :: n, nz
      complex(8), intent( in)  :: p(0:n)
      complex(8), intent( in)  :: z(nz)
      complex(8), intent( out) :: f(nz)
    
      integer :: i, j
      complex(8) :: zz( 0:n, nz)
    
      do i = 1, nz
        do j = 0, n
          zz(j,i) = z(i)**j
        end do
      end do
      call zgemv( 't', n+1, nz, cmplx( 1.d0, 0.d0, 8), zz, n+1, &
             p, 1, cmplx( 0.d0, 0.d0, 8), &
             f, 1)

      return
    end subroutine poly_seval

    subroutine poly_feval( n, p, nz, z, f)
      integer, intent( in)     :: n, nz
      complex(8), intent( in)  :: p(0:n)
      complex(8), intent( in)  :: z(nz)
      complex(8), intent( out) :: f(nz)
    
      integer :: i

      f = z - p(1)
      do i = 2, n
        f = f*(z - p(i))
      end do
      f = f*p(0)

      return
    end subroutine poly_feval
end module mod_polynomials
