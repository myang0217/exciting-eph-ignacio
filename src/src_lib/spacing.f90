module mod_spacing
  implicit none

  contains
    subroutine spacing( a, b, n, x, mode, c, w)
      real(8), intent( in)            :: a, b  ! left and right limit of the interval
      integer, intent( in)            :: n     ! #points (= #intervals + 1)
      real(8), intent( out)           :: x(n)  ! points
      integer, intent( in)            :: mode  ! 1=linear, 2=lorentzian
      real(8), optional, intent( in)  :: c, w  ! center and width of distribution for non-linear spacing
    
      ! NOTES
      ! The limits a and b are always included in x.
      integer :: i, nt
      real(8) :: xt, dx
      real(8), allocatable :: d(:,:)
    
      nt = 400

      ! linear spacing
      if( mode .eq. 1) then
        dx = (b - a)/dble( n-1)
        x(1) = a
        do i = 2, n
          x(i) = x(i-1) + dx
        end do
      else
        allocate( d( nt, 2))
        dx = (b - a)/dble( nt-1)
        d(1,1) = a
        do i = 2, nt
          d(i,1) = d(i-1,1) + dx
        end do
        ! Lorentzian scaling
        if( mode .eq. 2) then
          d(:,2) = 1.d0/((d(:,1) - c)**2 + 0.25d0*w**2)
          call spacingFromDensity( nt, d(:,1), d(:,2), n, x)
        else
          write(*,*)
          write(*,'("Error (spacing): Invalid spacing mode.")')
          stop
        end if
      end if
      
      return
    end subroutine spacing

    subroutine mergeSpacings( nf, xf, nk, xk, ni, xi, xir)
      use m_plotmat
      integer, intent( in)  :: nf, nk, ni
      real(8), intent( in)  :: xf( nf)
      real(8), intent( in)  :: xk( nk)
      real(8), intent( in)  :: xir(2)
      real(8), intent( out) :: xi( ni)

      integer :: i, nt
      real(8) :: df(nf), dk(nk), xmin, xmax, r(2)
      real(8), allocatable :: xt(:), yt(:), it(:), cf(:,:)

      call getSpacingDensity( nf, xf, df)
      call getSpacingDensity( nk, xk, dk)

      nt = 400
      xmin = xir(1)
      xmax = xir(2)
      allocate( xt( nt), yt( nt), it( nt), cf( 3, nt))
      call spacing( xmin, xmax, nt, xt, 1)
      do i = 1, nt
        call interp1d( nf, xf, df, 1, xt(i), r(1), 'spline')
        call interp1d( nk, xk, dk, 1, xt(i), r(2), 'spline')
        yt(i) = max( r(1), r(2))
      end do
      call fderiv( -1, nt, xt, yt, it, cf)
      yt = it*dble( ni-1)/it( nt)
      
      xi(1) = xmin
      do i = 2, ni-1
        call interp1d( nt, xt, yt, 1, xi(i-1), r(1), 'spline')
        call interp1d( nt, yt-r(1), xt, 1, (/1.d0/), xi(i), 'spline')
      end do
      xi( ni) = xmax
     
      deallocate( xt, yt, it, cf)
      return
    end subroutine mergeSpacings

    subroutine getSpacingDensity( n, x, d)
      integer, intent( in)  :: n
      real(8), intent( in)  :: x(n)
      real(8), intent( out) :: d(n)

      real(8) :: xt( 0:n+1), yt( 0:n+1), it( 0:n+1), cf( 3, 0:n+1)
      xt(1:n) = x
      xt(0)   = x(1) - (x(2) - xt(1))
      xt(n+1) = x(n) + (x(n) - xt(n-1))
      call spacing( dble( -1), dble( n+1), n+2, yt, 1)
      call fderiv( 1, n+2, xt, yt, it, cf)
      d = it(1:n)
    end subroutine getSpacingDensity
    
    subroutine spacingFromDensity( nd, xd, d, nx, x)
      integer, intent( in)  :: nd, nx
      real(8), intent( in)  :: xd( nd), d( nd)
      real(8), intent( out) :: x( nx)

      integer :: i
      real(8) :: id( nd), cf( 3, nd)

      call fderiv( -1, nd, xd, d, id, cf)
      id = id*dble( nx-1)/id( nd)

      x(1) = xd(1)
      do i = 2, nx - 1
        call interp1d( nd, xd, id, 1, x( i-1), x(i), 'linear')
        call interp1d( nd, id-x(i), xd, 1, (/1.d0/), x(i), 'linear')
      end do
      x(nx) = xd( nd)
    end subroutine spacingFromDensity
end module mod_spacing
