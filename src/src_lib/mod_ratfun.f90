! This file is distributed under the terms of the GNU General Public License.
!
! Created May 2020 (Sebastian Tillack)
!
! DESCRIPTION
module mod_ratfun
  implicit none

  contains
    subroutine ratfun_multipole( n, w, f, eps, npmax, pol, res)
      use mod_manopt
      integer, intent( in)     :: n              ! #(z,f) pairs
      real(8), intent( in)     :: w(n)           ! abscissa values
      complex(8), intent( in)  :: f(n)           ! function values f=f(z)
      real(8), intent( inout)  :: eps            ! aimed error
      integer, intent( inout)  :: npmax          ! maximum number of poles
      complex(8), intent( out) :: pol(npmax)     ! poles
      complex(8), intent( out) :: res(npmax)     ! residues

      integer :: i, np, idx(npmax)
      real(8) :: err
      complex(8) :: zer(npmax)

      ! for non-linear optimization
      real(8) :: lambda(2), lor, rho
      integer :: it, jt, dxo(2), dx(2,1)
      complex(8) :: con, intf0
      real(8), allocatable :: x(:,:,:)
      complex(8), allocatable :: fi(:)

      !write(*,*) '--- MULTIPOLE EXPANSION ---'
      ! get initial guess from AAA algorithm
      np = npmax; err = eps
      call ratfun_aaa( n, cmplx( w, 0.d0, 8), f, cmplx( 1.d0, 0.d0, 8), err, np, zer, pol, res)
      !write(*,*) 'MP AAA GUESS'
      !do i = 1, np
      !  write(*,'(i,6g20.10)') i, pol(i), res(i)
      !end do
      !write(*,'(i,4g20.10)') 0, 0.d0, 0.d0, sum( res(1:np))
      ! refine guess using vector fitting algorithm
      call ratfun_vecfit( n, cmplx( w, 0.d0, 8), f, np, pol(1:np), res(1:np))
      !write(*,*) 'MP VF GUESS'
      !do i = 1, np
      !  write(*,'(i,6g20.10)') i, pol(i), res(i)
      !end do
      !write(*,'(i,4g20.10)') 0, 0.d0, 0.d0, sum( res(1:np))

      ! refine result using non-linear optimization
      ! subject to possible constraints
      allocate( x(4*np,1,1), fi(n))
      ! set up vector of unknowns
      do i = 1, np
        x(0*np+i,1,1) = sqrt( abs( dble( res(i))))
        x(1*np+i,1,1) = aimag( res(i))
        x(2*np+i,1,1) = dble( pol(i))
        x(3*np+i,1,1) = sqrt( abs( aimag( pol(i))))
      end do
      dxo = [4*np, 1]; dx(:,1) = dxo
      ! constraint parameters
      lor = 0.61803398875d0 ! Lorentzian shape of poles
      rho = 0.999d0         ! initial strength of constraints
      lambda = [1.d0, 1.d0] ! initial Lagrange multipliers
      intf0 = sum( abs( f)**2)
      con = sum( res(1:np)) - 1.d0
      ! start optimization loop
      if( .true.) then
        do it = 1, 100
          ! minimize Lagrangian
          jt = 1000; err = 1.d-3
          if( it == -1) then
            call manopt_euclid_lbfgs( x, dxo, 1, dx, &
                   cost=cost, &
                   costgrad=costgrad, &
                   stdout=6, memlen=30, epsgrad=err, maxit=jt, minstep=1.d-4)
          else
            call manopt_euclid_lbfgs( x, dxo, 1, dx, &
                   cost=cost, &
                   costgrad=costgrad, &
                   stdout=0, memlen=30, epsgrad=err, maxit=jt, minstep=1.d-4)
          end if
          pol(1:np) = cmplx( x((2*np+1):3*np,1,1), -x((3*np+1):4*np,1,1)**2, 8)
          res(1:np) = cmplx( x((0*np+1):1*np,1,1)**2, x((1*np+1):2*np,1,1), 8)
          ! update Lagrange multipliers
          lambda(:) = lambda(:) + rho/(1.d0 - rho) * [ sum( dble( res)) - 1.d0, sum( aimag( res)) ]
          if( abs( sum( res(1:np)) - 1.d0) > 0.5d0*abs( con)) rho = rho**0.5d0
          con = sum( res(1:np)) - 1.d0
          ! check for convergence
          !fi = 0.d0
          !do i = 1, np
          !  fi(:) = fi(:) + res(i) / (w - pol(i))
          !end do
          !err = sqrt( sum( abs( fi - f)**2)/intf0)
          !write(*,'(2i,7g20.10)') it, jt, err, maxval( abs( fi - f)), con, lambda, rho!/(1.d0 - rho)
          if( abs( con) < 1.d-6) exit
        end do
        ! refine result
        jt = 1000
        call manopt_euclid_lbfgs( x, dxo, 1, dx, &
               cost=cost, &
               costgrad=costgrad, &
               stdout=0, memlen=30, maxit=jt, minstep=1.d-4)
      end if
      ! sort the poles according to their strength
      call sortidx( np, -dble( abs( res(1:np))), idx(1:np))
      pol(1:np) = pol( idx)
      res(1:np) = res( idx)
      ! get final error
      fi = 0.d0
      do i = 1, np
        fi(:) = fi(:) + res(i) / (w - pol(i))
      end do
      eps = sqrt( sum( abs( fi - f)**2)/intf0)
      !write(*,*) 'MP OPTIMIZED RESULT'
      !do i = 1, np
      !  write(*,'(i,6g20.10)') i, pol(i), res(i)
      !end do
      !write(*,'(i,4g20.10)') 0, 0.d0, 0.d0, sum( res(1:np))
      !write(*,*) '--- MULTIPOLE EXPANSION ---'
      deallocate( x, fi)

      contains
        subroutine cost( x, dxo, kx, dx, fun)
          integer, intent( in)  :: dxo(2), kx, dx(2,kx)
          real(8), intent( in)  :: x(dxo(1),dxo(2),*)
          real(8), intent( out) :: fun

          integer :: ip, np
          real(8) :: c1, c2
          real(8), allocatable :: q(:), rr(:), ri(:), fr(:), fi(:)

          fun = 0.d0
          np = dxo(1)/4
          allocate( q(n), rr(n), ri(n), fr(n), fi(n))

          ! objective
          rr = dble( f); ri = aimag( f)
          do ip = 1, np
            q = 1.d0/((w - x(2*np+ip,1,1))**2 + x(3*np+ip,1,1)**4)
            fr = (x(0*np+ip,1,1)**2*(w - x(2*np+ip,1,1)) + x(1*np+ip,1,1)*x(3*np+ip,1,1)**2)*q
            fi = (x(1*np+ip,1,1)*(w - x(2*np+ip,1,1)) - x(0*np+ip,1,1)**2*x(3*np+ip,1,1)**2)*q
            rr = rr - fr; ri = ri - fi
          end do
          fun = dot_product( rr, rr) + dot_product( ri, ri)
          ! constraints
          c1 = sum(x(1:np,1,1)**2) - 1.d0; c2 = sum(x((1*np+1):2*np,1,1))
          fun = fun + lor/(1.d0-lor)*sum(x((1*np+1):2*np,1,1)**2) + &
                      lambda(1)*c1 + lambda(2)*c2 + &
                      0.5d0*rho/(1.d0-rho)*(c1**2 + c2**2)

          deallocate( q, rr, ri, fr, fi)
        end subroutine

        subroutine costgrad( x, dxo, kx, dx, fun, g, dgo, funonly)
          integer, intent( in)           :: dxo(2), kx, dx(2,kx), dgo(2)
          real(8), intent( in)           :: x(dxo(1),dxo(2),*)
          real(8), intent( out)          :: fun
          real(8), intent( inout)        :: g(dgo(1),dgo(2),*)
          logical, optional, intent( in) :: funonly

          logical :: dograd
          integer :: ip, np
          real(8) :: c1, c2
          real(8), allocatable :: q(:), rr(:), ri(:), fr(:), fi(:), gr(:,:), gi(:,:)
          real(8), allocatable :: t1(:), t2(:), t3(:), t4(:)

          dograd = .true.
          if( present( funonly)) dograd = .not. funonly

          fun = 0.d0
          np = dxo(1)/4
          allocate( q(n), rr(n), ri(n), fr(n), fi(n))
          if( dograd) then
            allocate( gr(n,4*np), gi(n,4*np))
            allocate( t1(n), t2(n), t3(n), t4(n))
          end if

          ! objective
          rr = dble( f); ri = aimag( f)
          do ip = 1, np
            q = 1.d0/((w - x(2*np+ip,1,1))**2 + x(3*np+ip,1,1)**4)
            fr = (x(0*np+ip,1,1)**2*(w - x(2*np+ip,1,1)) + x(1*np+ip,1,1)*x(3*np+ip,1,1)**2)*q
            fi = (x(1*np+ip,1,1)*(w - x(2*np+ip,1,1)) - x(0*np+ip,1,1)**2*x(3*np+ip,1,1)**2)*q
            rr = rr - fr; ri = ri - fi

            if( dograd) then
              t1 = (w - x(2*np+ip,1,1))*q; t2 = x(3*np+ip,1,1)**2*q; t3 = -x(0*np+ip,1,1)**2*q; t4 = -x(1*np+ip,1,1)*q
              gr(:,0*np+ip) =  2.d0*x(0*np+ip,1,1)*t1
              gi(:,0*np+ip) = -2.d0*x(0*np+ip,1,1)*t2
              gr(:,1*np+ip) = t2
              gi(:,1*np+ip) = t1
              gr(:,2*np+ip) = 2.d0*fr*t1 + t3
              gi(:,2*np+ip) = 2.d0*fi*t1 + t4
              gr(:,3*np+ip) = -2.d0*x(3*np+ip,1,1)*(2.d0*fr*t2 + t4)
              gi(:,3*np+ip) = -2.d0*x(3*np+ip,1,1)*(2.d0*fi*t2 - t3)
            end if
          end do
          fun = dot_product( rr, rr) + dot_product( ri, ri)
          if( dograd) then
            call dgemv( 't', n, 4*np, -2.d0, gr, n, rr, 1, 0.d0, g(1,1,1), 1)
            call dgemv( 't', n, 4*np, -2.d0, gi, n, ri, 1, 1.d0, g(1,1,1), 1)
          end if
          ! constraints
          c1 = sum(x(1:np,1,1)**2) - 1.d0; c2 = sum(x((1*np+1):2*np,1,1))
          fun = fun + lor/(1.d0-lor)*sum(x((1*np+1):2*np,1,1)**2) + &
                      lambda(1)*c1 + lambda(2)*c2 + &
                      0.5d0*rho/(1.d0-rho)*(c1**2 + c2**2)
          if( dograd) then
            do ip = 1, np
              g(0*np+ip,1,1) = g(0*np+ip,1,1) + 2.d0*x(0*np+ip,1,1)*(lambda(1) + rho/(1.d0-rho)*c1)
              g(1*np+ip,1,1) = g(1*np+ip,1,1) + 2.d0*x(1*np+ip,1,1)*lor/(1.d0-lor) + lambda(2) + rho/(1.d0-rho)*c2
            end do
            deallocate( gr, gi, t1, t2, t3, t4)
          end if
          deallocate( q, rr, ri, fr, fi)
        end subroutine

    end subroutine

    subroutine ratfun_aaa( n, z, f, asymp, eps, mmax, zer, pol, res)
      use m_linalg
      integer, intent( in)     :: n              ! #(z,f) pairs
      complex(8), intent( in)  :: z(n)           ! abscissa values
      complex(8), intent( in)  :: f(n)           ! function values f=f(z)
      complex(8), intent( in)  :: asymp          ! asymptodic behaviour (f(z) ~ 1/(asymp*z) for z --> infinity)
      real(8), intent( inout)  :: eps            ! aimed error
      integer, intent( inout)  :: mmax           ! maximum rational order
      complex(8), intent( out) :: zer(mmax)      ! zeros (of numerator)
      complex(8), intent( out) :: pol(mmax)      ! poles (zeros of denominator)
      complex(8), intent( out) :: res(mmax)      ! residues at poles

      integer :: it, j, k, nr
      real(8) :: err, f0
      complex(8) :: zr(mmax), fr(mmax), wr(mmax)

      integer, allocatable :: idx(:)
      real(8), allocatable :: sval(:)
      complex(8), allocatable :: num(:), den(:), ri(:), c(:,:), a(:,:)
      complex(8), allocatable :: lvec(:,:), rvec(:,:), eval1(:), eval2(:)

      allocate( idx(n), num(n), den(n), ri(n), c(n,mmax), a(n,mmax))
      allocate( sval(mmax), lvec(n,n), rvec(mmax,mmax))
      zr = 0.d0; fr = 0.d0; wr = 0.d0
      idx(:) = [(j,j=1,n)]
      f0 = sum( abs( f)**2)
      ! set rational interpolant to average
      !ri(:) = 0.5d0*f(:)*(cshift( z, 1) - cshift( z, -1))
      !ri(1) = 2.d0*ri(1)*(z(2) - z(1))/(z(2) - z(n))    
      !ri(n) = 2.d0*ri(n)*(z(n) - z(n-1))/(z(1) - z(n-1))      
      !ri(:) = sum( ri)/(z(n) - z(1))
      ri(:) = sum( f(:))/dble( n)
      ! find support points and weights
      do it = 1, mmax
        ! reduced number of points (support points excluded)
        nr = n - it
        ! find new support point
        j = maxloc( abs( f - ri), 1)
        zr(it) = z(j); fr(it) = f(j);
        ! update indices
        k = minloc( abs( idx - j), 1)
        idx( k:nr) = idx( (k+1):(nr+1))
        ! add column to Cauchy matrix
        c(:,it) = 1.d0 / (z(:) - zr(it))
        ! add column to Loewner matrix
        a(:,it) = (f(:) - fr(it))*c(:,it) + asymp*fr(it)*f(:)
        ! get weigths from SVD
        call zsvd( a( idx(1:nr), 1:it), sval( 1:it), lvec( 1:nr, 1:nr), rvec( 1:it, 1:it))
        wr( 1:it) = conjg( rvec( it, 1:it))
        ! build numerator and denominator
        call zgemv( 'n', n, it, zone, c, n, wr*fr, 1, zzero, num, 1)
        call zgemv( 'n', n, it, zone, c, n, wr, 1, zzero, den, 1); den = den + asymp*sum( wr*fr)
        ! build rational interpolant
        ri(:) = f(:); ri( idx(1:nr)) = num( idx(1:nr))/den( idx(1:nr))
        ! get error
        err = sqrt( sum( abs( ri - f)**2)/f0)
        if( err < eps) exit
      end do
      deallocate( idx, ri, c, a, num, den, lvec, rvec, sval)
      mmax = min( mmax, it)
      ! find zeros, poles and residues
      nr = mmax + 1
      allocate( c(nr,nr), a(nr,nr), eval1(nr), eval2(nr))
      c = 0.d0; a = 0.d0
      do j = 2, nr
        a(j,j) = 1.d0
        c(j,j) = zr(j-1)
        c(j,1) = 1.d0
      end do
      ! zeros of numerator
      c(1,2:nr) = wr(1:mmax)*fr(1:mmax)
      call zgegdiag( c, a, eval1, eval2)
      zer = 0.d0; it = 0
      do j = 1, nr
        if( abs( eval2(j)) < 1.d-23) cycle
        it = it + 1
        zer(it) = eval1(j)/eval2(j)
      end do
      ! poles (zeros of denominator)
      c(1,1) = asymp*sum( wr*fr); c(1,2:nr) = wr(1:mmax)
      call zgegdiag( c, a, eval1, eval2)
      pol = 0.d0; it = 0
      do j = 1, nr
        if( abs( eval2(j)) < 1.d-23) cycle
        it = it + 1
        pol(it) = eval1(j)/eval2(j)
      end do
      deallocate( c, a, eval1, eval2)
      ! residues at poles
      res = 0.d0; nr = 4
      allocate( c(nr,mmax), eval1(nr), den(nr), num(nr))
      eval1(:) = 1d-5*exp( twopi*zi*[(j,j=1,nr)]/dble( nr))
      do j = 1, it
        den(:) = pol(j) + eval1(:)
        do k = 1, mmax
          c(:,k) = 1.d0 / (den(:) - zr(k))
        end do
        call zgemv( 'n', nr, mmax, zone, c, nr, wr*fr, 1, zzero, num, 1)
        call zgemv( 'n', nr, mmax, zone, c, nr, wr, 1, zzero, den, 1); den = den + asymp*sum( wr*fr)
        res(j) = sum( num/den*eval1)/dble( nr)
      end do
      deallocate( c, eval1, den, num)

    end subroutine

    subroutine ratfun_vecfit( n, x, f, np, pol, res, eps, maxit)
      use m_linalg
      integer, intent( in)              :: n, np
      complex(8), intent( in)           :: x(n)
      complex(8), intent( in)           :: f(n)
      complex(8), intent( inout)        :: pol(np)
      complex(8), intent( out)          :: res(np)
      real(8), optional, intent( inout) :: eps
      integer, optional, intent( in)    :: maxit

      integer :: it, ip, itmax
      real(8) :: acc, err
      
      complex(8), allocatable :: c(:,:), y(:,:)

      ! maximum number of iterations
      itmax = 300
      if( present( maxit)) itmax = maxit
      ! convergence accuracy
      acc = 1.d-6
      if( present( eps)) acc = eps
      ! (extended) Cauchy matrix
      allocate( c(n,2*np))
      ! least squares solution
      allocate( y(2*np,1))
      ! minimization loop
      do it = 1, itmax
        do ip = 1, np
          c(:,ip) = 1.d0/(x(:) - pol(ip))
          c(:,ip+np) = -f(:)*c(:,ip)
        end do
        call zlsp( c, reshape( f, [n,1]), y)
        ! new poles are zeros if denominator
        do ip = 1, np
          c(1:np,ip) = -y(ip+np,1)
          c(ip,ip) = c(ip,ip) + pol(ip)
        end do
        call zgediag( c(1:np,1:np), pol)
        ! force poles to be in lower half of complex plane
        pol = cmplx( dble( pol), -abs( aimag( pol)), 8)
        ! check for convergence
        err = dble( dot_product( y((np+1):2*np,1), y((np+1):2*np,1)))
        if( err < acc) exit
      end do
      ! assign results
      res = y(1:np,1)
      if( present( eps)) eps = err
      ! free memory
      deallocate( c, y)
    end subroutine

    subroutine ratfun_vecfitc( n, x, f, np, pol, res, eps, maxit)
      use m_linalg
      integer, intent( in)              :: n, np
      complex(8), intent( in)           :: x(n)
      complex(8), intent( in)           :: f(n)
      complex(8), intent( inout)        :: pol(np)
      complex(8), intent( out)          :: res(np)
      real(8), optional, intent( inout) :: eps
      integer, optional, intent( in)    :: maxit

      integer :: it, ip, itmax
      real(8) :: acc, err
      
      integer, allocatable :: ipiv(:)
      complex(8), allocatable :: c(:,:), a(:,:), y(:)

      ! maximum number of iterations
      itmax = 300
      if( present( maxit)) itmax = maxit
      ! convergence accuracy
      acc = 1.d-6
      if( present( eps)) acc = eps
      ! (extended) Cauchy matrix
      allocate( c(n,2*np), a(2*np+1,2*np+1))
      ! least squares solution
      allocate( y(2*np+1), ipiv(2*np+1))
      ! minimization loop
      do it = 1, itmax
        do ip = 1, np
          c(:,ip) = 1.d0/(x(:) - pol(ip))
          c(:,ip+np) = -f(:)*c(:,ip)
        end do
        a = zzero
        call zgemm( 't', 'n', 2*np, 2*np, n, 2.d0*zone, c, n, c, n, zzero, a, 2*np+1)
        a(2*np+1,1:np) = zone
        a(1:np,2*np+1) = zone
        call zgemv( 't', n, 2*np, 2.d0*zone, c, n, f, 1, zzero, y, 1)
        y(2*np+1) = zone
        call zgesv( 2*np+1, 1, a, 2*np+1, ipiv, y, 2*np+1, ip)
        ! new poles are zeros if denominator
        do ip = 1, np
          c(1:np,ip) = -y(ip+np)
          c(ip,ip) = c(ip,ip) + pol(ip)
        end do
        call zgediag( c(1:np,1:np), pol)
        ! force poles to be in lower half of complex plane
        pol = cmplx( dble( pol), -abs( aimag( pol)), 8)
        ! check for convergence
        err = dble( dot_product( y((np+1):2*np), y((np+1):2*np)))
        if( err < acc) exit
      end do
      ! assign results
      res = y(1:np)
      if( present( eps)) eps = err
      ! free memory
      deallocate( a, c, y)
    end subroutine
end module mod_ratfun
