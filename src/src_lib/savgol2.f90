subroutine savgol2( nx, x, ny, y, f, px, py, wx, wy, dx, dy, g)
  ! Applies the Savitzky-Golay convolution filter to a 2D function.
  ! In the neighborhood of each point a 2D polynomial of degrees px and py
  ! is fittet to the data f. This polynomial can be used to approximate
  ! mixed derivatives up to order px and py. When the derivative orders
  ! dx and dy are set to zero, the original data f is smoothened. The strength
  ! of the smooting is determined by the number of neighbors and the degree of 
  ! the polynomial. The input x- and y-values are assumed to be equidistant.
  !
  ! created: 05/2019 (SeTi)
  use m_linalg
  implicit none

  integer, intent( in)  :: nx, ny      ! # of points in x/y-direction
  real(8), intent( in)  :: x( nx)      ! the x-values
  real(8), intent( in)  :: y( ny)      ! the y-values
  real(8), intent( in)  :: f( nx, ny)  ! the function values
  integer, intent( in)  :: px, py      ! degree of the fitting polynomial in x/y-direction
  integer, intent( in)  :: wx, wy      ! number of neighbors in x/y-direction 2n+1 > p
  integer, intent( in)  :: dx, dy      ! order of derivative that is returned d <= p
  real(8), intent( out) :: g( nx, ny)  ! the smoothed (interpolated) function/derivative

  integer :: i, j, k, l, ik, jl, nw, np
  real(8) :: ddx, ddy
  real(8) ::  m( (2*wx+1)*(2*wy+1), (px+1)*(py+1))
  real(8) :: mi( (px+1)*(py+1), (2*wx+1)*(2*wy+1))
  real(8) :: v1( -wx:wx), v2( -wy:wy), w( -wx:wx, -wy:wy), a( 0:px, 0:py)

  integer, parameter :: fact(0:10) = (/1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800/)

  if( 2*wx+1 .le. px) then
    write(*,*)
    write(*,'("Error (savgol2): 2*n+1 > p violated in x-direction for w = ",i3," and p = ",i3,".")') wx, px
    stop
  end if
  if( 2*wy+1 .le. py) then
    write(*,*)
    write(*,'("Error (savgol2): 2*n+1 > p violated in y-direction for w = ",i3," and p = ",i3,".")') wy, py
    stop
  end if
  if( (dx .gt. px) .or. (dy .gt. py)) then
    write(*,*)
    write(*,'("Error (savgol2): The derivative order must not be larger than the polynomial degree.")')
    stop
  end if

  ddx = 1.d0
  if( nx .gt. 1) ddx = (x(nx) - x(1))/dble( nx -1)
  ddy = 1.d0
  if( ny .gt. 1) ddy = (y(nx) - y(1))/dble( ny -1)

  nw = (2*wx+1)*(2*wy+1)
  np = (px+1)*(py+1)

  do i = -wx, wx
    v1(i) = i
  end do
  do i = -wy, wy
    v2(i) = i
  end do

  k = 0
  do i = 0, px
    do j = 0, py
      k = k + 1
      m(:,k:k) = reshape( matmul( reshape( v1**i, (/2*wx+1, 1/)), reshape( v2**j, (/1, 2*wy+1/))), (/nw, 1/))
    end do
  end do

  call rpinv( m, mi)
  g = 0.d0

  do i = 1, nx
    do j = 1, ny
      w = 0.d0
      do k = -wx, wx
        ik = i+k
        ik = min( max( 1, ik), nx)
        do l = -wy, wy
          jl = j+l
          jl = min( max( 1, jl), ny)
          w(k,l) = f(ik,jl)
          call dgemv( 'n', np, nw, 1.d0, mi, np, w, 1, 0.d0, a, 1)
          g(i,j) = a(dx,dy)*(dble( fact( dx))/ddx**dx)*(dble( fact( dy))/ddy**dy)
        end do
      end do
    end do
  end do
  return
end subroutine savgol2
