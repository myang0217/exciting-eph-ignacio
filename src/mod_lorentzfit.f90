module m_lorentzfit
  use m_linalg
  implicit none

  contains

  subroutine singleLorentz( p, N, x, y)
    ! computes a single Lorentzian specified by the parameters p
    ! for a given set of x-values
    integer, intent( in)  :: N      ! # x-values
    real(8), intent( in)  :: p(3)   ! parameters (amplitude, center, width (HWHM))
    real(8), intent( in)  :: x(N)   ! set of x-values to evaluate at
    
    real(8), intent( out) :: y(N)   ! corresponding Lorentzian

    integer :: i
    
    do i = 1, N
      y(i) = p(1)/(1+((x(i)-p(2))/p(3))**2)
    end do
    return
  end subroutine singleLorentz

  subroutine multipleLorentz( p, M, N, x, y)
    ! computes the sum of multiple Lorentzians speciefied by the paramters p
    ! for a given set of x-values
    integer, intent( in)  :: M      ! # Lorentzians
    integer, intent( in)  :: N      ! # x-values
    real(8), intent( in)  :: p(3,M) ! parameters (amplitudes, centers, widths (HWHM))
    real(8), intent( in)  :: x(N)   ! set of x-values to evaluate at
    
    real(8), intent( out) :: y(N)   ! corresponding Lorentzian

    integer :: i
    real(8) :: l(N)

    y = 0.d0
    do i = 1, M
      call singleLorentz( p(:,i), N, x, l)
      y = y + l
    end do
    return
  end subroutine multipleLorentz

  subroutine zeros( y, N, z, Nz)
    ! finds all indices z before which the values in y change sign
    integer, intent( in)  :: N      ! # y-values
    real(8), intent( in)  :: y(N)   ! set of y-values

    integer, intent( out) :: z(N)   ! set of zero indices
    integer, intent( out) :: Nz     ! # zeros

    integer :: i

    z = 0
    Nz = 0
    do i = 2, N
      if( y(i-1)*y(i) .lt. 1.d-8) then
        Nz = Nz + 1
        z( Nz) = i
      end if
    end do
    return
  end subroutine zeros

  subroutine maxArea( y, N, A, i1, i2)
    ! finds the area with the largest absolute value included between the function y and the x-axis
    ! and the corresponding interval
    integer, intent( in)  :: N      ! # y-values
    real(8), intent( in)  :: y(N)   ! set of y-values

    real(8), intent( out) :: A      ! maximum Area (absolute value)
    integer, intent( out) :: i1, i2 ! indices describing corresponding interval

    integer :: i, j, Nz, z(N), zt(N+2)
    real(8) :: b

    call zeros( y, N, z, Nz)
    zt = 0
    zt(1) = 1
    zt( 2:(1+Nz)) = z
    zt( 2+Nz) = N

    A = 0.d0
    i1 = 0
    i2 = 0
    do i = 1, Nz + 1
      b = 0.d0
      do j = zt(i), zt( i+1)-1
        b = b + 0.5d0*(y(j)+y( j+1))
      end do
      if( abs( A) .lt. abs( b)) then
        i1 = max( zt(i)-1, 1)
        i2 = zt( i+1)
        A = b
      end if
    end do
    return
  end subroutine maxArea

  subroutine connected( idx, N, i1, i2)
    ! finds the larges connected segment of indices contained in idx
    ! i1, i2 are the start end ending point of the segment
    integer, intent( in)  :: N      ! # of indices
    integer, intent( in)  :: idx(N) ! set of indices

    integer, intent( out) :: i1, i2 ! start, end of largest connected segment

    integer :: i, m0, m, t(N), srt(N)

    call sortidx( N, dble( idx), srt)
    t = idx( srt)
    i1 = t(1)
    i2 = t(1)
    m0 = 0
    m = 0
    do i = 2, N
      if( t(i) .eq. t(i-1)+1) then
        m = m + 1
      else
        m = 0
      end if
      if( m .gt. m0) then
        i2 = t( i-1)+1
        i1 = i2 - m
        m0 = m
      end if
    end do
    return
  end subroutine connected

  subroutine singleLorentzFit( x, y, N, p)
    ! fits a Lorentzian specified by the parameters p to a function y = f(x)
    ! This is done by finding the parabola best fitting 1/y
    integer, intent( in)  :: N          ! # x/y-values
    real(8), intent( in)  :: x(N), y(N) ! set of x/y-values

    real(8), intent( out) :: p(3)       ! parameters of fitted Lorentzian (amplitude, center, width (HWHM))

    integer :: i, sgn
    real(8) :: xnorm, ynorm, mat(N,3), rhs(N,1), p0(3,1)

    if( N .lt. 3) then
      write(*,*) " Error (singleLorentzFit): At least three x-y pairs must be provided."
      stop
    end if

    xnorm = maxval( x) - minval( x)
    ynorm = sum( abs( y))/dble( N-1)
    sgn = 1
    if( y( nint( 0.5d0*N)) .lt. 0.d0) sgn = -1
    do i = 1, N
      mat(i,3) = 1.d0
      mat(i,2) = x(i)/xnorm
      mat(i,1) = mat(i,2)*mat(i,2)
      rhs(i,1) = dble( sgn)*(ynorm/y(i))
    end do
    
    ! solve linear least squares problem
    call rlsp( mat, rhs, p0)

    p = (/0.d0, 0.d0, 1.d0/)
    p(1) = dble( sgn)*ynorm*(4.d0*p0(1,1)/(4.d0*p0(1,1)*p0(3,1) - p0(2,1)*p0(2,1)))
    p(2) = -0.5d0*xnorm*p0(2,1)/p0(1,1)
    p(3) = xnorm*abs( 0.5d0*sqrt( cmplx( 4.d0*p0(1,1)*p0(3,1) - p0(2,1)*p0(2,1), 0.d0, 8))/p0(1,1))
    return
  end subroutine singleLorentzFit

  subroutine bestSingleLorentzFit( x, y, N, p, Nmin)
    ! finds the best fitting Lorentzian to a function y = f(x) that can be obtained
    ! by fitting a parabola to the inverse of a part of the provided data
    integer, intent( in)  :: N             ! # x/y-values
    real(8), intent( in)  :: x(N), y(N)    ! set of x/y-values
    integer, optional, intent( in) :: Nmin ! minimum # x/y-values to fit to

    real(8), intent( out) :: p(3)          ! parameters of fitted Lorentzian (amplitude, center, width (HWHM))

    integer :: i, sgn, srt(N), N0, N1, i1, i2
    real(8) :: e, e0, p0(3), f(N)

    N0 = 1
    if( present( Nmin)) N0 = Nmin

    sgn = 1
    if( y( nint( 0.5d0*N)) .lt. 0.d0) sgn = -1
    ! get positive y in descending order
    call sortidx( N, -dble( sgn)*y, srt)
    do N1 = N, 1, -1
      if( dble( sgn)*y( srt( N1)) .gt. 0.d0) exit
    end do

    e0 = 1.d127
    p = (/0.d0, 0.d0, 1.d0/)
    do i = N1, N0, -1
      call connected( srt( 1:i), i, i1, i2)
      i1 = i1 + 1
      i2 = i2 - 1
      if( i2 - i1 .gt. 3) then
        call singleLorentzFit( x( i1:i2), y( i1:i2), i2-i1+1, p0)
        call singleLorentz( p0, N, x, f)
        e = dot_product( y-f, y-f)
        ! accept only fits with maximum in given interval
        if( (e .lt. e0) .and. (p0(2) .gt. x(1)) .and. (p0(2) .lt. x(N))) then
          e0 = e
          p = p0
        end if
      end if
    end do
    return
  end subroutine bestSingleLorentzFit

  subroutine multipleLorentzFit1D( x, y, N, p, No, e, epsRel, epsAbs, Nloc, Nred, maxit)
    ! decomposes a function y = f(x) into a linear combination of Lorentzians
    integer, intent( in)               :: N                 ! # x/y-values
    real(8), intent( in)               :: x(N), y(N)        ! set of x/y-values

    integer, intent( out)              :: No                ! # output Lorentzians
    real(8), allocatable, intent( out) :: p(:,:)            ! parameters of fitted Lorentzians
    real(8), intent( out)              :: e                 ! achieved error

    real(8), optional, intent( in)     :: epsRel, epsAbs    ! aimed relative/absolute error
    integer, optional, intent( in)     :: Nloc              ! # points for local fit
    integer, optional, intent( in)     :: Nred              ! # points for reduction
    integer, optional, intent( in)     :: maxit             ! maximum # iterations

    integer :: Nl, Nr, itmax, it, i, j, i1, i2
    real(8) :: emax, e0, dx, t1, A, p0(3), p1(3), pr(3,1), pt(3,N), s(N), res(N)
    logical :: rel

    real(8), allocatable :: xr(:), yr(:,:), rmat(:,:), irmat(:,:), xf(:), yf(:)

    ! set optional and default parameters
    rel = .true.
    if( present( epsAbs)) rel = .false.
    emax = 1.d-3
    if( present( epsRel)) emax = epsRel
    if( present( epsAbs)) emax = epsAbs
    Nl = 100
    if( present( Nloc)) Nl = Nloc
    Nr = 20
    if( present( Nred)) Nr = Nred
    itmax = N
    if( present( maxit)) itmax = min( itmax, maxit)

    dx = (x(N) - x(1))/dble( N-1)
    allocate( xr( Nr), yr( Nr, 1), rmat( Nr, 3), irmat( 3, Nr), xf( Nl), yf( Nl))
    call linspace( -3.d0, 3.d0, Nr, xr)
    do i = 1, Nr
      rmat(i,3) = 1.d0
      rmat(i,2) = xr(i)
      rmat(i,1) = xr(i)*xr(i)
    end do
    call rpinv( rmat, irmat)
    e0 = sqrt( dot_product( y, y)/dble( N-1))
    
    e = e0
    if( rel) e = e/e0
    No = 0
    it = 0
    p0 = 0.d0
    res = y
    call maxArea( res, N, A, i1, i2)

    do while( (e .gt. emax) .and. (it .lt. itmax))
      ! find best locally fitting Lorentzian
      call linspace( x( i1), x( i2), Nl, xf)
      call interp1d( N, x, res, Nl, xf, yf, 'spline')
      call bestSingleLorentzFit( xf, yf, Nl, p0)
      if( p0(1) .eq. 0.d0) then
        p0(2) = 0.5d0*(x( i1) + x( i2))
        p0(1) = sum( abs( yf))/dble( Nl)
        if( yf( nint( 0.5d0*Nl)) .lt. 0.d0) p0(1) = -p0(1)
        p0(3) = abs( A*dx/(pi*p0(1)))
      end if

      ! combine this Lorentzian with the most similar one if possible
      if( No .gt. 0) then
        call lorentzSim( p0, pt( :, 1:No), No, s( 1:No))
        i = maxloc( s( 1:No), 1)
        if( s(i) .gt. 0.9d0) then
          p1(1) = pt(1,i)/p0(1)
          p1(2) = (pt(2,i) - p0(2))/p0(3)
          p1(3) = pt(3,i)/p0(3)
          call multipleLorentz( (/p1,(/1.d0, 0.d0, 1.d0/)/), 2, Nr, xr, yr(:,1))
          do j = 1, Nr
            yr(j,1) = 1.d0/yr(j,1)
          end do
          !call rlsp( rmat, yr, pr)
          pr = matmul( irmat, yr)
          p1(1) = 4.d0*pr(1,1)/(4.d0*pr(1,1)*pr(3,1) - pr(2,1)*pr(2,1))
          p1(2) = -0.5d0*pr(2,1)/pr(1,1)
          p1(3) = abs( 0.5d0*sqrt( 4.d0*pr(1,1)*pr(3,1) - pr(2,1)*pr(2,1))/pr(1,1))
          pt(1,i) = p1(1)*p0(1)
          pt(2,i) = p1(2)*p0(3) + p0(2)
          pt(3,i) = p1(3)*p0(3)
        else
          No = No + 1
          pt( :, No) = p0
        end if
      else
        No = No + 1
        pt( :, No) = p0
      end if

      call multipleLorentz( pt( :, 1:No), No, N, x, res)
      res = y - res
      call maxArea( res, N, A, i1, i2)
      e = sqrt( dot_product( res, res)/dble( N-1))
      if( rel) e = e/e0
      it = it + 1
    end do

    if( (No .eq. 1) .and. (abs( pt(1,1)) .lt. emax*1.d-6)) No = 0
    if( allocated( p)) deallocate( p)
    allocate( p( 3, max( 1, No)))
    p = 0.d0
    p(3,:) = 1.d0
    do i = 1, No
      p(:,i) = pt(:,i)
    end do

    deallocate( xr, yr, rmat, xf, yf)
    return
  end subroutine multipleLorentzFit1D

  subroutine multipleLorentzFit2D( x, y, z, Nx, Ny, px, py, c, Nox, Noy, e, &
          epsRel, epsAbs, epsLD, Nloc, Nred, Nfitx, Nfity, Nbasex, Nbasey, maxitx, maxity)
    use m_plotmat
    integer, intent( in)               :: Nx, Ny
    real(8), intent( in)               :: x( Nx), y( Ny)
    real(8), intent( in)               :: z( Nx, Ny)

    integer, intent( out)              :: Nox, Noy
    real(8), allocatable, intent( out) :: px(:,:), py(:,:), c(:,:)
    real(8), intent( out)              :: e

    real(8), optional, intent( in)     :: epsRel, epsAbs    ! aimes realtive/absolute error
    real(8), optional, intent( in)     :: epsLD             ! threshold for removal of linearly dependent basis functions
    integer, optional, intent( in)     :: Nloc              ! # points for local fit
    integer, optional, intent( in)     :: Nred              ! # points for reduction
    integer, optional, intent( in)     :: Nfitx, Nfity      ! # points for global fit in x/y-direction
    integer, optional, intent( in)     :: Nbasex, Nbasey    ! fixed x/y basis set size
    integer, optional, intent( in)     :: maxitx, maxity    ! maximum # iterations
    
    ! parameters
    integer :: Nl, Nr, Nfx, Nfy, Nbx, Nby, itmaxx, itmaxy, Nix, Niy
    real(8) :: emax, eld
    logical :: rel

    ! external functions
    real(8), external :: ddot

    ! local variables
    integer :: i, j
    real(8) :: zmax, e0
    integer, allocatable :: srt(:)
    real(8), allocatable :: xf(:), yf(:), xi(:), yi(:), zt(:), zf(:), zi(:), ptx(:,:), pty(:,:), olp(:,:), lf(:,:), a(:,:), b(:,:), q(:,:), r(:,:)

    ! set optional and default parameters
    rel = .true.
    if( present( epsAbs)) rel = .false.
    emax = 1.d-3
    if( present( epsRel)) emax = epsRel
    if( present( epsAbs)) emax = epsAbs
    eld = 0.1
    if( present( epsLD)) eld = epsLD
    Nl = 100
    if( present( Nloc)) Nl = Nloc
    Nr = 20
    if( present( Nred)) Nr = Nred
    Nfx = 400
    if( present( Nfitx)) Nfx = Nfitx
    Nfy = 400
    if( present( Nfity)) Nfy = Nfity
    Nbx = Nx
    if( present( Nbasex)) Nbx = Nbasex
    Nby = Ny
    if( present( Nbasey)) Nby = Nbasey
    itmaxx = Nx
    if( present( maxitx)) itmaxx = min( itmaxx, maxitx)
    itmaxy = Ny
    if( present( maxity)) itmaxy = min( itmaxy, maxity)
    Nix = 8*Nx
    Niy = 8*Ny

    allocate( xf( Nfx), yf( Nfy), xi( Nix), yi( Niy))
    call linspace( x(1), x( Nx), Nfx, xf)
    call linspace( x(1)-0.5d0*(x( Nx)-x(1)), x( Nx)+0.5d0*(x( Nx)-x(1)), Nix, xi)
    call linspace( y(1), y( Ny), Nfy, yf)
    call linspace( y(1)-0.5d0*(y( Ny)-y(1)), y( Ny)+0.5d0*(y( Ny)-y(1)), Niy, yi)

    Nox = 0
    Noy = 0
    e = 0.d0

    !============================
    !     FIT IN X-DIRECTION
    !============================

    ! decompose data in x-direction
    allocate( zt( Nx), zf( Nfx), zi( Nix))
    zt = 0.d0
    do i = 1, Ny
      zt = zt + abs( z(:,i))
    end do
    zt = zt/dble( Ny)
    zmax = maxval( abs( z))
    call interp1d( Nx, x, zt, Nfx, xf, zf, 'spline')
    if( rel) then
      call multipleLorentzFit1D( xf, zf, Nfx, ptx, Nox, e, epsRel=emax, maxit=itmaxx)
    else
      call multipleLorentzFit1D( xf, zf, Nfx, ptx, Nox, e, epsAbs=emax, maxit=itmaxx)
    end if

    ! build overlap between basis functions
    allocate( olp( Nox, Nox), q( max( 3, Nox), Nox), r( Nox, Nox))
    do i = 1, Nox
      olp(i,i) = 2.d0*sqrt( ptx(3,i)*ptx(3,i))*(ptx(3,i) + ptx(3,i))/((ptx(2,i) - ptx(2,i))**2 + (ptx(3,i) + ptx(3,i))**2)
      do j = 1, i-1
        olp(i,j) = 2.d0*sqrt( ptx(3,i)*ptx(3,j))*(ptx(3,i) + ptx(3,j))/((ptx(2,i) - ptx(2,j))**2 + (ptx(3,i) + ptx(3,j))**2)
        olp(j,i) = olp(i,j)
      end do
    end do

    ! remove linearly dependent basis functions using QR-factorization
    call rqr( olp, r=r)
    j = Nox
    Nox = 0
    do i = 1, j
      if( abs( r(i,i)) .gt. eld) then
        Nox = Nox + 1
        q( 1:3, Nox) = ptx( :, i)
      end if
    end do
    ptx( :, 1:Nox) = q( 1:3, 1:Nox)

    ! further remove less contributing components to reach fixed basis size if needed
    if( Nox .gt. Nbx) then
      allocate( lf( Nix, Nox), a( Nox, 1), srt( Nox))
      do i = 1, Nox
        call singleLorentz( (/1.d0, ptx(2,i), ptx(3,i)/), Nix, xi, lf(:,i))
      end do
      call interp1d( Nfx, xf, zf, Nix, xi, zi, 'spline')
      call rlsp( lf, reshape( zi, (/Nix, 1/)), a)
      call sortidx( Nox, -abs( a(:,1)*ptx(3,:)), srt)
      ptx = ptx( :, srt)
      deallocate( lf, a, srt)
    end if
    Nox = min( Nbx, Nox)

    ! find best amplitudes of basis functions for each y
    ! use denser sampling to avoid overfitting
    allocate( lf( Nix, Nox), a( Nox, Ny))
    do i = 1, Nox
      call singleLorentz( (/1.d0, ptx(2,i), ptx(3,i)/), Nix, xi, lf(:,i))
    end do
    do i = 1, Ny
      call interp1d( Nx, x, z(:,i), Nix, xi, zi, 'spline')
      call rlsp( lf, reshape( zi, (/Nix, 1/)), a(:,i:i))
      do j = 1, Nox
        if( abs( a(j,i)) .gt. 2.d0*zmax) a(j,i) = 0.d0
      end do
    end do
    deallocate( lf)

    !============================
    !     FIT IN Y-DIRECTION
    !============================

    ! decompose data in y-direction
    a = transpose( a)
    deallocate( zt, zf, zi)
    allocate( zt( Ny), zf( Nfy), zi( Niy))
    zt = 0.d0
    do i = 1, Nox
      zt = zt + abs( a(:,i))
    end do
    zt = zt/dble( Nox)
    zmax = maxval( abs( a))
    call interp1d( Ny, y, zt, Nfy, yf, zf, 'spline')
    if( rel) then
      call multipleLorentzFit1D( yf, zf, Nfy, pty, Noy, e, epsRel=emax, maxit=itmaxy)
    else
      call multipleLorentzFit1D( yf, zf, Nfy, pty, Noy, e, epsAbs=emax, maxit=itmaxy)
    end if

    ! build overlap between basis functions
    deallocate( olp, q, r)
    allocate( olp( Noy, Noy), q( max( 3, Noy), Noy), r( Noy, Noy))
    do i = 1, Noy
      olp(i,i) = 2.d0*sqrt( pty(3,i)*pty(3,i))*(pty(3,i) + pty(3,i))/((pty(2,i) - pty(2,i))**2 + (pty(3,i) + pty(3,i))**2)
      do j = 1, i-1
        olp(i,j) = 2.d0*sqrt( pty(3,i)*pty(3,j))*(pty(3,i) + pty(3,j))/((pty(2,i) - pty(2,j))**2 + (pty(3,i) + pty(3,j))**2)
        olp(j,i) = olp(i,j)
      end do
    end do

    ! remove linearly dependent basis functions using QR-factorization
    call rqr( olp, r=r)
    j = Noy
    Noy = 0
    do i = 1, j
      if( abs( r(i,i)) .gt. eld) then
        Noy = Noy + 1
        q( 1:3, Noy) = pty( :, i)
      end if
    end do
    pty( :, 1:Noy) = q( 1:3, 1:Noy)

    ! further remove less contributing components to reach fixed basis size if needed
    if( Noy .gt. Nby) then
      allocate( lf( Niy, Noy), b( Noy, 1), srt( Noy))
      do i = 1, Noy
        call singleLorentz( (/1.d0, pty(2,i), pty(3,i)/), Niy, yi, lf(:,i))
      end do
      call interp1d( Nfy, yf, zf, Niy, yi, zi, 'spline')
      call rlsp( lf, reshape( zi, (/Niy, 1/)), b)
      call sortidx( Noy, -abs( b(:,1)), srt)
      pty = pty( :, srt)
      deallocate( lf, b, srt)
    end if
    Noy = min( Nby, Noy)

    ! find best amplitudes of product basis functions for each x/y pair
    ! use denser sampling to avoid overfitting
    allocate( lf( Niy, Noy), b( Noy, Nox))
    do i = 1, Noy
      call singleLorentz( (/1.d0, pty(2,i), pty(3,i)/), Niy, yi, lf(:,i))
    end do
    do i = 1, Nox
      call interp1d( Ny, y, a(:,i), Niy, yi, zi, 'spline')
      call rlsp( lf, reshape( zi, (/Niy, 1/)), b(:,i:i))
      do j = 1, Noy
        if( abs( b(j,i)) .gt. 2.d0*zmax) b(j,i) = 0.d0
      end do
    end do
    deallocate( lf)

    !============================
    !      FINALIZE RESULTS
    !============================
    if( .not. present( Nbasex)) Nbx = Nox
    if( .not. present( Nbasey)) Nby = Noy
    if( allocated( px)) deallocate( px)
    if( allocated( py)) deallocate( py)
    if( allocated( c)) deallocate( c)
    allocate( px( 2, Nbx), py( 2, Nby), c( Nbx, Nby))
    px = 0.d0
    px(2,:) = 1.d0
    py = 0.d0
    py(2,:) = 1.d0
    c = 0.d0
    c( 1:Nox, 1:Noy) = transpose( b)
    deallocate( a, b)
    allocate( a( Nx, Nox), b( Ny, Noy), lf( Nx, Ny))
    lf = z
    e0 = sqrt( ddot( Nx*Ny, lf, 1, lf, 1)/dble( Nx*Ny-1))
    do i = 1, Nox
      px(:,i) = ptx(2:3,i)
      call singleLorentz( (/1.d0, px(1,i), px(2,i)/), Nx, x, a(:,i))
    end do
    do i = 1, Noy
      py(:,i) = pty(2:3,i)
      call singleLorentz( (/1.d0, py(1,i), py(2,i)/), Ny, y, b(:,i))
    end do
    do i = 1, Nox
      do j = 1, Noy
        call dgemm( 'n', 't', Nx, Ny, 1, -c(i,j), &
               a(:,i), Nx, &
               b(:,j), Ny, 1.d0, &
               lf, Nx)
      end do
    end do
    e = sqrt( ddot( Nx*Ny, lf, 1, lf, 1)/dble( Nx*Ny-1))
    if( rel) e = e/e0
    deallocate( a, b, lf, xf, xi, yf, yi, zf, zi, zt, ptx, pty, olp)
  end subroutine multipleLorentzFit2D

  subroutine lorentzSim( p0, p, N, s)
    ! returns a measure for the similarity of two Lorentzians
    integer, intent( in)  :: N
    real(8), intent( in)  :: p0(3), p(3,N)
    real(8), intent( out) :: s(N)

    integer :: i
    do i = 1, N
      s(i) = sqrt( 2.d0*sqrt( p0(3)*p(3,i))*(p0(3) + p(3,i))/((p0(2) - p(2,i))**2 + (p0(3) + p(3,i))**2))
      if( p0(1)*p(1,i) .lt. 0.d0) s(i) = -s(i)
    end do
    return
  end subroutine lorentzSim


  subroutine linspace( x1, x2, N, x)
    real(8), intent( in)  :: x1, x2
    integer, intent( in)  :: N
    real(8), intent( out) :: x(N)

    integer :: i
    real(8) :: dx

    dx = (x2 - x1)/dble( N-1)
    do i = 1, N
      x(i) = x1 + dx*(i-1)
    end do
    return
  end subroutine linspace
end module m_lorentzfit
