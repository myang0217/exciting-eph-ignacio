module mod_wannier_interpolate
  use modmain
  use mod_wannier
  use m_linalg
  use m_plotmat
  use mod_opt_tetra
  implicit none

! module variables
  type( k_set)     :: wfint_kset                       ! k-point set on which the interpolation is performed
  type( Gk_set)    :: wfint_Gkset                      ! G+k-point set on which the interpolation is performed
  logical	   :: wfint_initialized = .false.
  logical	   :: wfint_mindist                    ! use minimum distances interpolation method
  real(8)	   :: wfint_efermi                     ! interpolated fermi energy
  type( tetra_set) :: wfint_tetra                      ! tetrahedra for tetrahedron integration
  real(8)          :: wfint_vvbm(3), wfint_evbm
  real(8)          :: wfint_vcbm(3), wfint_ecbm

  real(8), allocatable    :: wfint_eval(:,:)        ! interpolated eigenenergies
  complex(8), allocatable :: wfint_transform(:,:,:) ! corresponding expansion coefficients
  integer, allocatable    :: wfint_bandmap(:)       ! map from interpolated bands to original bands

  real(8), allocatable    :: wfint_occ(:,:)         ! interpolated occupation numbers
  real(8), allocatable    :: wfint_phase(:,:)       ! summed phase factors in interpolation
  complex(8), allocatable :: wfint_pqr(:,:)

  ! often used matrices
  complex(8), allocatable :: wfint_hwr(:,:,:)          ! R-dependent Hamiltonian in Wannier gauge

! methods
  contains
    !BOP
    ! !ROUTINE: wfint_init
    ! !INTERFACE:
    !
    subroutine wfint_init( int_kset, evalin, serial)
      ! !USES:
        use m_getunit 
      ! !INPUT PARAMETERS:
      !   int_kset : k-point set on which the interpolation is performed on (in, type k_set)
      ! !DESCRIPTION:
      !   Sets up the interpolation grid and calculates the interpolated eigenenergies as well as 
      !   the corresponding expansion coefficients and phasefactors for the interpolated wavefunctions.
      !
      ! !REVISION HISTORY:
      !   Created July 2017 (SeTi)
      !EOP
      !BOC
      type( k_set), intent( in)      :: int_kset
      real(8), optional, intent( in) :: evalin( wf_fst:wf_lst, wf_kset%nkpt)
      logical, optional, intent( in) :: serial
    
      integer :: fst, lst
      real(8), allocatable :: evalfv(:,:), evalin_(:,:)

      if( wfint_initialized) call wfint_destroy

      wfint_mindist = input%properties%wannier%mindist
      wfint_kset = int_kset
    
      allocate( evalin_( wf_fst:wf_lst, wf_kset%nkpt))
    
      if( present( evalin)) then
        evalin_ = evalin
      else
        call wfhelp_geteval( evalfv, fst, lst)
        if( (fst .gt. wf_fst) .or. (lst .lt. wf_lst)) then
          write(*,*)
          write(*, '("Error (wfint_init): Eigenenergies out of range of Wannier functions.")')
          stop
        end if
        evalin_ = evalfv( wf_fst:wf_lst, :)
        deallocate( evalfv)
      end if
    
      if( wfint_mindist) call wannier_mindist( input%structure%crystal%basevect, wf_kset, wf_nrpt, wf_rvec, wf_nwf, wf_centers, wf_wdistvec, wf_wdistmul)
      call wfint_fourierphases( wfint_kset, wf_nrpt, wf_rvec, wf_rmul, wfint_pqr, wfint_phase)

      allocate( wfint_eval( wf_nwf, wfint_kset%nkpt))
      allocate( wfint_transform( wf_nwf, wf_nwf, wfint_kset%nkpt))
      
      if( present( serial)) then
        call wfint_interpolate_eigsys( evalin_, serial=serial)
      else
        call wfint_interpolate_eigsys( evalin_)
      end if
    
      wfint_initialized = .true.
      deallocate( evalin_)

      return
    end subroutine wfint_init
    !EOC

!--------------------------------------------------------------------------------------
    
    subroutine wfint_destroy
      if( allocated( wfint_phase))     deallocate( wfint_phase)
      if( allocated( wfint_pqr))       deallocate( wfint_pqr)
      if( allocated( wfint_eval))      deallocate( wfint_eval)
      if( allocated( wfint_transform)) deallocate( wfint_transform)
      if( allocated( wfint_occ))       deallocate( wfint_occ)
      if( allocated( wfint_hwr))       deallocate( wfint_hwr)
      !if( allocated( wfint_bandmap))   deallocate( wfint_bandmap)
      !if( allocated( wfint_rvec))      deallocate( wfint_rvec)
      !if( allocated( wfint_rmul))      deallocate( wfint_rmul)
      !if( allocated( wfint_wdistvec))  deallocate( wfint_wdistvec)
      !if( allocated( wfint_wdistmul))  deallocate( wfint_wdistmul)
      call delete_k_vectors( wfint_kset)
      call delete_Gk_vectors( wfint_Gkset)
      wfint_initialized = .false.
      return
    end subroutine wfint_destroy

!--------------------------------------------------------------------------------------
!   BASIC FUNCTIONS OFTEN NEEDED IN GENERIC INTERPOLATION
!--------------------------------------------------------------------------------------
    
    ! precomputes Fourier factors appearing in generic interpolation
    subroutine wfint_fourierphases( qset, nrpt, rvec, rmul, pqr, phase)
      type( k_set), intent( in)                    :: qset
      integer, intent( in)                         :: nrpt
      integer, intent( in)                         :: rvec( 3, nrpt)
      integer, intent( in)                         :: rmul( nrpt)
      complex(8), allocatable, intent( out)        :: pqr(:,:)
      real(8), allocatable, optional, intent( out) :: phase(:,:)

      integer :: ir, iq
      real(8) :: d
      complex(8), allocatable :: auxmat(:,:)

      if( allocated( pqr)) deallocate( pqr)
      allocate( pqr( qset%nkpt, nrpt))

      do ir = 1, nrpt
        do iq = 1, qset%nkpt
          d = twopi*dot_product( qset%vkl( :, iq), dble( rvec( :, ir)))
          pqr( iq, ir) = cmplx( cos( d), sin( d), 8)/dble( rmul( ir))
        end do
      end do

      if( present( phase)) then
        if( allocated( phase)) deallocate( phase)
        allocate( phase( wf_kset%nkpt, qset%nkpt))
        allocate( auxmat( qset%nkpt, wf_kset%nkpt))
        call zgemm( 'n', 't', qset%nkpt, wf_kset%nkpt, nrpt, zone, &
               pqr, qset%nkpt, &
               wf_pkr, wf_kset%nkpt, zzero, &
               auxmat, qset%nkpt)
        phase = dble( transpose( auxmat))/wf_kset%nkpt
        deallocate( auxmat)
      end if

      return
    end subroutine wfint_fourierphases

!--------------------------------------------------------------------------------------

    ! generates real-space Hamiltonian in Wannier gauge H_{mn}(R)
    subroutine wfint_genhwr( evalin)
      use m_plotmat
      real(8), intent( in) :: evalin( wf_fst:wf_lst, wf_kset%nkpt)

      integer :: ik, ir, ist, jst, igroup
      complex(8), allocatable :: hwk(:,:,:), auxmat(:,:)

      if( allocated( wfint_hwr)) deallocate( wfint_hwr)
      allocate( wfint_hwr( wf_nwf, wf_nwf, wf_nrpt))

      allocate( hwk( wf_nwf, wf_nwf, wf_kset%nkpt), auxmat( wf_fst:wf_lst, wf_nwf))
      hwk = zzero
      do ik = 1, wf_kset%nkpt
        do ist = wf_fst, wf_lst
          do jst = 1, wf_nwf
            auxmat( ist, jst) = wf_transform( ist, jst, ik)*evalin( ist, ik)
          end do
        end do
        do igroup = 1, wf_ngroups
          call zgemm( 'c', 'n', wf_groups( igroup)%nwf, wf_groups( igroup)%nwf, wf_nst, zone, &
                 wf_transform( :, wf_groups( igroup)%fwf, ik), wf_nst, &
                 auxmat( :, wf_groups( igroup)%fwf), wf_nst, zzero, &
                 hwk( wf_groups( igroup)%fwf, wf_groups( igroup)%fwf, ik), wf_nwf)
        end do
      end do
      call wfint_ftk2r( hwk, wfint_hwr, 1)
      !do ir = 1, wf_nrpt
      !  write(*,'(i,f20.10)') ir, maxval( abs( wfint_hwr(:,:,ir)))
      !end do
      deallocate( hwk, auxmat)

      return
    end subroutine wfint_genhwr

!--------------------------------------------------------------------------------------

    ! transform matrix from coarse reciprocal-space grid to real-space grid
    subroutine wfint_ftk2r( mwk, mwr, ld)
      complex(8), intent( in)  :: mwk( wf_nwf, wf_nwf, ld, wf_kset%nkpt)
      complex(8), intent( out) :: mwr( wf_nwf, wf_nwf, ld, wf_nrpt)
      integer, intent( in)     :: ld

      call zgemm( 'n', 'n', wf_nwf*wf_nwf*ld, wf_nrpt, wf_kset%nkpt, cmplx( 1.d0/wf_kset%nkpt, 0.d0, 8), &
             mwk, wf_nwf*wf_nwf*ld, &
             wf_pkr, wf_kset%nkpt, zzero, &
             mwr, wf_nwf*wf_nwf*ld)
      return
    end subroutine wfint_ftk2r

!--------------------------------------------------------------------------------------

    ! transform matrix from double coarse reciprocal-space grid to double real-space grid
    subroutine wfint_ftkk2rr( mwkk, mwrr, ld)
      complex(8), intent( in)  :: mwkk( wf_nwf, wf_nwf, ld, wf_kset%nkpt, wf_kset%nkpt)
      complex(8), intent( out) :: mwrr( wf_nwf, wf_nwf, ld, wf_nrpt, wf_nrpt)
      integer, intent( in)     :: ld

      integer :: ik1, ik2, ir1, ir2
      complex(8) :: phase( wf_kset%nkpt, wf_kset%nkpt)

#ifdef USEOMP
!$omp parallel default( shared) private( ir1, ir2, ik1, ik2, phase)
!$omp do collapse(2)
#endif
      do ir2 = 1, wf_nrpt
        do ir1 = 1, wf_nrpt
          do ik2 = 1, wf_kset%nkpt
            do ik1 = 1, wf_kset%nkpt
              phase(ik1,ik2) = wf_pkr( ik1, ir1)*wf_pkr( ik2, ir2)
            end do
          end do
          call zgemv( 'n', wf_nwf*wf_nwf*ld, wf_kset%nkpt*wf_kset%nkpt, cmplx( 1.d0/wf_kset%nkpt**2, 0.d0, 8), &
                 mwkk, wf_nwf*wf_nwf*ld, &
                 phase, 1, zzero, &
                 mwrr(1,1,1,ir1,ir2), 1)
        end do
      end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif

      return
    end subroutine wfint_ftkk2rr

!--------------------------------------------------------------------------------------

    ! transform matrix from real-space grid to fine reciprocal-space grid
    ! R-dependent summation weight can be provided
    subroutine wfint_ftr2q( mwr, mwq, iq, ld, rwgt)
      complex(8), intent( in)  :: mwr( wf_nwf, wf_nwf, ld, wf_nrpt)
      complex(8), intent( out) :: mwq( wf_nwf, wf_nwf, ld)
      integer, intent( in)     :: iq, ld
      complex(8), optional, intent( in) :: rwgt( wf_nrpt)

      integer :: ir, ist, jst
      complex(8), allocatable :: wgts(:,:)

      if( wfint_mindist) then
        mwq = zzero
        allocate( wgts( wf_nwf, wf_nwf))
        do ir = 1, wf_nrpt
          call wfint_getmindistwgts( ir, iq, wgts)
          do ist = 1, wf_nwf
            do jst = 1, wf_nwf
              if( present( rwgt)) then
                mwq( ist, jst, :) = mwq( ist, jst, :) + rwgt( ir)*wgts( ist, jst)*mwr( ist, jst, :,  ir)
              else
                mwq( ist, jst, :) = mwq( ist, jst, :) + wgts( ist, jst)*mwr( ist, jst, :, ir)
              end if
            end do
          end do
        end do
        deallocate( wgts)
      else
        if( present( rwgt)) then
          call zgemv( 'n', wf_nwf*wf_nwf*ld, wf_nrpt, zone, &
                 mwr, wf_nwf*wf_nwf*ld, &
                 wfint_pqr( iq, :)*rwgt, 1, zzero, &
                 mwq, 1)
        else
          call zgemv( 'n', wf_nwf*wf_nwf*ld, wf_nrpt, zone, &
                 mwr, wf_nwf*wf_nwf*ld, &
                 wfint_pqr( iq, :), 1, zzero, &
                 mwq, 1)
        end if
      end if
      return
    end subroutine wfint_ftr2q

!--------------------------------------------------------------------------------------

    ! transform matrix from double real-space grid to double fine reciprocal-space grid
    ! R-dependent summation weight can be provided
    subroutine wfint_ftrr2qq( mwrr, mwqq, iq1, iq2, ld, rwgt)
      complex(8), intent( in)  :: mwrr( wf_nwf, wf_nwf, ld, wf_nrpt, wf_nrpt)
      complex(8), intent( out) :: mwqq( wf_nwf, wf_nwf, ld)
      integer, intent( in)     :: iq1, iq2, ld
      complex(8), optional, intent( in) :: rwgt( wf_nrpt, wf_nrpt)

      integer :: ir1, ir2
      complex(8) :: phase( wf_nrpt, wf_nrpt)

      do ir1 = 1, wf_nrpt
        do ir2 = 1, wf_nrpt
          phase( ir1, ir2) = wfint_pqr( iq1, ir1)*wfint_pqr( iq2, ir2)
        end do
      end do

      if( present( rwgt)) then
        call zgemv( 'n', wf_nwf*wf_nwf*ld, wf_nrpt*wf_nrpt, zone, &
               mwrr, wf_nwf*wf_nwf*ld, &
               phase*rwgt, 1, zzero, &
               mwqq, 1)
      else
        call zgemv( 'n', wf_nwf*wf_nwf*ld, wf_nrpt*wf_nrpt, zone, &
               mwrr, wf_nwf*wf_nwf*ld, &
               phase, 1, zzero, &
               mwqq, 1)
      end if
      return
    end subroutine wfint_ftrr2qq

!--------------------------------------------------------------------------------------

    ! transform matrix at k from Hamiltonian to Wannier gauge
    subroutine wfint_h2wk( mhk, mwk, ik, ld, ik2)
      complex(8), intent( in)        :: mhk( wf_fst:wf_lst, wf_fst:wf_lst, ld)
      complex(8), intent( out)       :: mwk( wf_nwf, wf_nwf, ld)
      integer, intent( in)           :: ik, ld
      integer, optional, intent( in) :: ik2

      integer :: d, ip
      complex(8) :: auxmat( wf_nwf, wf_fst:wf_lst, ld)

      ip = ik
      if( present( ik2)) ip = ik2
      ! call zgemm( 'n', 'c', wf_nwf*wf_nwf, 3*natmtot, 3*natmtot, zone, &
      !        g(:,:,:,iq,ire), wf_nwf, &
      !        evecq(:,:,iq), 3*natmtot, zzero, &
      !        gp(:,:,:,iq,ire), wf_nwf)
      call zgemm( 'c', 'n', wf_nwf, wf_nst*ld, wf_nst, zone, &
             wf_transform( :, :, ik), wf_nst, &
             mhk, wf_nst, zzero, &
             auxmat, wf_nwf)
      do d = 1, ld
        call zgemm( 'n', 'n', wf_nwf, wf_nwf, wf_nst, zone, &
               auxmat( :, :, d), wf_nwf, &
               wf_transform( :, :, ip), wf_nst, zzero, &
               mwk( :, :, d), wf_nwf)
      end do
      return
    end subroutine wfint_h2wk

!--------------------------------------------------------------------------------------

    ! transform matrix at q from Wannier to Hamiltonian gauge
    subroutine wfint_w2hq( mwq, mhq, iq, ld, iq2)
      complex(8), intent( in)        :: mwq( wf_nwf, wf_nwf, ld)
      complex(8), intent( out)       :: mhq( wf_nwf, wf_nwf, ld)
      integer, intent( in)           :: iq, ld
      integer, optional, intent( in) :: iq2

      integer :: d, ip
      complex(8) :: auxmat( wf_nwf, wf_nwf, ld)

      ip = iq
      if( present( iq2)) ip = iq2

      call zgemm( 'c', 'n', wf_nwf, wf_nwf*ld, wf_nwf, zone, &
             wfint_transform( :, :, iq), wf_nwf, &
             mwq, wf_nwf, zzero, &
             auxmat, wf_nwf)
      do d = 1, ld
        call zgemm( 'n', 'n', wf_nwf, wf_nwf, wf_nwf, zone, &
               auxmat( :, :, d), wf_nwf, &
               wfint_transform( :, :, ip), wf_nwf, zzero, &
               mhq( :, :, d), wf_nwf)
      end do
      return
    end subroutine wfint_w2hq

!--------------------------------------------------------------------------------------

    ! get weights needed for minimal distance mode interpolation
    subroutine wfint_getmindistwgts( ir, iq, wgts)
      integer, intent( in)     :: ir, iq
      complex(8), intent( out) :: wgts( wf_nwf, wf_nwf)

      integer :: ist, jst, m
      real(8) :: dotp

      wgts = zzero
      do ist = 1, wf_nwf
        do jst = 1, wf_nwf
          do m = 1, wf_wdistmul( ist, jst, ir)
            dotp = twopi*dot_product( wfint_kset%vkl( :, iq), dble( wf_wdistvec( :, m, ist, jst, ir)))
            dotp = dotp + dot_product( wfint_kset%vkc( :, iq), wf_centers( :, jst) - wf_centers( :, ist))
            wgts( ist, jst) = wgts( ist, jst) + cmplx( cos( dotp), sin( dotp), 8)
          end do
          wgts( ist, jst) = wgts( ist, jst)/wf_rmul( ir)/wf_wdistmul( ist, jst, ir)
        end do
      end do
      return
    end subroutine wfint_getmindistwgts
    
!--------------------------------------------------------------------------------------
! INTERPOLATION FUNCTIONS FOR VARIOUS QUANTITIES
!--------------------------------------------------------------------------------------

    !BOP
    ! !ROUTINE: wfint_interpolate_eigsys
    ! !INTERFACE:
    !
    subroutine wfint_interpolate_eigsys( evalin, serial)
      ! !USES:
      ! !INPUT PARAMETERS:
      !   evalin : eigenenergies on original grid (in, real( wf_fst:wf_lst, wf_kset%nkpt))
      ! !DESCRIPTION:
      !   Calculates the interpolated eigenenergies as well as the corresponding expansion 
      !   coefficients and phasefactors for the interpolated wavefunctions.
      !
      ! !REVISION HISTORY:
      !   Created July 2017 (SeTi)
      !EOP
      !BOC

      real(8), intent( in)           :: evalin( wf_fst:wf_lst, wf_kset%nkpt)
      logical, optional, intent( in) :: serial
      
      integer :: iq, q1, q2
      real(8) :: t1, t0, ts, te, thwr, thwq, thbq
      logical :: parallel

      complex(8), allocatable :: hwq(:,:)

      parallel = .true.
      if( present( serial)) parallel = .not. serial

      thwr = 0.d0
      thwq = 0.d0
      thbq = 0.d0
      call timesec( t0)

      !**********************************************
      ! interpolated eigenenergies and corresponding 
      ! eigenvectors in Wannier basis
      !**********************************************
    
      ! get R-dependent Hamiltonian in Wannier gauge
      call timesec( ts)
      if( .not. allocated( wfint_hwr)) call wfint_genhwr( evalin)
      call timesec( te)
      thwr = te - ts

      allocate( hwq( wf_nwf, wf_nwf))
      if( parallel) then
        q1 = firstofset( mpiglobal%rank, wfint_kset%nkpt)
        q2 = lastofset( mpiglobal%rank, wfint_kset%nkpt)
      else
        q1 = 1
        q2 = wfint_kset%nkpt
      end if
#ifdef USEOMP
!$omp parallel default( shared) private( iq, hwq)
!$omp do
#endif
      do iq = q1, q2
        ! transform it to the q-dependent Hamiltonian in Wannier gauge
        call timesec( ts)
        call wfint_ftr2q( wfint_hwr, hwq, iq, 1)
        call timesec( te)
#ifdef USEOMP
!$omp atomic update
#endif
        thwq = thwq + te - ts
#ifdef USEOMP
!$omp end atomic
#endif
        ! find the matrices that transform it to the Hamilton gauge
        call timesec( ts)
        call zhediag( hwq, wfint_eval( :, iq), evec=wfint_transform( :, :, iq))
        call timesec( te)
#ifdef USEOMP
!$omp atomic update
#endif
        thbq = thbq + te - ts
#ifdef USEOMP
!$omp end atomic
#endif
      end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
      if( parallel) then
        call mpi_allgatherv_ifc( set=wfint_kset%nkpt, rlen=wf_nwf, rbuf=wfint_eval)
        call mpi_allgatherv_ifc( set=wfint_kset%nkpt, rlen=wf_nwf*wf_nwf, zbuf=wfint_transform)
        call barrier
      end if
      call timesec( t1)
      deallocate( hwq)

      if( mpiglobal%rank .eq. -1) then
        write(*,'("   INTERPOLATION TIMING  ")')
        write(*,'("==========================")')
        write(*,'("build HW_R : ",f13.6)') thwr
        write(*,'("build HW_q : ",f13.6)') thwq
        write(*,'("diag. HW_q : ",f13.6)') thbq
        write(*,'("--------------------------")')
        write(*,'("sum        : ",f13.6)') thwr + thwq + thbq
        write(*,'("rest       : ",f13.6)') t1 - t0 - (thwr + thwq + thbq)
        write(*,'("--------------------------")')
        write(*,'("total      : ",f13.6)') t1 - t0
        write(*,'("==========================")')
        write(*,*)
      end if
    
      return
    end subroutine wfint_interpolate_eigsys
    !EOC

!--------------------------------------------------------------------------------------
    
    !BOP
    ! !ROUTINE: wfint_interpolate_occupancy
    ! !INTERFACE:
    !
    subroutine wfint_interpolate_occupancy( usetetra)
      ! !USES:
      use mod_opt_tetra
      ! !INPUT PARAMETERS:
      !   usetetra : to use tetrahedron integration or not (in, optional, logical)
      ! !DESCRIPTION:
      !   Calclulates the interpolated occupation numbers for the wannierized bands and
      !   interpolated Fermi energy.
      !
      ! !REVISION HISTORY:
      !   Created July 2017 (SeTi)
      !EOP
      logical, optional, intent( in) :: usetetra
      !BOC
      integer :: fst, lst
      logical :: usetetra_ = .false.
      real(8) :: eval( wf_fst:(wf_fst+wf_nwf-1), wfint_kset%nkpt)
      real(8) :: occ( wf_fst:(wf_fst+wf_nwf-1), wfint_kset%nkpt)

      eval = wfint_eval

      if( present( usetetra)) usetetra_ = usetetra
      if( allocated( wfint_occ)) deallocate( wfint_occ)
      allocate( wfint_occ( wf_nwf, wfint_kset%nkpt))

      fst = wf_fst
      lst = wf_fst + wf_nwf - 1
      if( allocated( wfint_bandmap)) then
        fst = wfint_bandmap(1)
        lst = wfint_bandmap( wf_nwf)
      end if

      if( usetetra_) then
        call wfhelp_init_tetra( wfint_tetra, wfint_kset, reduce=.true.)
        call wfhelp_occupy( wfint_kset, eval, fst, lst, wfint_efermi, occ, wfint_tetra)
      else
        call wfhelp_occupy( wfint_kset, eval, fst, lst, wfint_efermi, occ)
      end if

      wfint_occ = occ
      return
    end subroutine wfint_interpolate_occupancy
    !EOC

!--------------------------------------------------------------------------------------

    !BOP
    ! !ROUTINE: wfint_interpolate_ederiv
    ! !INTERFACE:
    !
    subroutine wfint_interpolate_ederiv( velo_, mass_)
      ! !INPUT PARAMETERS:
      !   velo : group-velocity vector (out, real( 3, wf_nwf, wfint_kset%nkpt))
      !   mass : effective-mass tensor (out, real( 3, 3, wf_nwf, wfint_kset%nkpt))
      ! !DESCRIPTION:
      ! Calculates the group-velocity vector (first derivative) and 
      ! effective-mass tensors (inverse of second derivative) for the Wannier-interpolated bands.
      !
      ! !REVISION HISTORY:
      !   Created July 2017 (SeTi)
      !EOP
      real(8), intent( out) :: velo_( 3, wf_nwf, wfint_kset%nkpt)
      real(8), intent( out) :: mass_( 3, 3, wf_nwf, wfint_kset%nkpt)
      !BOC

      integer :: ik, iq, ir, ist, jst, im, igroup, d1, d2, ndeg, sdeg, ddeg
      real(8) :: dotp, eps1, eps2, vr(3)
      complex(8) :: ftweight, hamwk( wf_nwf, wf_nwf)
      complex(8) :: velo( wf_nwf, wf_nwf, 3, wfint_kset%nkpt)
      complex(8) :: dmat( wf_nwf, wf_nwf, 3, wfint_kset%nkpt)
      complex(8) :: mass( wf_nwf, wf_nwf, 3, 3, wfint_kset%nkpt)
      real(8) :: degeval( wf_nwf)
      complex(8) :: degmat( wf_nwf, wf_nwf), degevec( wf_nwf, wf_nwf)
      
      real(8), allocatable :: evalin(:,:)
      complex(8), allocatable :: auxmat(:,:)

      eps1 = 1.d-4
      eps2 = 1.d-2
      ddeg = 0

      velo_ = 0.d0
      mass_ = 0.d0

      call wfhelp_geteval( evalin, ist, jst)
      if( .not. allocated( wfint_hwr)) call wfint_genhwr( evalin( wf_fst:wf_lst, :))

      ! get R-dependent Hamiltonian in Wannier gauge
      allocate( auxmat( wf_nwf, wf_nwf))

      velo = zzero
      mass = zzero
      dmat = zzero

      ! get first band-derivative (velocity)
      do d1 = 1, 3
        do iq = 1, wfint_kset%nkpt

          hamwk = zzero
          do ir = 1, wf_nrpt
            call r3mv( input%structure%crystal%basevect, dble( wf_rvec( :, ir)), vr)
            if( wfint_mindist) then
              do ist = 1, wf_nwf
                do jst = 1, wf_nwf
                  ftweight = zzero
                  do im = 1, wf_wdistmul( ist, jst, ir)
                    call r3mv( input%structure%crystal%basevect, dble( wf_wdistvec( :, im, ist, jst, ir)), vr)
                    dotp = dot_product( wfint_kset%vkc( :, iq), vr + wf_centers( :, jst) - wf_centers( :, ist))
                    ftweight = ftweight + cmplx( cos( dotp), sin( dotp), 8)
                  end do
                  hamwk( ist, jst) = hamwk( ist, jst) + zi*vr( d1)*ftweight/wf_rmul( ir)/wf_wdistmul( ist, jst, ir)*wfint_hwr( ist, jst, ir)
                end do
              end do
            else
              hamwk = hamwk + zi*vr( d1)*wfint_pqr( iq, ir)*wfint_hwr( :, :, ir)
            end if
          end do
          ! force hermiticity (not guaranteed if wfint_mindist)
          hamwk = cmplx( 0.5d0, 0.d0, 8)*(hamwk + conjg( transpose( hamwk)))

          call wfint_w2hq( hamwk, velo( :, :, d1, iq), iq, 1)
          ! handle degeneracies of first order
          sdeg = 1
          do while( (sdeg .lt. wf_nwf) .and. (d1 .eq. ddeg))
            ndeg = 1
            do while( (sdeg .lt. wf_nwf))
              if( abs( wfint_eval( sdeg, iq) - wfint_eval( sdeg+1, iq)) .ge. eps1) exit
              ndeg = ndeg + 1
              sdeg = sdeg + 1
            end do
            if( ndeg .gt. 1) then
              degmat( 1:ndeg, 1:ndeg) = velo( (sdeg-ndeg+1):sdeg, (sdeg-ndeg+1):sdeg, d1, iq)
              call zhediag( degmat( 1:ndeg, 1:ndeg), degeval( 1:ndeg), evec=degevec( 1:ndeg, 1:ndeg))
              call zgemm( 'n', 'n', wf_nwf, ndeg, ndeg, zone, &
                     wfint_transform( :, (sdeg-ndeg+1):sdeg, iq), wf_nwf, &
                     degevec, wf_nwf, zzero, &
                     auxmat( :, 1:ndeg), wf_nwf)
              wfint_transform( :, (sdeg-ndeg+1):sdeg, iq) = auxmat( :, 1:ndeg)
              call wfint_w2hq( hamwk, velo( :, :, d1, iq), iq, 1)
            else
              sdeg = sdeg + 1
            end if
          end do

          ! set up D-matrix
          do ist = 1, wf_nwf
            do jst = 1, wf_nwf
              if( abs( wfint_eval( ist, iq) - wfint_eval( jst, iq)) .gt. eps1) then
                dmat( ist, jst, d1, iq) = velo( ist, jst, d1, iq)/(wfint_eval( jst, iq) - wfint_eval( ist, iq))
              end if
            end do
          end do

        end do
      end do

      do d1 = 1, 3
        ! get second band-derivative (inverse effective mass)
        do iq = 1, wfint_kset%nkpt
          do d2 = 1, 3
            hamwk = zzero
            do ir = 1, wf_nrpt
              if( wfint_mindist) then
                do ist = 1, wf_nwf
                  do jst = 1, wf_nwf
                    ftweight = zzero
                    do im = 1, wf_wdistmul( ist, jst, ir)
                      call r3mv( input%structure%crystal%basevect, dble( wf_wdistvec( :, im, ist, jst, ir)), vr)
                      dotp = dot_product( wfint_kset%vkc( :, iq), vr + wf_centers( :, jst) - wf_centers( :, ist))
                      ftweight = ftweight + cmplx( cos( dotp), sin( dotp), 8)
                    end do
                    hamwk( ist, jst) = hamwk( ist, jst) - vr( d1)*vr( d2)*ftweight/wf_rmul( ir)/wf_wdistmul( ist, jst, ir)*wfint_hwr( ist, jst, ir)
                  end do
                end do
              else
                call r3mv( input%structure%crystal%basevect, dble( wf_rvec( :, ir)), vr)
                hamwk = hamwk - vr( d1)*vr( d2)*wfint_pqr( iq, ir)*wfint_hwr( :, :, ir)
              end if
            end do
            ! force hermiticity (not guaranteed if wfint_mindist)
            hamwk = cmplx( 0.5d0, 0.d0, 8)*(hamwk + conjg( transpose( hamwk)))
            call wfint_w2hq( hamwk, mass( :, :, d1, d2, iq), iq, 1)
            call zgemm( 'n', 'n', wf_nwf, wf_nwf, wf_nwf, zone, &
                   velo( :, :, d1, iq), wf_nwf, &
                   dmat( :, :, d2, iq), wf_nwf, zzero, &
                   auxmat, wf_nwf)
            mass( :, :, d1, d2, iq) = mass( :, :, d1, d2, iq) + auxmat + conjg( transpose( auxmat))
            ! handle degeneracies of second order
            sdeg = 1
            do while( (sdeg .lt. wf_nwf) .and. (d1 .eq. ddeg) .and. (d2 .eq. ddeg))
              ndeg = 1
              do while( (sdeg .lt. wf_nwf))
                if( (abs( velo( sdeg, sdeg, d1, iq) - velo( sdeg+1, sdeg+1, d1, iq)) .ge. eps2) .and. &
                    (abs( wfint_eval( sdeg, iq) - wfint_eval( sdeg+1, iq)) .ge. eps1)) exit
                ndeg = ndeg + 1
                sdeg = sdeg + 1
              end do
              if( ndeg .gt. 1) then
                degmat( 1:ndeg, 1:ndeg) = mass( (sdeg-ndeg+1):sdeg, (sdeg-ndeg+1):sdeg, d1, d2, iq)
                call zhediag( degmat( 1:ndeg, 1:ndeg), degeval( 1:ndeg), evec=degevec( 1:ndeg, 1:ndeg))
                do ist = 1, ndeg
                  mass( sdeg-ndeg+ist, sdeg-ndeg+ist, d1, d2, iq) = cmplx( degeval( ist), 0, 8)
                end do
              else
                sdeg = sdeg + 1
              end if
            end do
          end do
        end do
      end do

      velo_ = 0.d0
      mass_ = 0.d0
      do iq = 1, wfint_kset%nkpt
        do ist = 1, wf_nwf
          velo_( :, ist, iq) = dble( velo( ist, ist, :, iq))
          mass_( :, :, ist, iq) = dble( mass( ist, ist, :, :, iq))
        end do
      end do

      deallocate( evalin, auxmat)

      return
    end subroutine wfint_interpolate_ederiv
    !EOC

!--------------------------------------------------------------------------------------

    subroutine wfint_interpolate_pmat( pmat)
      use mod_wannier_filehandling, only: wffile_reademat
      use mod_wannier_omega,        only: wfomega_m
      use m_getunit
      complex(8), intent( out) :: pmat( wf_nwf, wf_nwf, 3, wfint_kset%nkpt)

      integer :: ik, ir, iq, idxn, i, j, ik2
      integer :: isym, ist, jst, un, recl
      real(8) :: v1(3), v2(3), sc(3,3), dq
      logical :: success
      type( k_set) :: kset

      complex(8), allocatable :: hwr(:,:,:), dhwq(:,:,:), awk(:,:,:,:), awr(:,:,:,:), awq(:,:,:)
      complex(8), allocatable :: rwgt(:,:), auxmat(:,:), auxmat2(:,:,:)
      complex(8), allocatable :: pmat0(:,:,:), pmatkh(:,:,:)
      real(8), allocatable :: eval(:,:)

      pmat = zzero

      ! get Wannier Berry connection on coarse grid
      call wffile_reademat( success)
      if( .not. success) then
        if( mpiglobal%rank .eq. 0) then
          write(*,*)
          write(*,'("Error (wfint_interpolate_pmat): Wannier plane-wave matrix-elements could not be read from file.")')
        end if
        stop
      end if
      !do wf_group = 1, wf_ngroups
      !  call wfomega_m_diag
      !end do
      call wfomega_m
      allocate( awk( wf_nwf, wf_nwf, 3, wf_kset%nkpt))
      allocate( auxmat (wf_nwf, wf_nwf))
      awk = zzero
      do ik = 1, wf_kset%nkpt
        do idxn = 1, wf_n_ntot
          auxmat = wf_m( :, :, ik, idxn) - conjg( transpose( wf_m( :, :, wf_n_ik2( idxn, ik), idxn)))
          auxmat = zi*auxmat*wf_n_wgt( idxn)
          do i = 1, 3
            awk( :, :, i, ik) = awk( :, :, i, ik) + wf_n_vc( i, idxn)*auxmat
          end do
        end do
      end do

      ! Fourier transform Wannier Berry connection to real space
      allocate( awr( wf_nwf, wf_nwf, 3, wf_nrpt))
      call wfint_ftk2r( awk, awr, 3)
      deallocate( awk, auxmat)

      ! do the interpolation
      allocate( rwgt( wf_nrpt, 3), auxmat( 3, wf_nrpt))
      allocate( dhwq( wf_nwf, wf_nwf, 3), awq( wf_nwf, wf_nwf, 3))
      auxmat = cmplx( dble( wf_rvec), 0.d0, 8)
      call zgemm( 't', 't', wf_nrpt, 3, 3, zi, &
             auxmat, 3, &
             cmplx( input%structure%crystal%basevect, 0.d0, 8), 3, zzero, &
             rwgt, wf_nrpt)
      deallocate( auxmat)
      allocate( auxmat2( wf_nwf, wf_nwf, 3))
      write(*,*) wf_rvec(:,1)
      do i = 1, wf_nwf
        call r3mv( ainv, wf_centers(:,i), v1)
        call r3mv( ainv, dble( awr(i,i,:,1)), v2)
        write(*,'(6f13.6)') v1, v2
      end do
      do iq = 1, wfint_kset%nkpt
        ! Fourier transform to fine grid
        do i = 1, 3
          call wfint_ftr2q( wfint_hwr, dhwq(:,:,i), iq, 1, rwgt=rwgt(:,i))
        end do
        call wfint_ftr2q( awr, awq, iq, 3)
        ! transform from Wannier to Hamiltonian gauge
        call wfint_w2hq( dhwq, auxmat2, iq, 3)
        pmat(:,:,:,iq) = pmat(:,:,:,iq) + auxmat2
        call wfint_w2hq( awq, auxmat2, iq, 3)
        do i = 1, 3
          auxmat2(:,:,i) = 0.5d0*(auxmat2(:,:,i) + conjg( transpose( auxmat2(:,:,i))))
        end do
        do j = 1, wf_nwf
          do i = 1, wf_nwf
            pmat(i,j,:,iq) = pmat(i,j,:,iq) - zi*(wfint_eval( j, iq) - wfint_eval( i, iq))*auxmat2( i, j, :)
          end do
        end do
      end do
      deallocate( auxmat2, rwgt, awr, awq, dhwq)
      
      allocate( pmat0( 3, nstsv, nstsv), pmatkh( wf_fst:wf_lst, wf_fst:wf_lst, 3))
      allocate( eval( wf_nwf, wfint_kset%nkpt), auxmat2( wf_nwf, wf_nwf, wfint_kset%nkpt))

      eval = wfint_eval
      auxmat2 = wfint_transform
      call r3minv( wfint_kset%bvec, sc)
      kset = wfint_kset
      dq = 1.d-4
      do iq = 1, wfint_kset%nkpt
        kset%vkc(1,iq) = kset%vkc(1,iq) + dq
        call r3mv( sc, kset%vkc(:,iq), kset%vkl(:,iq))
      end do
      call wfint_init( kset)
      
      inquire( file='PMAT.OUT', exist=success)
      if( .not. success) call writepmat
      
      inquire( iolength=recl) pmat0
      call getunit( un)
      open( un, file='PMAT.OUT', action='read', form='unformatted', access='direct', recl=recl)
      do ik = 1, wf_kset%nkpt
        call findkpt( wf_kset%vkl( :, ik), isym, iq)
        read( un, rec=iq) pmat0
        sc = symlatc( :, :, lsplsymc( isym))
        do ist = wf_fst, wf_lst
          do jst = wf_fst, wf_lst
            if( isym .gt. 1) then
              call r3mv( sc, dble( pmat0( :, ist, jst)), v1)
              call r3mv( sc, aimag( pmat0( :, ist, jst)), v2)
              pmatkh( ist, jst, :) = cmplx( v1, v2, 8)
            else
              pmatkh( ist, jst, :) = pmat0( :, ist, jst)
            end if
          end do
        end do

        !call zgemm( 'c', 'n', wf_nwf, wf_nwf, wf_nwf, cmplx( 1.d0/dq, 0.d0, 8), &
        !       auxmat2(:,:,ik), wf_nwf, &
        !       wfint_transform(:,:,ik), wf_nwf, zzero, &
        !       pmat(:,:,1,ik), wf_nwf)
        !do i = 1, wf_nwf
        !  do j = 1, wf_nwf
        !    pmat(i,j,1,ik) = (eval(j,ik) - eval(i,ik))*pmat(i,j,1,ik)
        !  end do
        !end do

        write(*,'(i,3f13.6)') ik, wf_kset%vkl(:,ik)
        call plotmat( zone*abs(pmatkh(:,:,1)))
        write(*,*)
        call plotmat( zone*abs(pmat(:,:,1,ik)))
        write(*,*)
      end do
      close( un)
      deallocate( eval, auxmat2)
      deallocate( pmat0, pmatkh)

      return
    end subroutine wfint_interpolate_pmat

!--------------------------------------------------------------------------------------

    !BOP
    ! !ROUTINE: wfint_interpolate_bandchar
    ! !INTERFACE:
    !
    subroutine wfint_interpolate_bandchar( lmax, bc)
      ! !INPUT PARAMETERS:
      !   lmax : maximum angular quantum number (in, integer)
      !   bc   : band character (out, real( 0:lmax, natmtot, wf_nwf, wfint_kset%nkpt))
      ! !DESCRIPTION:
      ! Calculates the band character for the Wannier-interpolated bands.
      !
      ! !REVISION HISTORY:
      !   Created July 2017 (SeTi)
      !EOP
      integer, intent( in) :: lmax
      real(8), intent( out) :: bc( 0:lmax, natmtot, wf_nwf, wfint_kset%nkpt)
      !BOC

      integer :: iq, l, m, lm, lmmax, lammax, ist, ias
      real(8), allocatable :: radolp(:,:,:,:), elm(:,:)
      complex(8), allocatable :: dmat(:,:,:,:), radcoeffr(:,:,:,:,:), ulm(:,:,:), auxmat(:,:)

      lmmax = (lmax + 1)**2

      call wfint_gen_radcoeffr( lmax, lammax, radcoeffr)
      call wfint_gen_radolp( lmax, lammax, radolp)

      allocate( elm( lmmax, natmtot))
      allocate( ulm( lmmax, lmmax, natmtot))
      call genlmirep( lmax, lmmax, elm, ulm)

      bc = 0.d0
#ifdef USEOMP
!$omp parallel default( shared) private( iq, dmat, auxmat, l, m, lm, ist, ias)
#endif
      allocate( dmat( lmmax, lmmax, wf_nwf, natmtot))
      allocate( auxmat( lmmax, lmmax))
#ifdef USEOMP
!$omp do
#endif
      do iq = firstofset( mpiglobal%rank, wfint_kset%nkpt), lastofset( mpiglobal%rank, wfint_kset%nkpt)
        call wfint_interpolate_dmat( lmax, lammax, iq, radcoeffr, radolp, dmat, diagonly=.false.)
        do ias = 1, natmtot
          do ist = 1, wf_nwf
            call zgemm( 'n', 'n', lmmax, lmmax, lmmax, zone, &
                   ulm( :, :, ias), lmmax, &
                   dmat( :, :, ist, ias), lmmax, zzero, &
                   auxmat, lmmax)
            call zgemm( 'n', 'c', lmmax, lmmax, lmmax, zone, &
                   auxmat, lmmax, &
                   ulm( :, :, ias), lmmax, zzero, &
                   dmat( :, :, ist, ias), lmmax)
            do l = 0, lmax
              do m = -l, l
                lm = idxlm( l, m)
                bc( l, ias, ist, iq) = bc( l, ias, ist, iq) + dble( dmat( lm, lm, ist, ias))
              end do
            end do
          end do
        end do
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( dmat, auxmat)
#ifdef USEOMP
!$omp end parallel
#endif
      call mpi_allgatherv_ifc( set=wfint_kset%nkpt, rlen=(lmax+1)*natmtot*wf_nwf, rbuf=bc)
      call barrier

      if( allocated( radcoeffr)) deallocate( radcoeffr)
      if( allocated( radolp)) deallocate( radolp)

      return
      
    end subroutine wfint_interpolate_bandchar
    !EOC

!--------------------------------------------------------------------------------------

    !BOP
    ! !ROUTINE: wfint_interpolate_dos
    ! !INTERFACE:
    !
    subroutine wfint_interpolate_dos( lmax, nsmooth, intgrid, neffk, nsube, ewin, tdos, scissor, tetra, pdos, lonly, jdos, ntrans)
      ! !USES
      use mod_eigenvalue_occupancy, only: occmax
      use mod_my_tetra
      ! !INPUT PARAMETERS:
      !   lmax    : maximum angular quantum number (in, integer)
      !   nsmooth : smoothing of output result (in, integer)
      !   intgrid : interpolation/integration grid (in, integer(3))
      !   neffk   : effective number of k-points in one direction (in, integer)
      !   nsube   : number of energy subdivisions in energy window (in, integer)
      !   ewin    : energy window (in, real(2))
      !   tdos    : total DOS (out, real( nsube))
      !   scissor : energy scissor to apply (in, optional, real)
      !   tetra   : use tetrahedron integration (in, optional, logical)
      !   pdos    : partial DOS (out, optional, real( nsube, (lmax+1)**2, natmtot))
      !   lonly   : partial DOS only l-resolved (in, optional, logical)
      !   jdos    : joint DOS (out, optional, real( nsube, 0:wf_nwf))
      !   ntrans  : number of transistion in JDOS (out, optional, integer)
      ! !DESCRIPTION:
      ! Calculates the total density of states (DOS) and optionally also the partial or joint DOS.
      !
      ! !REVISION HISTORY:
      !   Created July 2017 (SeTi)
      !EOP
      integer, intent( in) :: lmax, nsmooth, intgrid(3), neffk, nsube
      real(8), intent( in) :: ewin(2)
      real(8), intent( out) :: tdos( nsube)
      ! optional arguments
      real(8), optional, intent( in) :: scissor
      logical, optional, intent( in) :: tetra, lonly
      real(8), optional, intent( out) :: pdos( nsube, (lmax+1)**2, natmtot)
      real(8), optional, intent( out) :: jdos( nsube, 0:wf_nwf)
      integer, optional, intent( out) :: ntrans
      !BOC

      integer :: lmmax, ias, l, m, lm, ist, jst, iq, q1, q2, ie, nk(3), lammax, n, mtrans
      integer :: ie1, ie2, ne, ite, stat
      real(8) :: dosscissor, tmpfermi, t0, t1
      type( k_set) :: tmp_kset
      logical :: genpdos, genjdos, usetetra, pdoslonly
      type( t_set) :: tset

      real(8), allocatable :: energies(:,:), radolp(:,:,:,:), elm(:,:), e(:), ftdos(:,:,:), fjdos(:,:), fpdos(:,:,:,:), edif(:,:,:), ejdos(:,:)
      complex(8), allocatable :: ulm(:,:,:), radcoeffr(:,:,:,:,:), dmat(:,:,:,:), auxmat(:,:), cjdos(:,:,:)

      dosscissor = 0.d0
      if( present( scissor)) dosscissor = scissor

      genpdos = .false.
      if( present( pdos)) genpdos = .true.

      genjdos = .false.
      if( present( jdos)) genjdos = .true.

      usetetra = .true.
      if( present( tetra)) usetetra = tetra

      pdoslonly = .true.
      if( present( lonly)) pdoslonly = lonly

      lmmax = (lmax+1)**2

      call wfint_findbandmap

      call generate_k_vectors( tmp_kset, bvec, intgrid, wf_kset%vkloff, .true., uselibzint=.false.)
      nk(:) = max( neffk/tmp_kset%ngridk, 1)

      allocate( e( nsube))
      do ie = 1, nsube
        e( ie) = ewin(1) + dble( ie-1)*(ewin(2)-ewin(1))/(nsube-1)
      end do

      call wfint_init( tmp_kset)

      call wfint_interpolate_occupancy( usetetra=usetetra)
      
      allocate( energies( wf_nwf, wfint_kset%nkpt))
      energies = wfint_eval
      tmpfermi = wfint_efermi
      if( wf_fermizero) tmpfermi = 0.d0
      if( wf_fermizero) energies = energies - wfint_efermi

      ! apply scissor
      do iq = 1, wfint_kset%nkpt
        do ist = 1, wf_nwf
          if( energies( ist, iq) .gt. tmpfermi) energies( ist, iq) = energies( ist, iq) + dosscissor
        end do
      end do

      if( usetetra) call wfhelp_init_tetra( wfint_tetra, wfint_kset, reduce=.true.)
      !--------------------------------------!
      !              total DOS               !
      !--------------------------------------!
      allocate( ftdos( wf_nwf, wfint_kset%nkpt, nsube))
      ftdos = 1.d0
      if( usetetra) then
        call opt_tetra_int( wfint_tetra, 2, wfint_kset%nkpt, wf_nwf, energies, e, nsube, ftdos)
        tdos = 0.d0
#ifdef USEOMP
!$omp parallel default( shared) private( ie)
!$omp do
#endif
        do ie = 1, nsube
          tdos( ie) = sum( ftdos( :, :, ie))
        end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
      else
        call brzint( nsmooth, wfint_kset%ngridk, nk, wfint_kset%ikmap, nsube, ewin, wf_nwf, wf_nwf, &
             energies, &
             ftdos(:,:,1), &
             tdos)
      end if

      !--------------------------------------!
      !             partial DOS              !
      !--------------------------------------!
      if( genpdos) then
        allocate( elm( lmmax, natmtot))
        allocate( ulm( lmmax, lmmax, natmtot))
        call genlmirep( lmax, lmmax, elm, ulm)
        deallocate( elm)
        allocate( fpdos( wf_nwf, lmmax, natmtot, wfint_kset%nkpt))
        fpdos(:,:,:,:) = 0.d0
        call wfint_gen_radcoeffr( lmax, lammax, radcoeffr)
        call wfint_gen_radolp( lmax, lammax, radolp)

        q1 = firstofset( mpiglobal%rank, wfint_kset%nkpt)
        q2 = lastofset( mpiglobal%rank, wfint_kset%nkpt)
        allocate( dmat( lmmax, lmmax, wf_nwf, natmtot))
        allocate( auxmat( lmmax, lmmax))
        do iq = q1, q2
          call wfint_interpolate_dmat( lmax, lammax, iq, radcoeffr, radolp, dmat)
          do ias = 1, natmtot
            do ist = 1, wf_nwf
              call zgemm( 'n', 'n', lmmax, lmmax, lmmax, zone, &
                     ulm( :, :, ias), lmmax, &
                     dmat( :, :, ist, ias), lmmax, zzero, &
                     auxmat, lmmax)
              call zgemm( 'n', 'c', lmmax, lmmax, lmmax, zone, &
                     auxmat, lmmax, &
                     ulm( :, :, ias), lmmax, zzero, &
                     dmat( :, :, ist, ias), lmmax)
              do l = 0, lmax
                do m = -l, l
                  lm = idxlm( l, m)
                  if( pdoslonly) then
                    fpdos( ist, l+1, ias, iq) = fpdos( ist, l+1, ias, iq) + dble( dmat( lm, lm, ist, ias))
                  else
                    fpdos( ist, lm, ias, iq) = dble( dmat( lm, lm, ist, ias))
                  end if
                end do
              end do
            end do
          end do
        end do
        deallocate( dmat, auxmat, ulm)
        call mpi_allgatherv_ifc( set=wfint_kset%nkpt, rlen=lmmax*natmtot*wf_nwf, rbuf=fpdos)
        call barrier

        do ias = 1, natmtot
          do l = 0, lmax
            if( pdoslonly) then
              if( usetetra) then
#ifdef USEOMP
!$omp parallel default( shared) private( ie)
!$omp do
#endif
                do ie = 1, nsube
                  pdos( ie, l+1, ias) = sum( fpdos( :, l+1, ias, :)*ftdos( :, :, ie))
                end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
              else
                call brzint( nsmooth, wfint_kset%ngridk, nk, wfint_kset%ikmap, nsube, ewin, wf_nwf, wf_nwf, &
                       energies, &
                       fpdos( :, l+1, ias, :), &
                       pdos( :, l+1, ias))
              end if
            else
              do m = -l, l
                lm = idxlm( l, m)
                if( usetetra) then
#ifdef USEOMP
!$omp parallel default( shared) private( ie)
!$omp do
#endif
                  do ie = 1, nsube
                    pdos( ie, lm, ias) = sum( fpdos( :, lm, ias, :)*ftdos( :, :, ie))
                  end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
                else
                  call brzint( nsmooth, wfint_kset%ngridk, nk, wfint_kset%ikmap, nsube, ewin, wf_nwf, wf_nwf, &
                         energies, &
                         fpdos( :, lm, ias, :), &
                         pdos( :, lm, ias))
                end if
              end do
            end if
          end do
        end do

        deallocate( fpdos)
      end if
      deallocate( ftdos)

      !--------------------------------------!
      !              joint DOS               !
      !--------------------------------------!
      if( genjdos) then
        allocate( edif( wf_nwf, wfint_kset%nkpt, wf_nwf))
        edif = -1.d100
        mtrans = 0
        ntrans = 0
        l = 0
        do iq = 1, wfint_kset%nkpt
          n = 0
          do ist = wf_nwf, 1, -1
            if( wfint_occ( ist, iq) .gt. 1.d-10) l = max( l, ist)
            if( energies( ist, iq) .le. tmpfermi) then
              n = n + 1
              m = 0
              do jst = 1, wf_nwf
                if( energies( jst, iq) .gt. tmpfermi) then
                  m = m + 1
                  edif( m, iq, n) = energies( jst, iq) - energies( ist, iq)
                end if
              end do
              mtrans = max( mtrans, m)
            end if
          end do
          ntrans = max( ntrans, n)
        end do
        ! state dependent JDOS
        if( usetetra) then
          do l = 1, wf_nwf      ! lowest (partially) unoccupied band
            if( occmax-minval( wfint_occ(l,:)) .gt. 1.d-10) exit
          end do
          do m = wf_nwf, 1, -1  ! highest (partially) occupied band
            if( maxval( wfint_occ(m,:)) .gt. 1.d-10) exit
          end do
            
          allocate( cjdos( nsube, l:wf_nwf, m))
          allocate( ulm( l:wf_nwf, m, wfint_kset%nkpt))
          do iq = 1, wfint_kset%nkpt
            do jst = 1, m
              do ist = l, wf_nwf
                ulm( ist, jst, iq) = cmplx( min( wfint_occ( jst, iq), occmax-wfint_occ( ist, iq)), 0.d0, 8)
              end do
            end do
          end do

          call my_tetra_init( tset, wfint_kset, 2, reduce=.true.)
          call timesec( t0)
          call my_tetra_int_deltadiff( tset, wfint_kset%nkpt, wf_nwf-l+1, energies( l:wf_nwf, :), m, energies( 1:m, :), &
                 nsube, e, 1, resc=cjdos, matc=ulm)
          call timesec( t1)

          jdos = 0.d0
          do jst = 1, m
            jdos(:,jst) = sum( dble( cjdos(:,:,jst)), 2)
          end do
          jdos(:,0) = sum( jdos(:,1:m), 2)
                 
          deallocate( cjdos, ulm)

!            call timesec( t0)
!            call opt_tetra_int_ediff( wfint_tetra, 2, wfint_kset%nkpt, wf_nwf, energies, e( ie1:ie2), ie2-ie1+1, fpdos, brange=(/1, l, 0, wf_nwf/))
!            call timesec( t1)
!            write(*,'(i,f13.6)') m, t1-t0
!
!            do ist = 1, wf_nwf
!              do jst = ist + 1, wf_nwf
!                do iq = 1, wfint_kset%nkpt
!                  jdos( ie1:ie2, ist) = jdos( ie1:ie2, ist) + min( wfint_occ( ist, iq), occmax-wfint_occ( jst, iq))*fpdos( ist, jst, iq, :)/occmax
!                end do
!              end do
!              jdos( ie1:ie2 ,0) = jdos( ie1:ie2, 0) + jdos( ie1:ie2, ist)
!            end do
        else
          allocate( fjdos( mtrans*ntrans, wfint_kset%nkpt))
          allocate( ejdos( mtrans*ntrans, wfint_kset%nkpt))
          fjdos(:,:) = 1.d0
          ejdos(:,:) = -1.d100
#ifdef USEOMP
!$omp parallel default( shared) private( ist)
!$omp do
#endif
          do ist = 1, ntrans
            call brzint_new( nsmooth, wfint_kset%ngridk, nk, wfint_kset%ikmap, nsube, ewin, mtrans, mtrans, &
                   edif( 1:mtrans, :, ist), &
                   fjdos( 1:mtrans, :), &
                   jdos( :, ist))
          end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
          ! total JDOS
          iq = 0
          do ist = 1, ntrans
            do jst = 1, mtrans
              if( (.not. (maxval( edif( jst, :, ist)) .lt. ewin(1))) .and. (.not. (minval( edif( jst, :, ist)) .gt. ewin(2)))) then
                iq = iq + 1
                ejdos( iq, :) = edif( jst, :, ist)
              end if
            end do
          end do
  
          call brzint_new( nsmooth, wfint_kset%ngridk, nk, wfint_kset%ikmap, nsube, ewin, iq, iq, &
                 ejdos( 1:iq, :), &
                 fjdos( 1:iq, :), &
                 jdos( :, 0))
          deallocate( fjdos, ejdos)            
        end if
        deallocate( edif)
        ntrans = ntrans*mtrans
      end if

      deallocate( e, energies)
      if( allocated( radcoeffr)) deallocate( radcoeffr)
      if( allocated( radolp)) deallocate( radolp)

      return
      
    end subroutine wfint_interpolate_dos
    !EOC

!--------------------------------------------------------------------------------------

    !BOP
    ! !ROUTINE: wfint_interpolate_dielmat
    ! !INTERFACE:
    !
    subroutine wfint_interpolate_dielmat( nomega, omegas, dielmat, comps, scissor)
      ! !USES
      use modxs, only: symt2
      ! !INPUT PARAMETERS:
      !   nomega  : number of frequencies (in, integer)
      !   omegas  : frequencies (in, real( nomega))
      !   dielmat : dielectric matrix (out, complex( 3, 3, nomega))
      !   comps   : the tensor components ( in, optional, integer( 2, *))
      !   scissor : energy scissor to apply (in, optional, real)
      ! !DESCRIPTION:
      ! Calculates the imaginary part of the dielectric matrix within the 
      ! independent (quasi-) particle approximation without local field effects.
      ! The real part is obtained from the Kramers-Kronig relations.
      !
      ! !REVISION HISTORY:
      !   Created June 2019 (SeTi)
      !EOP
      integer, intent( in)           :: nomega
      real(8), intent( in)           :: omegas( nomega)
      complex(8), intent( out)       :: dielmat( 3, 3, nomega)
      ! optional arguments
      integer, optional, intent( in) :: comps(:,:)
      real(8), optional, intent( in) :: scissor
      !BOC
      integer :: nc, cc(2,9)
      real(8) :: sciss

      integer :: un, recl, isym, ik, iq, ist, jst, lst, iw, i, j, k
      real(8) :: sc(3,3), epsi(9), v1(3), v2(3), tmpfermi, s, t0, t1
      logical :: exist, qpscale

      integer, allocatable :: ksymmap(:,:)
      real(8), allocatable :: eks(:,:), eqp(:,:), energies(:,:), wgt(:,:,:), epsr(:), opp(:,:,:,:)
      complex(8), allocatable :: pmat(:,:,:), pmatkh(:,:,:), pmatkw(:,:,:,:), pmatr(:,:,:,:), pmatqw(:,:,:), pmatqh(:,:,:)

      sciss = 0.d0
      if( present( scissor)) sciss = scissor
      qpscale = (abs( sciss) .gt. 1.d-8)

      nc = 0
      if( present( comps)) then
        if( size( comps, 1) .ne. 2) then
          write(*,*)
          write(*,'("Error (wfint_interpolate_dielmat): The first dimension of the components must be 2.")')
          stop
        end if
        nc = size( comps, 2)
        cc(:,1:nc) = comps
      end if

      ! compute all component if nothin else is speciefied
      if( nc .eq. 0) then
        do i = 1, 3
          do j = 1, 3
            nc = nc + 1
            cc( 1, nc) = i
            cc( 2, nc) = j
          end do
        end do
      end if

      ! get KS and QP energies on coarse grid
      if( any( input%properties%wannier%input .eq. (/'gw','gwold'/))) then
        call wfhelp_geteval( eks, ist, jst, mode='gwks')
        call wfhelp_geteval( eqp, ist, jst, mode='gw')
        qpscale = .true.
      else
        call wfhelp_geteval( eks, ist, jst)
        call wfhelp_geteval( eqp, ist, jst)
      end if
      write(*,*) qpscale

      ! get the occupation factors on the dense grid
      write(*,'(30a)',advance='no') 'occupancies'
      call timesec( t0)
      call wfint_interpolate_occupancy( usetetra=.true.)
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! set energies on dense grid
      write(*,'(30a)',advance='no') 'energies'
      call timesec( t0)
      allocate( energies( wf_nwf, wfint_kset%nkpt))
      energies = wfint_eval
      tmpfermi = wfint_efermi
      if( wf_fermizero) tmpfermi = 0.d0
      if( wf_fermizero) then
        energies = energies - wfint_efermi
        eks = eks - wfint_efermi
        eqp = eqp - wfint_efermi
      end if
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! apply scissor
      write(*,'(30a)',advance='no') 'scissor'
      call timesec( t0)
      do iq = 1, wfint_kset%nkpt
        do ist = 1, wf_nwf
          if( energies( ist, iq) .gt. tmpfermi) energies( ist, iq) = energies( ist, iq) + sciss
        end do
      end do
      do ik = 1, wf_kset%nkpt
        do ist = wf_fst, wf_lst
          if( eqp( ist, ik) .gt. tmpfermi) eqp( ist, ik) = eqp( ist, ik) + sciss
        end do
      end do
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! get the momentum matrix elements on the coarse grid
      write(*,'(30a)',advance='no') 'p(k)'
      call timesec( t0)
      allocate( pmatkw( wf_nwf, wf_nwf, 3, wf_kset%nkpt))
      allocate( pmat( 3, nstsv, nstsv), pmatkh( wf_fst:wf_lst, wf_fst:wf_lst, 3))
      
      inquire( file='PMAT.OUT', exist=exist)
      if( .not. exist) call writepmat
      
      inquire( iolength=recl) pmat
      call getunit( un)
      open( un, file='PMAT.OUT', action='read', form='unformatted', access='direct', recl=recl)
      do ik = 1, wf_kset%nkpt
        call findkpt( wf_kset%vkl( :, ik), isym, iq)
        read( un, rec=iq) pmat
        sc = symlatc( :, :, lsplsymc( isym))
        do ist = wf_fst, wf_lst
          do jst = wf_fst, wf_lst
            if( isym .gt. 1) then
              call r3mv( sc, dble( pmat( :, ist, jst)), v1)
              call r3mv( sc, aimag( pmat( :, ist, jst)), v2)
              pmatkh( ist, jst, :) = cmplx( v1, v2, 8)
            else
              pmatkh( ist, jst, :) = pmat( :, ist, jst)
            end if
            ! perform QP scaling if needed
            if( qpscale) then
              s = eks( jst, ik) - eks( ist, ik)
              if( abs( s) .gt. 1.d-8) then
                s = (eqp( jst, ik) - eqp( ist, ik))/s
                pmatkh( ist, jst, :) = pmatkh( ist, jst, :)*s
              end if
            end if
          end do
        end do
        call wfint_h2wk( pmatkh, pmatkw( :, :, :, ik), ik, 3)
      end do
      close( un)
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      deallocate( pmat, pmatkh, eks, eqp)
      !stop

      ! build k-point symmetry map
      write(*,'(30a)',advance='no') 'ksymm'
      call timesec( t0)
      i = nint( maxval( wfint_kset%wkpt( 1:wfint_kset%nkpt))*wfint_kset%nkptnr)
      allocate( ksymmap( 0:i, wfint_kset%nkpt))
      if( wfint_kset%isreduced) then
        ksymmap = 0
#ifdef USEOMP
!$omp parallel default( shared) private( ik, isym, iq)
!$omp do
#endif
        do ik = 1, wfint_kset%nkptnr
          call findkptinset( wfint_kset%vklnr( :, ik), wfint_kset, isym, iq)
#ifdef USEOMP
!$omp critical
#endif
          ksymmap( 0, iq) = ksymmap( 0, iq) + 1
          ksymmap( ksymmap( 0, iq), iq) = isym
#ifdef USEOMP
!$omp end critical
#endif
        end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
      else 
        ksymmap = 1
      end if
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! find lowest partially occupied band
      lst = 0
      do iq = 1, wfint_kset%nkpt
        do ist = 1, wf_nwf
          if( wfint_occ( ist, iq) .gt. 1.d-8) then
            lst = max( lst, ist)
          else
            exit
          end if
        end do
      end do

      ! get the R-dependent momentum matrix elements in Wannier gauge
      write(*,'(30a)',advance='no') 'p(R)'
      call timesec( t0)
      allocate( pmatr( wf_nwf, wf_nwf, 3, wf_nrpt))
      call wfint_ftk2r( pmatkw, pmatr, 3)
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! interband contribution
      write(*,'(30a)',advance='no') 'p(q)'
      call timesec( t0)
      dielmat = zzero
      allocate( opp( nc, wf_nwf, wf_nwf, wfint_kset%nkpt))
      allocate( pmat( wf_nwf, wf_nwf, 3), pmatqw( wf_nwf, wf_nwf, 3), pmatqh( wf_nwf, wf_nwf, 3))

      opp = 0.d0
#ifdef USEOMP
!$omp parallel default( shared) private( iq, ik, pmatqw, pmatqh, pmat, ist, jst, sc, v1, v2, i)
!$omp do
#endif
      do iq = 1, wfint_kset%nkpt
        call wfint_ftr2q( pmatr, pmatqw, iq, 3)
        call wfint_w2hq( pmatqw, pmatqh, iq, 3)
        do ik = 1, ksymmap( 0, iq)
          pmat = pmatqh
          do ist = 1, wf_nwf
            do jst = ist+1, wf_nwf
              if( ksymmap( ik, iq) .gt. 1) then
                sc = symlatc( :, :, lsplsymc( ksymmap( ik, iq)))
                call r3mv( sc, dble( pmat( ist, jst, :)), v1)
                call r3mv( sc, aimag( pmat( ist, jst, :)), v2)
                pmat( ist, jst, :) = cmplx( v1, v2, 8)
              end if
              do i = 1, nc
                opp( i, ist, jst, iq) = opp( i, ist, jst, iq) + (wfint_occ( ist, iq) - wfint_occ( jst, iq))*&
                                           dble( pmat( ist, jst, cc(1,i))*conjg( pmat( ist, jst, cc(2,i))))/&
                                           (wfint_kset%wkpt( iq)*dble( wfint_kset%nkptnr))
              end do
            end do
          end do
        end do
      end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
      call timesec( t1)
      write(*,'(f13.6)') t1-t0
      deallocate( pmat, pmatr, pmatqw, pmatqh)

      write(*,'(30a)',advance='no') 'integrate'
      call timesec( t0)
      allocate( wgt( wf_nwf, wf_nwf, wfint_kset%nkpt))
      do iw = 1, nomega
        call opt_tetra_int_ediff( wfint_tetra, 2, wfint_kset%nkpt, wf_nwf, energies, omegas( iw), 1, wgt, brange=(/1, lst, 0, wf_nwf/))
        call dgemv( 'n', nc, wf_nwf*wf_nwf*wfint_kset%nkpt, fourpi*pi/omega, &
               opp, nc, &
               wgt, 1, 0.d0, &
               epsi, 1)
        if( abs( omegas( iw)) .gt. 1.d-8) epsi = epsi/(omegas( iw)*omegas( iw))
        do i = 1, nc
          dielmat( cc(1,i), cc(2,i), iw) = cmplx( 0.d0, epsi(i), 8)
        end do
      end do
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      deallocate( energies, wgt, opp)

      allocate( epsr( nomega))
      do i = 1, nc
        call kramkron( cc(1,i), cc(2,i), 1.d-8, nomega, omegas, aimag( dielmat( cc(1,i), cc(2,i), :)), epsr)
          dielmat( cc(1,i), cc(2,i), :) = dielmat( cc(1,i), cc(2,i), :) + cmplx( epsr, 0.d0, 8)
        if( cc(1,i) .ne. cc(2,i)) dielmat( cc(2,i), cc(1,i), :) = dielmat( cc(1,i), cc(2,i), :)
      end do
      
      deallocate( epsr)

      return
    end subroutine wfint_interpolate_dielmat
    !EOC

!--------------------------------------------------------------------------------------

    !BOP
    ! !ROUTINE: wfint_interpolate_dielmat
    ! !INTERFACE:
    !
    subroutine wfint_interpolate_dielmat2( nomega, omegas, dielmat, comps, scissor)
      ! !USES
      use modxs, only: symt2
      ! !INPUT PARAMETERS:
      !   nomega  : number of frequencies (in, integer)
      !   omegas  : frequencies (in, real( nomega))
      !   dielmat : dielectric matrix (out, complex( 3, 3, nomega))
      !   comps   : the tensor components ( in, optional, integer( 2, *))
      !   scissor : energy scissor to apply (in, optional, real)
      ! !DESCRIPTION:
      ! Calculates the imaginary part of the dielectric matrix within the 
      ! independent (quasi-) particle approximation without local field effects.
      ! The real part is obtained from the Kramers-Kronig relations.
      !
      ! !REVISION HISTORY:
      !   Created June 2019 (SeTi)
      !EOP
      integer, intent( in)           :: nomega
      real(8), intent( in)           :: omegas( nomega)
      complex(8), intent( out)       :: dielmat( 3, 3, nomega)
      ! optional arguments
      integer, optional, intent( in) :: comps(:,:)
      real(8), optional, intent( in) :: scissor
      !BOC
      integer :: nc, cc(2,9)
      real(8) :: sciss, dq, m(3,3)

      integer :: un, recl, isym, ik, iq, ist, jst, lst, fst, iw, i, j, k
      real(8) :: sc(3,3), v1(3), v2(3), tmpfermi, s, t0, t1
      complex(8) :: epsi(9)
      logical :: exist, qpscale
      type( k_set) :: kset, ksetnr, kqsetnr

      integer, allocatable :: ksymmap(:,:)
      real(8), allocatable :: energies(:,:), occ(:,:), wgt(:,:,:), epsr(:)
      complex(8), allocatable :: u0(:,:,:), pmatq(:,:,:,:), opp(:,:,:,:)

      sciss = 0.d0
      if( present( scissor)) sciss = scissor
      qpscale = (abs( sciss) .gt. 1.d-8)

      dq = 0.001d0
      call r3minv( wf_kset%bvec, m)

      nc = 0
      if( present( comps)) then
        if( size( comps, 1) .ne. 2) then
          write(*,*)
          write(*,'("Error (wfint_interpolate_dielmat): The first dimension of the components must be 2.")')
          stop
        end if
        nc = size( comps, 2)
        cc(:,1:nc) = comps
      end if

      ! compute all component if nothin else is speciefied
      if( nc .eq. 0) then
        do i = 1, 3
          do j = 1, 3
            nc = nc + 1
            cc( 1, nc) = i
            cc( 2, nc) = j
          end do
        end do
      end if

      ! get the occupation factors on the dense grid
      write(*,'(30a)',advance='no') 'occupancies'
      call timesec( t0)
      call wfint_interpolate_occupancy( usetetra=.true.)
      allocate( occ( wf_nwf, wfint_kset%nkpt))
      occ = wfint_occ
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! set energies on dense grid
      write(*,'(30a)',advance='no') 'energies'
      call timesec( t0)
      allocate( energies( wf_nwf, wfint_kset%nkpt))
      energies = wfint_eval
      tmpfermi = wfint_efermi
      if( wf_fermizero) tmpfermi = 0.d0
      if( wf_fermizero) energies = energies - wfint_efermi
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! apply scissor
      write(*,'(30a)',advance='no') 'scissor'
      call timesec( t0)
      do iq = 1, wfint_kset%nkpt
        do ist = 1, wf_nwf
          if( energies( ist, iq) .gt. tmpfermi) energies( ist, iq) = energies( ist, iq) + sciss
        end do
      end do
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! build k-point symmetry map
      write(*,'(30a)',advance='no') 'ksymm'
      call timesec( t0)
      i = nint( maxval( wfint_kset%wkpt( 1:wfint_kset%nkpt))*wfint_kset%nkptnr)
      allocate( ksymmap( 0:i, wfint_kset%nkpt))
      if( wfint_kset%isreduced) then
        ksymmap = 0
#ifdef USEOMP
!$omp parallel default( shared) private( ik, isym, iq)
!$omp do
#endif
        do ik = 1, wfint_kset%nkptnr
          call findkptinset( wfint_kset%vklnr( :, ik), wfint_kset, isym, iq)
#ifdef USEOMP
!$omp critical
#endif
          ksymmap( 0, iq) = ksymmap( 0, iq) + 1
          ksymmap( ksymmap( 0, iq), iq) = isym
#ifdef USEOMP
!$omp end critical
#endif
        end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
      else 
        ksymmap = 1
      end if
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      ! find highest partially and fully occupied band
      lst = 0
      fst = 0
      do iq = 1, wfint_kset%nkpt
        do ist = 1, wf_nwf
          if( occ( ist, iq) .gt. 1.d-8) then
            lst = max( lst, ist)
            if( occ( ist, iq) .gt. 2.d0 - 1.d-8) fst = max( fst, ist)
          else
            exit
          end if
        end do
      end do

      ! interband contribution
      write(*,'(30a)',advance='no') 'p(q)'
      call timesec( t0)
      dielmat = zzero

      ! get the momentum matrix elements on the coarse grid
      kset = wfint_kset
      call generate_k_vectors( ksetnr, wf_kset%bvec, wfint_kset%ngridk, (/0.d0, 0.d0, 0.d0/), .false., uselibzint=.false.)
      call wfint_init( ksetnr)
      allocate( u0( wf_nwf, wf_nwf, ksetnr%nkpt))
      allocate( pmatq( wf_nwf, wf_nwf, 3, ksetnr%nkpt))
      u0 = wfint_transform

      do k = 1, 3
        kqsetnr = ksetnr
        do iq = 1, kqsetnr%nkpt
          kqsetnr%vkc(k,iq) = kqsetnr%vkc(k,iq) + dq
          !call r3mv( m, kqsetnr%vkc( :, iq), kqsetnr%vkl( :, iq))
          kqsetnr%vkl(:,iq) = kqsetnr%vkl(:,iq) + dq*m(:,k)
        end do
        call wfint_init( kqsetnr)
        do iq = 1, kqsetnr%nkpt
          call zgemm( 'c', 'n', wf_nwf, wf_nwf, wf_nwf, cmplx( 1.d0/dq, 0, 8), &
                 wfint_transform( :, :, iq), wf_nwf, &
                 u0( :, :, iq), wf_nwf, zzero, &
                 pmatq( :, :, k, iq), wf_nwf)
        end do
      end do
      deallocate( u0)
      call writematlab( pmatq( :, :, 1, 1), '1')
      call writematlab( pmatq( :, :, 2, 1), '2')
      call writematlab( pmatq( :, :, 3, 1), '3')

      allocate( opp( nc, wf_nwf, wf_nwf, kset%nkpt))
      opp = zzero
!#ifdef USEOMP
!!$omp parallel default( shared) private( iq, ik, ist, jst, i)
!!$omp do
!#endif
      do ik = 1, ksetnr%nkpt
        iq = kset%ik2ikp( ik)
        do ist = 1, lst
          do jst = lst+1, wf_nwf
            do i = 1, nc
              opp( i, ist, jst, iq) = opp( i, ist, jst, iq) + (occ( ist, iq) - occ( jst, iq))*&
                                         ( pmatq( ist, jst, cc(1,i), ik)*conjg( pmatq( ist, jst, cc(2,i), ik)))/&
                                         (kset%wkpt( iq)*dble( kset%nkptnr))
            end do
          end do
        end do
      end do
!#ifdef USEOMP
!!$omp end do
!!$omp end parallel
!#endif
      !call plotmat( cmplx( opp( 1, :, :, 12), 0.d0, 8))
      !stop
      deallocate( pmatq)
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      write(*,'(30a)',advance='no') 'integrate'
      call timesec( t0)
      allocate( wgt( wf_nwf, wf_nwf, kset%nkpt))
      call wfhelp_init_tetra( wfint_tetra, kset, ttype=2, reduce=.true.)
      do iw = 1, nomega
        call opt_tetra_int_ediff( wfint_tetra, 2, kset%nkpt, wf_nwf, energies, omegas( iw), 1, wgt, brange=(/1, lst, 0, wf_nwf/))
        call zgemv( 'n', nc, wf_nwf*wf_nwf*kset%nkpt, cmplx( fourpi*pi/omega, 0.d0, 8), &
               opp, nc, &
               cmplx( wgt, 0.d0, 8), 1, zzero, &
               epsi, 1)
        write(*,'(i,f13.6,18g20.10)') iw, omegas( iw), epsi( 1:nc)
        if( abs( omegas( iw)) .gt. 1.d-8) epsi = epsi/(omegas( iw)*omegas( iw))
        do i = 1, nc
          dielmat( cc(1,i), cc(2,i), iw) = cmplx( 0.d0, dble( epsi(i)), 8)
        end do
      end do
      call timesec( t1)
      write(*,'(f13.6)') t1-t0

      deallocate( energies, occ, wgt, opp)

      allocate( epsr( nomega))
      do i = 1, nc
        call kramkron( cc(1,i), cc(2,i), 1.d-8, nomega, omegas, aimag( dielmat( cc(1,i), cc(2,i), :)), epsr)
          dielmat( cc(1,i), cc(2,i), :) = dielmat( cc(1,i), cc(2,i), :) + cmplx( epsr, 0.d0, 8)
        if( cc(1,i) .ne. cc(2,i)) dielmat( cc(2,i), cc(1,i), :) = dielmat( cc(1,i), cc(2,i), :)
      end do

      !do iw = 1, nomega
      !  write(*,'(i,g20.10,2g20.10)') iw, omegas( iw), dielmat( 1, 1, iw)
      !end do
      
      deallocate( epsr)

      return
    end subroutine wfint_interpolate_dielmat2
    !EOC

!--------------------------------------------------------------------------------------
      
    ! interpolates the density matrix
    ! needed for band character and PDOS
    subroutine wfint_pwmat
      use mod_pwmat
      use m_plotmat
      integer :: ik, iq, ikq, ir1, ir2
      real(8) :: vkql(3)

      complex(8), allocatable :: mkq0(:,:), mkq(:,:,:,:), mrr(:,:,:,:)
      complex(8), allocatable :: evec1(:,:,:), evec2(:,:,:)
      complex(8), allocatable :: auxmat(:,:)

      ! generate PW matrix elements on coarse grid
      allocate( mkq0( wf_fst:wf_lst, wf_fst:wf_lst))
      allocate( mkq( wf_nwf, wf_nwf, wf_kset%nkpt, wf_kset%nkpt))
      allocate( evec1( nmatmax_ptr, nstfv, nspinor))
      allocate( evec2( nmatmax_ptr, nstfv, nspinor))
      allocate( auxmat( wf_fst:wf_lst, wf_nwf))
      call pwmat_init( input%groundstate%lmaxapw, 8, wf_kset, wf_fst, wf_lst, wf_fst, wf_lst, &
             fft=.false.)
      do ik = 1, wf_kset%nkpt
        call wfhelp_getevec( ik, evec1)
        call pwmat_prepare( ik, evec1( :, :, 1))
      end do
      do iq = 1, wf_kset%nkpt
        write(*,*) iq
        call pwmat_init_qg( wf_kset%vkl(:,iq), (/0, 0, 0/), 1)
        do ik = 1, wf_kset%nkpt
          vkql = wf_kset%vkl(:,ik) + wf_kset%vkl(:,iq)
          call findkptinset( vkql, wf_kset, ir1, ikq)
          call wfhelp_getevec( ik,  evec1)
          call wfhelp_getevec( ikq, evec2)
          call pwmat_genpwmat( ik, evec1(:,wf_fst:wf_lst,1), evec2(:,wf_fst:wf_lst,1), mkq0)
          call zgemm( 'n', 'n', wf_nst, wf_nwf, wf_nst, zone, &
                 mkq0, wf_nst, &
                 wf_transform(:,:,ikq), wf_nst, zzero, &
                 auxmat, wf_nst)
          call zgemm( 'c', 'n', wf_nwf, wf_nwf, wf_nst, zone, &
                 wf_transform(:,:,ik), wf_nst, &
                 auxmat, wf_nst, zzero, &
                 mkq(1,1,ik,iq), wf_nwf)
        end do
      end do
      deallocate( evec1, evec2, mkq0, auxmat)

      ! Fourier transform to R,R'
      allocate( mrr( wf_nwf, wf_nwf, wf_nrpt, wf_nrpt))
      allocate( auxmat( wf_nrpt, wf_nrpt))
      call wfint_ftkk2rr( mkq, mrr, 1)
      do ir2 = 1, wf_nrpt
        do ir1 = 1, wf_nrpt
          auxmat(ir1,ir2) = maxval( abs( mrr(:,:,ir1,ir2)))
        end do
      end do
      call writematlab( auxmat, 'mrr')
      deallocate( auxmat, mkq, mrr)
    end subroutine wfint_pwmat
!--------------------------------------------------------------------------------------
      
    ! interpolates the density matrix
    ! needed for band character and PDOS
    subroutine wfint_interpolate_dmat( lmax, lammax, iq, radcoeffr, radolp, dmat, diagonly)
      integer, intent( in)           :: lmax, lammax, iq
      complex(8), intent( in)        :: radcoeffr( wf_nwf, lammax, (lmax+1)**2, natmtot, wf_nrpt)
      real(8), intent( in)           :: radolp( lammax, lammax, 0:lmax, natmtot)
      complex(8), intent( out)       :: dmat( (lmax+1)**2, (lmax+1)**2, wf_nwf, natmtot)
      logical, optional, intent( in) :: diagonly

      integer :: ir, is, ia, ias, o, l1, m1, lm1, m2, lm2, lmmax, ilo1, maxdim, ist, jst
      integer :: lamcnt( 0:lmax, nspecies), o2idx( apwordmax, 0:lmax, nspecies), lo2idx( nlomax, 0:lmax, nspecies), lm2l( (lmax+1)**2)

      complex(8), allocatable :: radcoeffq1(:,:), radcoeffq2(:,:), U(:,:), auxmat(:,:), wgts(:,:,:)
      logical :: diag

      complex(8) :: zdotc

      diag = .false.
      if( present( diagonly)) diag = diagonly

      lm2l = 0
      do l1 = 0, lmax
        do m1 = -l1, l1
          lm2l( idxlm( l1, m1)) = l1
        end do
      end do
      lmmax = (lmax+1)**2
      maxdim = 0
      do is = 1, nspecies
        o2idx( :, :, is) = 0
        lo2idx( :, :, is) = 0
        lamcnt( :, is) = 0
        do l1 = 0, lmax
          do o = 1, apword( l1, is)
            lamcnt( l1, is) = lamcnt( l1, is) + 1
            o2idx( o, l1, is) = lamcnt( l1, is)
          end do
        end do
        do ilo1 = 1, nlorb( is)
          l1 = lorbl( ilo1, is)
          if( l1 .le. lmax) then
            lamcnt( l1, is) = lamcnt( l1, is) + 1
            lo2idx( ilo1, l1, is) = lamcnt( l1, is)
          end if
        end do
        maxdim = max( maxdim, maxval( lamcnt( :, is)))
      end do
      if( maxdim .ne. lammax) then
        if( mpiglobal%rank .eq. 0) then
          write(*,*)
          write( *, '("Error (wfint_interpolate_dmat): Inconsistent input. Check lmax and lammax.")')
        end if
        stop
      end if

      ! build q-point density coefficients
      if( wfint_mindist) then
        allocate( wgts( wf_nwf, wf_nwf, wf_nrpt))
        do ir = 1, wf_nrpt
          call wfint_getmindistwgts( ir, iq, wgts( :, :, ir))
          wgts( :, :, ir) = transpose( wgts( :, :, ir))
        end do
      end if
          
      dmat = zzero

#ifdef USEOMP
!$omp parallel default( shared) private( is, ia, ias, l1, lm1, m2, lm2, radcoeffq1, radcoeffq2, ir, ist, jst, U, auxmat)
#endif
      allocate( radcoeffq1( maxdim, wf_nwf))
      allocate( radcoeffq2( maxdim, wf_nwf))
      allocate( auxmat( maxdim, wf_nwf))
      allocate( U( wf_nwf, wf_nwf))
      U = wfint_transform(:,:,iq)
#ifdef USEOMP
!$omp do collapse( 3)
#endif
      do is = 1, nspecies
        do ia = 1, natoms( is)

          do lm1 = 1, lmmax

            l1 = lm2l( lm1)
            ias = idxas( ia, is)
            radcoeffq1 = zzero
            do ir = 1, wf_nrpt
              if( wfint_mindist) U = wfint_transform(:,:,iq)*wgts(:,:,ir)
              call zgemm( 't', 'n', maxdim, wf_nwf, wf_nwf, wfint_pqr( iq, ir), &
                     radcoeffr( :, :, lm1, ias, ir), wf_nwf, &
                     U, wf_nwf, zone, &
                     radcoeffq1, maxdim)
            end do
            if( diag) then
              call zgemm( 't', 'n', maxdim, wf_nwf, maxdim, zone, &
                     cmplx( radolp( :, :, l1, ias), 0, 8), maxdim, &
                     radcoeffq1, maxdim, zzero, &
                     auxmat, maxdim)
              do ist = 1, wf_nwf
                dmat( lm1, lm1, ist, ias) = zdotc( lamcnt( l1, is), radcoeffq1( :, ist), 1, auxmat( :, ist), 1) 
              end do
            else
              do m2 = -l1, l1
                lm2 = idxlm( l1, m2)
                ! calculate q-point density coefficient
                radcoeffq2 = zzero
                do ir = 1, wf_nrpt
                  if( wfint_mindist) U = wfint_transform(:,:,iq)*wgts(:,:,ir)
                  call zgemm( 't', 'n', maxdim, wf_nwf, wf_nwf, wfint_pqr( iq, ir), &
                         radcoeffr( :, :, lm2, ias, ir), wf_nwf, &
                         U, wf_nwf, zone, &
                         radcoeffq2, maxdim)
                end do
                call zgemm( 't', 'n', maxdim, wf_nwf, maxdim, zone, &
                       cmplx( radolp( :, :, l1, ias), 0, 8), maxdim, &
                       radcoeffq1, maxdim, zzero, &
                       auxmat, maxdim)
                do ist = 1, wf_nwf
                  dmat( lm1, lm2, ist, ias) = zdotc( lamcnt( l1, is), radcoeffq2( :, ist), 1, auxmat( :, ist), 1) 
                end do
              end do
            end if

          end do

        end do
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( radcoeffq1, radcoeffq2, auxmat, U)
#ifdef USEOMP
!$omp end parallel
#endif
      if( wfint_mindist) deallocate( wgts)
      return
      
    end subroutine wfint_interpolate_dmat

!--------------------------------------------------------------------------------------
      
    ! generates radial overlaps of basis functions
    subroutine wfint_gen_radolp( lmax, lammax, radolp)
      integer, intent( in) :: lmax
      integer, intent( out) :: lammax
      real(8), allocatable, intent( out) :: radolp(:,:,:,:)

      integer :: is, ia, ias, o, l1, lmmax, ilo1, ilo2, maxdim
      integer :: lamcnt( 0:lmax, nspecies), o2idx( apwordmax, 0:lmax, nspecies), lo2idx( nlomax, 0:lmax, nspecies)

      lmmax = (lmax + 1)**2

      call wfhelp_genradfun

      maxdim = 0
      do is = 1, nspecies
        o2idx( :, :, is) = 0
        lo2idx( :, :, is) = 0
        lamcnt( :, is) = 0
        do l1 = 0, lmax
          do o = 1, apword( l1, is)
            lamcnt( l1, is) = lamcnt( l1, is) + 1
            o2idx( o, l1, is) = lamcnt( l1, is)
          end do
        end do
        do ilo1 = 1, nlorb( is)
          l1 = lorbl( ilo1, is)
          if( l1 .le. lmax) then
            lamcnt( l1, is) = lamcnt( l1, is) + 1
            lo2idx( ilo1, l1, is) = lamcnt( l1, is)
          end if
        end do
        maxdim = max( maxdim, maxval( lamcnt( :, is)))
      end do
      lammax = maxdim

      ! radial overlap-intergrals
      if( allocated( radolp)) deallocate( radolp)
      allocate( radolp( maxdim, maxdim, 0:lmax, natmtot))
      radolp(:,:,:,:) = 0.d0
      do is = 1, nspecies
        do ia = 1, natoms( is)
          ias = idxas( ia, is)
          ! APW-APW integral
          do l1 = 0, lmax
            do o = 1, apword( l1, is)
              radolp( o2idx( o, l1, is), o2idx( o, l1, is), l1, ias) = 1.d0
            end do
          end do
          do ilo1 = 1, nlorb( is)
            l1 = lorbl( ilo1, is)
            if( l1 .le. lmax) then
              ! APW-LO integral
              do o = 1, apword( l1, is)
                if( (o2idx( o, l1, is) .gt. 0) .and. (lo2idx( ilo1, l1, is) .gt. 0)) then
                  radolp( o2idx( o, l1, is), lo2idx( ilo1, l1, is), l1, ias) = oalo( o, ilo1, ias)
                  radolp( lo2idx( ilo1, l1, is), o2idx( o, l1, is), l1, ias) = oalo( o, ilo1, ias)
                end if
              end do
              ! LO-LO integral
              do ilo2 = 1, nlorb( is)
                if( lorbl( ilo2, is) .eq. l1) then
                  if( (lo2idx( ilo1, l1, is) .gt. 0) .and. (lo2idx( ilo2, l1, is) .gt. 0)) then
                    radolp( lo2idx( ilo1, l1, is), lo2idx( ilo2, l1, is), l1, ias) = ololo( ilo1, ilo2, ias)
                    radolp( lo2idx( ilo2, l1, is), lo2idx( ilo1, l1, is), l1, ias) = ololo( ilo2, ilo1, ias)
                  end if
                end if
              end do
            end if
          end do
        end do
      end do
   
      return
      
    end subroutine wfint_gen_radolp

!--------------------------------------------------------------------------------------
      
    ! generates R-dependent muffin-tin density coefficients
    subroutine wfint_gen_radcoeffr( lmax, lammax, radcoeffr)
      integer, intent( in) :: lmax
      integer, intent( out) :: lammax
      complex(8), allocatable, intent( out) :: radcoeffr(:,:,:,:,:)

      integer :: ik, ir, is, ia, ias, l1, m1, lm1, o, ilo1, lmmax, ngknr, maxdim
      integer :: lamcnt( 0:lmax, nspecies), o2idx( apwordmax, 0:lmax, nspecies), lo2idx( nlomax, 0:lmax, nspecies)

      complex(8), allocatable :: evecfv(:,:,:), apwalm(:,:,:,:,:), radcoeffk(:,:,:,:,:)

      lmmax = (lmax + 1)**2

      call wfhelp_genradfun

      maxdim = 0
      do is = 1, nspecies
        o2idx( :, :, is) = 0
        lo2idx( :, :, is) = 0
        lamcnt( :, is) = 0
        do l1 = 0, lmax
          do o = 1, apword( l1, is)
            lamcnt( l1, is) = lamcnt( l1, is) + 1
            o2idx( o, l1, is) = lamcnt( l1, is)
          end do
        end do
        do ilo1 = 1, nlorb( is)
          l1 = lorbl( ilo1, is)
          if( l1 .le. lmax) then
            lamcnt( l1, is) = lamcnt( l1, is) + 1
            lo2idx( ilo1, l1, is) = lamcnt( l1, is)
          end if 
        end do
        maxdim = max( maxdim, maxval( lamcnt( :, is)))
      end do
      lammax = maxdim
   
      ! build k-point density coefficients
      allocate( radcoeffk( wf_fst:wf_lst, maxdim, lmmax, natmtot, wf_kset%nkpt))
      allocate( evecfv( nmatmax_ptr, nstfv, nspnfv))
      allocate( apwalm( ngkmax_ptr, apwordmax, lmmaxapw, natmtot, nspnfv))
      radcoeffk(:,:,:,:,:) = zzero

      do ik = 1, wf_kset%nkpt
        ngknr = wf_Gkset%ngk( 1, ik)

        ! get matching coefficients
        call match( ngknr, wf_Gkset%gkc( :, 1, ik), wf_Gkset%tpgkc( :, :, 1, ik), wf_Gkset%sfacgk( :, :, 1, ik), apwalm( :, :, :, :, 1))
          
        ! read eigenvector      
        call wfhelp_getevec( ik, evecfv)

        do is = 1, nspecies
          do ia = 1, natoms( is)
            ias = idxas( ia, is)
            ! APW contribution
            do l1 = 0, lmax
              do m1 = -l1, l1
                lm1 = idxlm( l1, m1)
                do o = 1, apword( l1, is)
                  call zgemv( 't', ngknr, wf_nwf, zone, &
                         evecfv( 1, wf_fst, 1), nmatmax_ptr, &
                         apwalm( 1, o, lm1, ias, 1), 1, zzero, &
                         radcoeffk( wf_fst, o2idx( o, l1, is), lm1, ias, ik), 1)
                end do
              end do
            end do
            ! LO contribution
            do ilo1 = 1, nlorb( is)
              l1 = lorbl( ilo1, is)
              if( l1 .le. lmax) then
                do m1 = -l1, l1
                  lm1 = idxlm( l1, m1)
                  radcoeffk( :, lo2idx( ilo1, l1, is), lm1, ias, ik) = evecfv( ngknr+idxlo( lm1, ilo1, ias), wf_fst:wf_lst, 1)
                end do
              end if
            end do
          end do
        end do
      end do

      deallocate( evecfv, apwalm)

      ! build R-point density coefficients
      if( allocated( radcoeffr)) deallocate( radcoeffr)
      allocate( radcoeffr( wf_nwf, maxdim, lmmax, natmtot, wf_nrpt))
      radcoeffr(:,:,:,:,:) = zzero
#ifdef USEOMP                
!$omp parallel default( shared) private( ir, ik, is, ia, ias, lm1)
!$omp do
#endif
      do ir = 1, wf_nrpt
        do ik = 1, wf_kset%nkpt
          do is = 1, nspecies
            do ia = 1, natoms( is)
              ias = idxas( ia, is)
              do lm1 = 1, lmmax
                call zgemm( 't', 'n', wf_nwf, maxdim, wf_nst, wf_pkr( ik, ir)/wf_kset%nkpt, &
                       wf_transform( :, :, ik), wf_nst, &
                       radcoeffk( :, :, lm1, ias, ik), wf_nst, zone, &
                       radcoeffr( :, :, lm1, ias, ir), wf_nwf)
              end do
            end do
          end do
        end do
      end do
#ifdef USEOMP                
!$omp end do
!$omp end parallel
#endif
      
      deallocate( radcoeffk)

      return
      
    end subroutine wfint_gen_radcoeffr

!--------------------------------------------------------------------------------------

    subroutine wfint_findbandmap
      integer :: ik, iq, ist, jst, fst, lst, nr
      character(32) :: inpt
      
      integer, allocatable :: map(:,:)
      real(8), allocatable :: eval(:,:)

      if( allocated( wfint_bandmap)) deallocate( wfint_bandmap)
      allocate( wfint_bandmap( wf_nwf))
      wfint_bandmap = 0

      if( any( input%properties%wannier%input .eq. (/'gw','gwold'/))) then
        call wfhelp_geteval( eval, fst, lst, mode='gwks')
      else
        call wfhelp_geteval( eval, fst, lst)
      end if
      call wfint_init( wf_kset, evalin=eval( wf_fst:wf_lst, :))

      allocate( map( wf_nwf, wf_kset%nkpt))
      map = 0
      do ik = 1, wf_kset%nkpt
        do ist = 1, wf_nwf
          do jst = fst, lst
            if( abs( eval( jst, ik) - wfint_eval( ist, ik)) .lt. 1.d-6) then
              map( ist, ik) = jst
              exit
            end if
          end do
        end do
      end do
      fst = 0
      do ist = 1, wf_nwf
        jst = minval( map( ist, :))
        if( (jst .eq. maxval( map( ist, :))) .and. (jst .ne. 0)) then
          wfint_bandmap( ist) = jst
          if( fst .eq. 0) fst = ist
        end if
      end do
      if( fst .eq. 0) then
        if( mpiglobal%rank .eq. 0) then
          write(*,*)
          write( *, '("Error (wfint_findbandmap) Bands cannot be maped!")')
        end if
        stop
      end if

      if( fst .gt. 1) then
        do ist = fst - 1, 1, -1
          wfint_bandmap( ist) = wfint_bandmap( ist + 1) - 1
        end do
      end if
      do ist = fst + 1, wf_nwf
        if( wfint_bandmap( ist) .eq. 0) wfint_bandmap( ist) = wfint_bandmap( ist - 1) + 1
      end do

      deallocate( eval, map)
      return
    end subroutine wfint_findbandmap
    
!--------------------------------------------------------------------------------------

    !BOP
    ! !ROUTINE: wfint_matchksgw_linreal
    ! !INTERFACE:
    !
    subroutine wfint_matchksgw_linreal( kset, fst, lst, rin, rout, fill)
      ! !INPUT PARAMETERS:
      !   kset     : kset on which the quantity is given (in, type( k_set))
      !   fst, lst : first and last band index for which the input is provided (in, integer)
      !   rin      : real scalar quantity corresponding to KS bands (in, real( fst:lst, kset%nkpt))
      !   rout     : quantity matched onto GW Wannier bands (out, real( wf_nwf, kset%nkpt))
      !   fill     : fill value for bands outside the wannierized bands (default: 0.d0) (in, optional, real)
      ! !DESCRIPTION:
      ! Matches a scalar real quatity corresponding to the KS bands at the given kset
      ! onto the Wannier interpolated GW bands at the given kset.
      !
      ! !REVISION HISTORY:
      !   Created June 2019 (SeTi)
      !EOP
      type( k_set), intent( in)      :: kset
      integer, intent( in)           :: fst, lst
      real(8), intent( in)           :: rin( fst:lst, kset%nkpt)
      real(8), intent( out)          :: rout( wf_nwf, kset%nkpt)
      real(8), optional, intent( in) :: fill
      !BOC

      integer :: iq, ist, jst
      real(8) :: f
      real(8), allocatable :: r(:), evalinks(:,:), evalinqp(:,:)
      complex(8), allocatable :: olp(:,:), evecintks(:,:,:)

      f = 0.d0
      if( present( fill)) f = fill

      call wfint_findbandmap

      allocate( olp( wf_nwf, wf_nwf), evecintks( wf_nwf, wf_nwf, kset%nkpt), r( wf_nwf))
      call wfhelp_geteval( evalinks, ist, jst, mode='gwks')
      call wfhelp_geteval( evalinqp, ist, jst, mode='gw')
      call wfint_init( kset, evalin=evalinks( wf_fst:wf_lst, :))
      evecintks = wfint_transform
      call wfint_init( kset, evalin=evalinqp( wf_fst:wf_lst, :))

      do iq = 1, kset%nkpt
        r = f
        do ist = 1, wf_nwf
          jst = wfint_bandmap( ist)
          if( (jst .ge. fst) .and. (jst .le. lst)) r( ist) = rin( jst, iq)
        end do
        call zgemm( 'c', 'n', wf_nwf, wf_nwf, wf_nwf, zone, &
               evecintks( :, :, iq), wf_nwf, &
               wfint_transform( :, :, iq), wf_nwf, zzero, &
               olp, wf_nwf)
        call dgemv( 't', wf_nwf, wf_nwf, 1.d0, &
               abs( olp)**2, wf_nwf, &
               r, 1, 0.d0, &
               rout( :, iq), 1)
      end do

      deallocate( r, olp, evecintks)
      if( allocated( evalinks)) deallocate( evalinks)
      if( allocated( evalinqp)) deallocate( evalinqp)
      
    end subroutine wfint_matchksgw_linreal
    !EOC

end module mod_wannier_interpolate
