module mod_wannier_opf
  use mod_wannier_variables
  use mod_wannier_omega
  use mod_stiefel_optim
  use m_linalg
  use m_plotmat

  implicit none

  private

! module variables
  real(8), parameter :: epssvd = 1.d-6

  integer :: N0, N  ! external and internal number of states
  integer :: J      !number of WFs
  integer :: P      !number of projectors
  logical :: sub    ! start from subspace
  complex(8), allocatable :: OPF(:,:,:) ! OPF matrix
  complex(8), allocatable :: U0(:,:,:)  ! original transformation matrices 
  complex(8), allocatable :: A(:,:,:)   ! overlap matrices

  ! for standard OPFs
  integer :: NY           ! number of X matrices
  real(8) :: lambda       ! Lagrangian multiplier
  real(8), allocatable    :: t(:)       ! weights
  complex(8), allocatable :: Y(:,:,:)   ! X matrices
  
  ! for improved OPFs
  integer :: minit, maxit
  integer :: dxo(2)
  real(8) :: gradnorm
  integer, allocatable :: dx(:,:)

! methods
  public :: wfopf_gen
    
  contains
    subroutine wfopf_gen( subspace)
      logical, optional, intent( in) :: subspace
      ! local variables
      integer :: ik, i, it, mit
      real(8) :: sum_n_wgt, phi, grad, omega
      real(8) :: t0, t1
      character( 256) :: fname
      logical :: gradient

      ! allocatable arrays
      real(8), allocatable :: val(:)
      complex(8), allocatable :: X(:,:), lvec(:,:), rvec(:,:)

      gradient = .true.

      if( mpiglobal%rank .eq. 0) then
        if( gradient) then
          write( wf_info, '(" calculate improved optimized projection functions (iOPF)...")')
        else
          write( wf_info, '(" calculate optimized projection functions (OPF)...")')
        end if
      end if
      call timesec( t0)
      minit    = input%properties%wannier%grouparray( wf_group)%group%minitopf
      maxit    = input%properties%wannier%grouparray( wf_group)%group%maxitopf
      gradnorm = input%properties%wannier%grouparray( wf_group)%group%epsopf

      !****************************
      !* PREPARATION
      !****************************
      sub = .false.
      if( present( subspace)) sub = subspace

      J = wf_groups( wf_group)%nwf
      P = wf_groups( wf_group)%nproj
      N0 = wf_groups( wf_group)%nst
      N = N0
      if( sub) N = J
      NY = wf_kset%nkpt*(1+wf_n_ntot)

      dxo = (/P,J/)
      allocate( dx(2,1))
      dx(:,1) = dxo

      sum_n_wgt = 2.d0*sum( wf_n_wgt)
      lambda = sum_n_wgt*input%properties%wannier%grouparray( wf_group)%group%lambdaopf

      allocate( U0(N0,J,wf_kset%nkpt))
      allocate( A(N,P,wf_kset%nkpt))
      allocate( OPF(P,J,1))
      allocate( Y(P,P,NY))
      allocate( t(NY))

      call wfopf_init

      !****************************
      !* MINIMIZATION
      !****************************
      allocate( X(P,P))
      it = 0
      ! start minimization
      call zucomp( OPF(:,:,1), X)

      if( gradient) then
        call wfopf_gen_g( minit, maxit, OPF, gradnorm)
        !call wfopf_omega( OPF, dxo, 1, dx, omega)
        !call zucomp( OPF(:,:,1), X)
        !call wfopf_lagrangian( NY, J, P, t, Y, X, phi)
        !phi = phi + J*sum_n_wgt
        !write(*,'(4x,2f13.6,g13.6)') omega, phi, grad
      else
        call wfopf_gen_l( minit, maxit, X, gradnorm)
        OPF(:,:,1) = X(:,1:J)
        !call wfopf_omega( OPF, dxo, 1, dx, omega)
        !call wfopf_lagrangian( NY, J, P, t, Y, X, phi)
        !phi = phi + J*sum_n_wgt
        !write(*,'(4x,2f13.6,g13.6)') omega, phi, grad
      end if

      if( allocated( wf_opf)) deallocate( wf_opf)
      allocate( wf_opf(P,J))
      wf_opf(:,:) = OPF(:,:,1)
      deallocate( X)

      !****************************
      ! find transformation matrices from OPFs
      !****************************
#ifdef USEOMP                
!$omp parallel default( shared) private( ik, X, val, lvec, rvec)
#endif
      allocate( X(N,J), val(J), lvec(N,N), rvec(J,J))
#ifdef USEOMP
!$omp do
#endif
      do ik = 1, wf_kset%nkpt
        call zgemm( 'n', 'n', N, J, P, zone, A(1,1,ik), N, wf_opf, P, zzero, X, N)
        call zsvd( X, val, lvec, rvec)
        call zgemm( 'n', 'n', N, J, J, zone, lvec, N, rvec, J, zzero, X, N)
        if( sub) then
          call zgemm( 'n', 'n', N0, J, N, zone, U0(1,1,ik), N0, X, N, zzero, &
               wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, ik), wf_nst)
        else
          wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                        wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, ik) = X
        end if
      end do
#ifdef USEOMP
!$omp end do
#endif
      deallocate( X, val, lvec, rvec)
#ifdef USEOMP
!$omp end parallel
#endif

      !****************************
      !* FINALIZATION
      !****************************
      ! determine phases for logarithms
      do i = 1, 5
        call wfomega_m
        call wfomega_diagphases( wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, 1), wf_nst, wf_nwf, wf_groups( wf_group)%nst)
      end do

      allocate( lvec( wf_nwf, wf_nwf))
      phi = 0.d0
      do ik = 1, wf_kset%nkpt
        call zgemm( 'c', 'n', wf_nwf, wf_nwf, wf_nst, zone, &
               wf_transform(:,:,ik), wf_nst, &
               wf_transform(:,:,ik), wf_nst, zzero, &
               lvec, wf_nwf)
        do i = 1, wf_nwf
          lvec(i,i) = lvec(i,i) - zone
        end do
        phi = phi + sum( abs( lvec))
      end do
      deallocate( lvec)
      ! calculate spread
      call wfomega_gen

      call timesec( t1)
      if( mpiglobal%rank .eq. 0) then
        write( wf_info, '(5x,"duration (seconds): ",T40,3x,F10.1)') t1-t0
        !write( wf_info, '(5x,"minimum iterations: ",T40,7x,I6)') minit
        write( wf_info, '(5x,"iterations: ",T40,7x,I6)') maxit
        if( gradient) then
          write( wf_info, '(5x,"gradient cutoff: ",T40,E13.6)') input%properties%wannier%grouparray( wf_group)%group%epsopf
          write( wf_info, '(5x,"norm of gradient: ",T40,E13.6)') gradnorm
        else
          write( wf_info, '(5x,"cutoff uncertainty: ",T40,E13.6)') input%properties%wannier%grouparray( wf_group)%group%epsopf
          write( wf_info, '(5x,"achieved uncertainty: ",T40,E13.6)') gradnorm
          write( wf_info, '(5x,"Lagrangian: ",T40,F13.6)') phi
        end if
        write( wf_info, '(5x,"Omega: ",T40,F13.6)') sum( wf_omega ( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))
        write( wf_info, *)
        call flushifc( wf_info)
      end if

      call wfopf_destroy
      return
      !EOC
    end subroutine wfopf_gen
    !EOP

    subroutine wfopf_gen_g( minit, maxit, X, eps)
      use m_getunit
      use mod_manopt, only: manopt_stiefel_cg, manopt_stiefel_lbfgs
      integer, intent( inout)    :: minit
      integer, intent( inout)    :: maxit
      complex(8), intent( inout) :: X(P,J,1)
      real(8), intent( inout)    :: eps

      integer :: convun
      real(8) :: omega0
      character(64) :: convfname

      call wfopf_omegagradient( X, dxo, 1, dx, omega0, X, dxo, funonly=.true.)
  
      if( input%properties%wannier%grouparray( wf_group)%group%writeconv) then
        call getunit( convun)
        write( convfname, '("opfloc_conv_",i3.3,".dat")'), wf_group
        open( convun, file=trim( convfname), action='write', form='formatted')
      end if
!#ifdef USEOMP
!!$omp critical
!#endif
      !call stiefel_optim_setexpconv( 10, 10, 1.d-2)
      if( input%properties%wannier%grouparray( wf_group)%group%optim .eq. 'cg') then
        !call stiefel_optim_cg( X, dxo, 1, dx, &
        !       wfopf_omega, wfopf_gradient, &
        !       tolgrad=eps, miniter=minit, maxiter=maxit, outunit=convun, &
        !       minstep=input%properties%wannier%grouparray( wf_group)%group%minstep)
        call manopt_stiefel_cg( X, dxo, 1, dx, &
               cost=wfopf_omega, grad=wfopf_gradient, &
               epsgrad=eps, minit=minit, maxit=maxit, stdout=convun, &
               minstep=input%properties%wannier%grouparray( wf_group)%group%minstep)
      else
        if( .false.) then
        call stiefel_optim_lbfgs( X, dxo, 1, dx, &
               wfopf_omega, wfopf_gradient, &
               tolgrad=eps, miniter=minit, maxiter=maxit, outunit=convun, &
               memory=input%properties%wannier%grouparray( wf_group)%group%memlen, &
               minstep=input%properties%wannier%grouparray( wf_group)%group%minstep)
        else
          call manopt_stiefel_lbfgs( X, dxo, 1, dx, &
               cost=wfopf_omega, &
               grad=wfopf_gradient, &
               !costgrad=wfopf_omegagradient, &
               epsgrad=eps, minit=minit, maxit=maxit, stdout=convun, &
               memlen=input%properties%wannier%grouparray( wf_group)%group%memlen, &
               minstep=input%properties%wannier%grouparray( wf_group)%group%minstep)
        end if
      end if
!#ifdef USEOMP
!!$omp end critical
!#endif
      if( input%properties%wannier%grouparray( wf_group)%group%writeconv) close( convun)

      return
    end subroutine wfopf_gen_g

    subroutine wfopf_gen_l( minit, maxit, X, eps)
      integer, intent( in)       :: minit
      integer, intent( inout)    :: maxit
      complex(8), intent( inout) :: X(P,P)
      real(8), intent( inout)    :: eps

      integer :: it
      real(8) :: eps0, lag, hist( minit)

      eps0 = eps
      hist = 0.d0
      it = 0

      do while( (it .lt. minit) .or. (eps .gt. eps0))
        it = it + 1
        if( it .gt. maxit) exit

        call wfopf_iter( NY, J, P, t, Y, X, lag)

        ! convergence analysis
        if( it .eq. 1) hist(:) = lag
        hist = cshift( hist, -1)
        hist(1) = lag
        if( it .gt. 1) then
          eps = abs( hist(2)/hist(1) - 1.d0)
        else
          eps = 1.d0
        end if
      end do
      maxit = it

      return
    end subroutine wfopf_gen_l

    subroutine wfopf_init
      integer :: ik, idxn, i

      real(8), allocatable :: val(:)
      complex(8), allocatable :: AA(:,:), lvec(:,:), rvec(:,:), UA(:,:,:)

      ! copy original transformation matrices
      U0 = wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                         wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, :)

      ! build projection matrices
      do ik = 1, wf_kset%nkpt
        if( sub) then
          call zgemm( 'c', 'n', J, P, N0, zone, &
                 U0(1,1,ik), N0, &
                 wf_groups( wf_group)%projection(:,:,ik), N0, zzero, &
                 A(1,1,ik), N)
        else
          A(:,:,ik) = wf_groups( wf_group)%projection(:,:,ik)
        end if
      end do

      ! initialize OPF matrix
      allocate( AA(P,P), val(P), lvec(P,P))
      AA = zzero
      do ik = 1, wf_kset%nkpt
        call zgemm( 'c', 'n', P, P, N, zone, A(1,1,ik), N, A(1,1,ik), N, zone, AA, P)
      end do
      AA = AA/dble( wf_kset%nkpt)
      call zhediag( AA, val, evec=lvec)
      do ik = 1, J
        OPF(:,ik,1) = lvec(:,P-ik+1)
      end do
      deallocate( AA, val, lvec)

      ! generate X matrices
      allocate( UA(N,P,wf_kset%nkpt), val(min(N,P)), lvec(N,N), rvec(P,P))
      if( sub) call wfomega_m
      do ik = 1, wf_kset%nkpt
        call zsvd( A(:,:,ik), val, lvec, rvec)
        call zgemm( 'n', 'n', N, P, min(N,P), zone, lvec, N, rvec, P, zzero, UA(1,1,ik), N)
      end do
      deallocate( val, lvec, rvec)
      allocate( lvec(N,P))
      i = 0
      ! enlarged M matrices
      do ik = 1, wf_kset%nkpt
        do idxn = 1, wf_n_ntot
          i = i + 1
          if( sub) then
            call zgemm( 'n', 'n', N, P, N, zone, &
                   wf_m( wf_groups( wf_group)%fwf, wf_groups( wf_group)%fwf, ik, idxn), wf_nwf, &
                   UA(1,1,wf_n_ik( idxn, ik)), N, zzero, &
                   lvec, N)
          else
            call zgemm( 'n', 'n', N, P, N, zone, &
                   wf_m0( wf_groups( wf_group)%fst, wf_groups( wf_group)%fst, ik, idxn), wf_nst, &
                   UA(1,1,wf_n_ik( idxn, ik)), N, zzero, &
                   lvec, N)
          end if
          call zgemm( 'c', 'n', P, P, N, zone, UA(1,1,ik), N, lvec, N, zzero, Y(1,1,i), P)
          t(i) = -2.d0*wf_n_wgt( idxn)/wf_kset%nkpt
        end do
      end do
      deallocate( UA, lvec)
      ! constraint matrices
      do ik = 1, wf_kset%nkpt
        i = i + 1
        call zgemm( 'c', 'n', P, P, N, zone, A(1,1,ik), N, A(1,1,ik), N, zzero, Y(1,1,i), P)
        do idxn = 1, P
          Y(idxn,idxn,i) = Y(idxn,idxn,i) - zone
        end do
        t(i) = lambda/wf_kset%nkpt
      end do

    end subroutine wfopf_init

    subroutine wfopf_omega( X, dxo, kx, dx, omega)
      integer, intent( in)    :: dxo(2), kx, dx(2,kx)
      complex(8), intent( in) :: X(dxo(1),dxo(2),*)
      real(8), intent( out)   :: omega

      integer :: ik, K

      real(8), allocatable :: s(:)
      complex(8), allocatable :: U(:,:), V(:,:), W(:,:)

!#ifdef USEOMP
!!$omp parallel default( shared) private( ik, s, V, W, U, K)
!#endif
      allocate( s(J), V(N,J), W(J,J), U(N,J))
!#ifdef USEOMP
!!$omp do
!#endif

      do ik = 1, wf_kset%nkpt
        call wfopf_X2U( X, dxo, dx(:,1), &
               A(1,1,ik), (/N,P/), &
               U, (/N,J/), &
               s, V, W, K)
        if( sub) then
          call zgemm( 'n', 'n', N0, J, J, zone, &
                 U0(1,1,ik), N0, &
                 U, N, zzero, &
                 wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, ik), wf_nst)
        else
          wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                        wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, ik) = U
        end if
      end do
!#ifdef USEOMP
!!$omp end do
!#endif
      deallocate( s, U, V, W)
!#ifdef USEOMP
!!$omp end parallel
!#endif
      call wfomega_gen( totonly=.true.)
      omega = sum( wf_omega( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))
      return
    end subroutine wfopf_omega

    subroutine wfopf_gradient( X, dxo, kx, dx, GX, dgo)
      integer, intent( in)     :: dxo(2), kx, dx(2,kx), dgo(2)
      complex(8), intent( in)  :: X(dxo(1),dxo(2),*)
      complex(8), intent( out) :: GX(dgo(1),dgo(2),*)

      integer :: ik, i, K
      real(8), allocatable :: s(:), F(:,:)
      complex(8), allocatable :: G(:,:), U(:,:), V(:,:), W(:,:), GU0(:,:), GU(:,:), AV(:,:), VGUW(:,:)
      complex(8), allocatable :: CNN(:,:), CJJ1(:,:), CJJ2(:,:), CNJ1(:,:), CNJ2(:,:)

      complex(8), external :: zdotc

      allocate( G(P,J))
      G = zzero
!#ifdef USEOMP
!!$omp parallel default( shared) private( ik, s, U, F, V, W, GU0, GU, AV, VGUW, CNN, CJJ1, CJJ2, CNJ1, CNJ2) reduction(+:G)
!#endif
      allocate( s(J), U(N,J), F(J,J), V(N,J), W(J,J), GU0(N0,J), GU(N,J), AV(P,J), VGUW(J,J))
      allocate( CNN(N,N), CJJ1(J,J), CJJ2(J,J), CNJ1(N,J), CNJ2(N,J))
!#ifdef USEOMP
!!$omp do
!#endif
      do ik = 1, wf_kset%nkpt
        call wfopf_X2U( X, dxo, dx(:,1), &
               A(1,1,ik), (/N,P/), &
               U, (/N,J/), &
               s, V, W, K)
        if( sub) then
          call zgemm( 'n', 'n', N0, J, J, zone, &
                 U0(1,1,ik), N0, &
                 U, N, zzero, &
                 wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, ik), wf_nst)
        else
          wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                        wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, ik) = U
        end if

        call wfomega_gradu( ik, GU0, N0)
        if( sub) then
          call zgemm( 'c', 'n', N, J, N0, zone, U0(1,1,ik), N0, GU0, N0, zzero, GU, N)
        else
          GU = GU0
        end if
        call wfopf_getF( s, J, K, F)

        call zgemm( 'c', 'n', J, J, N, zone, V, N, GU, N, zzero, CJJ1, J)
        call zgemm( 'n', 'n', J, J, J, zone, CJJ1, J, W, J, zzero, VGUW, J)
        call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, V, N, zzero, AV, P)

        CJJ1 = cmplx( F, 0.d0, 8)*VGUW
        CJJ1 = CJJ1 - conjg( transpose( CJJ1))
        call zgemm( 'n', 'c', J, J, J, zone, CJJ1, J, W, J, zzero, CJJ2, J)
        call zgemm( 'n', 'n', P, J, J, zone, AV, P, CJJ2, J, zone, G, P)

        if( J .lt. N) then
          CJJ1 = zzero
          do i = 1, K
            CJJ1(:,i) = W(:,i)/cmplx( s(i), 0.d0, 8)
          end do
          call zgemm( 'n', 'c', J, J, J, zone, CJJ1, J, W, J, zzero, CJJ2, J)
          call zgemm( 'n', 'n', N, J, J, zone, GU, N, CJJ2, J, zzero, CNJ1, N)
          call zgemm( 'n', 'c', N, N, J, -zone, V, N, V, N, zzero, CNN, N)
          do i = 1, N
            CNN(i,i) = CNN(i,i) + zone
          end do
          call zgemm( 'n', 'n', N, J, N, zone, CNN, N, CNJ1, N, zzero, CNJ2, N)
          call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, CNJ2, N, zone, G, P)
        end if

        if( J .lt. J) then
          CNJ1 = zzero
          do i = 1, K
            CNJ1(:,i) = V(:,i)/cmplx( s(i), 0.d0, 8)
          end do
          call zgemm( 'n', 'c', N, N, J, zone, CNJ1, N, V, N, zzero, CNN, N)
          call zgemm( 'n', 'n', N, J, J, zone, CNN, N, GU, N, zzero, CNJ1, N)
          call zgemm( 'n', 'c', J, J, J, -zone, W, J, W, J, zzero, CJJ1, J)
          do i = 1, J
            CJJ1(i,i) = CJJ1(i,i) + zone
          end do
          call zgemm( 'n', 'n', N, J, J, zone, CNJ1, N, CJJ1, J, zzero, CNJ2, N)
          call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, CNJ2, N, zone, G, P)
        end if
      end do
!#ifdef USEOMP
!!$omp end do
!#endif
      deallocate( s, F, V, W, U, GU, AV, VGUW)
      deallocate( CNN, CJJ1, CJJ2, CNJ1, CNJ2)
!#ifdef USEOMP
!!$omp end parallel
!#endif
      GX(1:P,1:J,1) = G
      deallocate( G)

      return
    end subroutine wfopf_gradient

    subroutine wfopf_omegagradient( X, dxo, kx, dx, omega, GX, dgo, funonly)
      integer, intent( in)       :: dxo(2), kx, dx(2,kx), dgo(2)
      complex(8), intent( in)    :: X(dxo(1),dxo(2),*)
      real(8), intent( out)      :: omega
      complex(8), intent( inout) :: GX(dgo(1),dgo(2),*)
      logical, optional, intent( in) :: funonly

      integer :: ik, i, K
      logical :: dograd
      real(8), allocatable :: s(:), F(:,:)
      complex(8), allocatable :: G(:,:), U(:,:), V(:,:), W(:,:), GU0(:,:), GU(:,:), AV(:,:), VGUW(:,:)
      complex(8), allocatable :: CNN(:,:), CJJ1(:,:), CJJ2(:,:), CNJ1(:,:), CNJ2(:,:)

      complex(8), external :: zdotc

      dograd = .true.
      if( present( funonly)) dograd = .not. funonly

!#ifdef USEOMP
!!$omp parallel default( shared) private( ik, s, U, F, V, W, GU0, GU, AV, VGUW, CNN, CJJ1, CJJ2, CNJ1, CNJ2) reduction(+:G)
!#endif
      allocate( s(J), U(N,J), V(N,J), W(J,J))
      if( dograd) then
        allocate( G(P,J), F(J,J), GU0(N0,J), GU(N,J), AV(P,J), VGUW(J,J))
        allocate( CNN(N,N), CJJ1(J,J), CJJ2(J,J), CNJ1(N,J), CNJ2(N,J))
        G = zzero
      end if
!#ifdef USEOMP
!!$omp do
!#endif
      do ik = 1, wf_kset%nkpt
        call wfopf_X2U( X, dxo, dx(:,1), &
               A(1,1,ik), (/N,P/), &
               U, (/N,J/), &
               s, V, W, K)
        if( sub) then
          call zgemm( 'n', 'n', N0, J, J, zone, &
                 U0(1,1,ik), N0, &
                 U, N, zzero, &
                 wf_transform( wf_groups( wf_group)%fst, wf_groups( wf_group)%fwf, ik), wf_nst)
        else
          wf_transform( wf_groups( wf_group)%fst:wf_groups( wf_group)%lst, &
                        wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf, ik) = U
        end if

        if( dograd) then
          call wfomega_gradu( ik, GU0, N0)
          if( sub) then
            call zgemm( 'c', 'n', N, J, N0, zone, U0(1,1,ik), N0, GU0, N0, zzero, GU, N)
          else
            GU = GU0
          end if
          call wfopf_getF( s, J, K, F)

          call zgemm( 'c', 'n', J, J, N, zone, V, N, GU, N, zzero, CJJ1, J)
          call zgemm( 'n', 'n', J, J, J, zone, CJJ1, J, W, J, zzero, VGUW, J)
          call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, V, N, zzero, AV, P)

          CJJ1 = cmplx( F, 0.d0, 8)*VGUW
          CJJ1 = CJJ1 - conjg( transpose( CJJ1))
          call zgemm( 'n', 'c', J, J, J, zone, CJJ1, J, W, J, zzero, CJJ2, J)
          call zgemm( 'n', 'n', P, J, J, zone, AV, P, CJJ2, J, zone, G, P)

          if( J .lt. N) then
            CJJ1 = zzero
            do i = 1, K
              CJJ1(:,i) = W(:,i)/cmplx( s(i), 0.d0, 8)
            end do
            call zgemm( 'n', 'c', J, J, J, zone, CJJ1, J, W, J, zzero, CJJ2, J)
            call zgemm( 'n', 'n', N, J, J, zone, GU, N, CJJ2, J, zzero, CNJ1, N)
            call zgemm( 'n', 'c', N, N, J, -zone, V, N, V, N, zzero, CNN, N)
            do i = 1, N
              CNN(i,i) = CNN(i,i) + zone
            end do
            call zgemm( 'n', 'n', N, J, N, zone, CNN, N, CNJ1, N, zzero, CNJ2, N)
            call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, CNJ2, N, zone, G, P)
          end if

          if( J .lt. J) then
            CNJ1 = zzero
            do i = 1, K
              CNJ1(:,i) = V(:,i)/cmplx( s(i), 0.d0, 8)
            end do
            call zgemm( 'n', 'c', N, N, J, zone, CNJ1, N, V, N, zzero, CNN, N)
            call zgemm( 'n', 'n', N, J, J, zone, CNN, N, GU, N, zzero, CNJ1, N)
            call zgemm( 'n', 'c', J, J, J, -zone, W, J, W, J, zzero, CJJ1, J)
            do i = 1, J
              CJJ1(i,i) = CJJ1(i,i) + zone
            end do
            call zgemm( 'n', 'n', N, J, J, zone, CNJ1, N, CJJ1, J, zzero, CNJ2, N)
            call zgemm( 'c', 'n', P, J, N, zone, A(1,1,ik), N, CNJ2, N, zone, G, P)
          end if
        end if
      end do
!#ifdef USEOMP
!!$omp end do
!#endif
      deallocate( s, V, W, U)
      if( dograd) then
        GX(1:P,1:J,1) = G
        deallocate( G, F, GU, AV, VGUW)
        deallocate( CNN, CJJ1, CJJ2, CNJ1, CNJ2)
      end if
!#ifdef USEOMP
!!$omp end parallel
!#endif
      call wfomega_gen( totonly=.true.)
      omega = sum( wf_omega( wf_groups( wf_group)%fwf:wf_groups( wf_group)%lwf))

      return
    end subroutine wfopf_omegagradient

    subroutine wfopf_X2U( X, dxo, dx, A, da, U, du, s, V, W, K)
      integer, intent( in)     :: dxo(2), dx(2), da(2), du(2)
      complex(8), intent( in)  :: X(dxo(1),dxo(2))
      complex(8), intent( in)  :: A(da(1),da(2))
      complex(8), intent( out) :: U(du(1),du(2))
      real(8), intent( out)    :: s(du(2))
      complex(8), intent( out) :: V(du(1),du(2)), W(du(2),du(2))
      integer, intent( out)    :: K

      integer :: i
      real(8) :: maxs
      complex(8), allocatable :: AX(:,:), tmp(:,:)

      allocate( AX( da(1), du(2)))
      allocate( tmp( da(1), da(1)))
      call zgemm( 'n', 'n', da(1), du(2), dx(1), zone, A, da(1), X, dxo(1), zzero, AX, da(1))
      call zsvd( AX, s, tmp, W)
      maxs = maxval( s)
      V = tmp( 1:N, 1:J)
      W = conjg( transpose( W))
      do K = J, 1, -1
        if( s(K) .gt. epssvd*maxs) exit
        s(K) = 0.d0
        !V(:,K) = zzero
        !W(:,K) = zzero
      end do
      call zgemm( 'n', 'c', du(1), du(2), J, zone, V, du(1), W, du(2), zzero, U, du(1))
      deallocate( AX, tmp)
      return
    end subroutine wfopf_X2U

    subroutine wfopf_getF( s, J, K, F)
      integer, intent( in)  :: J, K
      real(8), intent( in)  :: s(J)
      real(8), intent( out) :: F(J,J)

      integer :: m, n
      real(8) :: maxs
       
      F = 0.d0
      maxs = maxval( s)
      do m = 1, K
        do n = m, K
          if( (abs( s(m) - s(n)) .le. epssvd*maxs) .and. &
              (s(m) + s(n) .gt. epssvd*maxs)) then
            F(m,n) = 1.d0/(s(m) + s(n))
          else
            F(m,n) = (s(n) - s(m))/(s(n)*s(n) - s(m)*s(m))
          end if
          F(n,m) = F(m,n)
        end do
      end do
      return
    end subroutine wfopf_getF

    subroutine wfopf_write_opf( un)
      integer, intent( in) :: un

      integer :: i, j, n

      if( wf_group .eq. 1) then
        n = 0
        do i = 1, wf_ngroups
          n = max( n, wf_groups( i)%nproj)
        end do
        call printbox( un, '*', "Optimized projection functions")
        write( un, *)
        write( un, '(80("-"))')
        write( un, '(7x,"#",4x)', advance='no')
        do i = 1, n
          write( un, '(i3,2x)', advance='no') i
        end do
        write( un, *)
        write( un, '(80("-"))')
      end if
      do i = 1, wf_groups( wf_group)%nwf
        write( un, '(4x,i4,4x)', advance='no') i
        do j = 1, wf_groups( wf_group)%nproj
          write( un, '(i3,2x)', advance='no') nint( 1.d2*abs( wf_opf(j,i))**2)
        end do
        write( un, *)
      end do
      write( un, '(80("-"))')
      if( wf_group .eq. wf_ngroups) write( un, *)
      return
    end subroutine wfopf_write_opf

    ! performs a single iteration of the minimization of the OPF Lagrangian
    subroutine wfopf_iter( K, N, M, t, X, W, L)
      integer, intent( in)       :: K         ! number of X matrices
      integer, intent( in)       :: N         ! number of OPFs
      integer, intent( in)       :: M         ! number of trial orbitals
      real(8), intent( in)       :: t(K)      ! weights t
      complex(8), intent( in)    :: X(M,M,K)  ! matrices X
      complex(8), intent( inout) :: W(M,M)    ! unitary matrix W
      real(8), intent( out)      :: L         ! Lagrangian

      real(8), parameter :: eps = 1.d-6

      integer :: i, j, ix
      real(8) :: q(3,3), p(3,1), sol(3), reval(3), revec(3,3), big(6,6), aux(3,3), evalmin
      real(8) :: r, tp(2), theta, phi
      complex(8) :: XX(2,2), XW(M,M), z(3,1), zeval(6), mx(2,2), gr(2,2), tmp

      complex(8), external :: zdotc
      
      do i = 1, N
        write(*,*) i, N
        do j = i+1, M
          q = 0.d0
          p = 0.d0
#ifdef USEOMP
!$omp parallel default( shared) private( ix, XW, XX, z), reduction(+:q,p)
!$omp do
#endif
          do ix = 1, K
            call zgemv( 'n', M, M, zone, X(1,1,ix), M, W(1,i), 1, zzero, XW(1,1), 1)
            call zgemv( 'n', M, M, zone, X(1,1,ix), M, W(1,j), 1, zzero, XW(1,2), 1)
            XX(1,1) = zdotc( M, W(1,i), 1, XW(1,1), 1)
            XX(1,2) = zdotc( M, W(1,i), 1, XW(1,2), 1)
            XX(2,1) = zdotc( M, W(1,j), 1, XW(1,1), 1)
            XX(2,2) = zdotc( M, W(1,j), 1, XW(1,2), 1)
            z(1,1) =    0.5d0*(XX(1,1) - XX(2,2))
            z(2,1) =   -0.5d0*(XX(1,2) + XX(2,1))
            z(3,1) = 0.5d0*zi*(XX(1,2) - XX(2,1))
            q = q + t(ix)*dble( matmul( z, transpose( conjg( z))))
            p = p + t(ix)*dble( conjg( XX(1,1) + XX(2,2))*z)
          end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif

          ! find solution of minimization
          call rsydiag( q, reval, evec=revec)
          if( j .le. N) then
            sol = revec(:,1)
            if( sol(1) .lt. 0.d0) sol = -sol
          else
            big = 0.d0
            big(1:3,1:3) = 2.d0*q
            big(1:3,4:6) = 0.25d0*matmul( p, transpose( p)) - matmul( q, q)
            big(4,1) = 1.d0
            big(5,2) = 1.d0
            big(6,3) = 1.d0
            call zgediag( cmplx( big, 0.d0, 8), zeval)
            evalmin = maxval( abs( zeval))
            do ix = 1, 6
              if( abs( aimag( zeval(ix))) .lt. eps) evalmin = min( evalmin, dble( zeval(ix)))
            end do
            do ix = 1, 3
              q(ix,ix) = q(ix,ix) - evalmin
            end do
            if( minval( reval - evalmin) .lt. eps) then
              call rpinv( q, aux)
              call r3mv( aux, -0.5d0*p(:,1), sol)
              call r3mv( q, sol, aux(:,1))
              if( (norm2( aux(:,1) + 0.5d0*p(:,1)) .ge. eps) .or. (norm2( sol) .gt. 1.d0)) then
                sol = 10.d0
              else
                sol = sol + revec(:,1)/norm2( revec(:,1))*sqrt( 1.d0 - norm2( sol))
              end if
            else
              call r3minv( q, aux)
              call r3mv( aux, -0.5d0*p(:,1), sol)
            end if
          end if

          ! set up the Given's rotation
          if( abs( 1.d0 - norm2( sol)) .le. eps) then
            call sphcrd( (/sol(2), sol(3), sol(1)/), r, tp)
            phi = tp(2)
            theta = tp(1)/2
            gr(1,1) = cmplx( cos( theta), 0, 8)
            gr(1,2) = exp( zi*phi)*sin( theta)
            gr(2,2) = gr(1,1)
            gr(2,1) = -conjg( gr(1,2))
            ! update unitary matrix
            do ix = 1, M
              tmp = W(ix,i)
              W(ix,i) = tmp*gr(1,1) + W(ix,j)*gr(2,1)
              W(ix,j) = W(ix,j)*gr(1,1) + tmp*gr(1,2)
            end do
          else
            !write(*,*) "Doh!"
          end if
        end do
      end do

      ! compute Lagrangian
      call wfopf_lagrangian( K, N, M, t, X, W, L)

      return
    end subroutine wfopf_iter

    subroutine wfopf_lagrangian( K, N, M, t, X, W, L)
      integer, intent( in)    :: K         ! number of X matrices
      integer, intent( in)    :: N         ! number of OPFs
      integer, intent( in)    :: M         ! number of trial orbitals
      real(8), intent( in)    :: t(K)      ! weights t
      complex(8), intent( in) :: X(M,M,K)  ! matrices X
      complex(8), intent( in) :: W(M,M)    ! unitary matrix W
      real(8), intent( out)   :: L         ! Lagrangian

      integer :: i, ix
      complex(8) :: z, XW(M,M)

      complex(8), external :: zdotc

      L = 0.d0
#ifdef USEOMP
!$omp parallel default( shared) private( ix, XW, z), reduction(+:L)
!$omp do
#endif
      do ix = 1, K
        call zgemm( 'n', 'n', M, M, M, zone, X(1,1,ix), M, W, M, zzero, XW, M)
        do i = 1, N
          z = zdotc( M, W(1,i), 1, XW(1,i), 1)
          L = L + t(ix)*dble( z*conjg( z))
        end do
      end do
#ifdef USEOMP
!$omp end do
!$omp end parallel
#endif
      
      return
    end subroutine wfopf_lagrangian

    subroutine wfopf_destroy
      if( allocated( OPF)) deallocate( OPF)
      if( allocated( U0)) deallocate( U0)
      if( allocated( A)) deallocate( A)
      if( allocated( t)) deallocate( t)
      if( allocated( Y)) deallocate( Y)
      if( allocated( dx)) deallocate( dx)
    end subroutine wfopf_destroy

end module mod_wannier_opf
