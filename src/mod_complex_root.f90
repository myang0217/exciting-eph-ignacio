module mod_complex_root
  use mod_triangulation

  implicit none

  type( triangulation)    :: croot_tri    ! triangulation of the search region
  complex(8), allocatable :: croot_f(:)   ! function values at vertices
  integer(2), allocatable :: croot_q(:)   ! quadrant of function value at each vertex
  integer, allocatable    :: croot_e(:)   ! candidate edges
  integer, allocatable    :: croot_r(:)   ! candidate region (given by faces)
  integer, allocatable    :: croot_b(:)   ! candidate region boundaries (given by edges)
  integer, allocatable    :: croot_l(:,:) ! candidate region loops (given by sequence of vertices)
  real(8)                 :: croot_emin   ! length of shortest candidate edge
  real(8)                 :: croot_emax   ! length of longest candidate edge
  real(8)                 :: croot_epsm   ! radius within which two vertices are considered equal
  real(8)                 :: croot_epsf   ! value below which the function is considered zero

  contains

    subroutine croot_destroy
      if( allocated( croot_f)) deallocate( croot_f)
      if( allocated( croot_q)) deallocate( croot_q)
      if( allocated( croot_e)) deallocate( croot_e)
      if( allocated( croot_r)) deallocate( croot_r)
      if( allocated( croot_b)) deallocate( croot_b)
      if( allocated( croot_l)) deallocate( croot_l)
      call triangulation_destroy( croot_tri)
    end subroutine croot_destroy

    subroutine croot_genmesh( xrange, yrange, space, pattern, center, eps)
      real(8), intent( in)                :: xrange(2), yrange(2), space
      character(*), optional, intent( in) :: pattern
      complex(8), optional, intent( in)   :: center
      real(8), optional, intent( in)      :: eps

      integer :: p, nv, n1, n2, i, j, n
      real(8) :: xmin, xmax, ymin, ymax, x, y, epsrand
      complex(8) :: c
      complex(8), allocatable :: verts(:)

      xmin = min( xrange(1), xrange(2))
      xmax = max( xrange(1), xrange(2))
      ymin = min( yrange(1), yrange(2))
      ymax = max( yrange(1), yrange(2))

      croot_epsm = 1.d-16
      if( present( eps)) croot_epsm = eps
      epsrand = max( 2.d0*croot_epsm, 1.d-3*space)

      ! pattern
      p = 1
      if( present( pattern)) then
        if( trim( pattern) .eq. 'honeycomb') then
          p = 1
        else if( trim( pattern) .eq. 'rectangular') then
          p = 2
        else if( trim( pattern) .eq. 'radial') then
          p = 3
          if( present( center)) then
            c = center
          else
            c = cmplx( 0.5d0*(xmin + xmax), 0.5d0*(ymin + ymax), 8)
          end if
        else
          p = 1
        end if
      end if

      ! HONEYCOMB
      if( p .eq. 1) then
        n1 = ceiling( (xmax - xmin)/space)
        n2 = nint( dble( 2*n1)*(ymax - ymin)/(xmax - xmin)/sqrt( 3.d0))
        if( modulo( n2, 2) .eq. 0) then
          nv = (n2/2 + 1)*(n1 + 1) + n2/2*(n1 + 2)
        else
          nv = (n2 + 1)/2*(2*n1 + 3)
        end if
        allocate( verts( nv))
        n = 0
        do j = 0, n2
          y = ymin + dble( j)*(ymax - ymin)/dble( n2)
          if( modulo( j, 2) .eq. 0) then
            do i = 0, n1
              x = xmin + dble( i)*(xmax - xmin)/dble( n1)
              n = n + 1
              verts( n) = cmplx( x, y, 8)
            end do
          else
            do i = 0, n1 + 1
              x = xmin + (dble( i) - 0.5d0)*(xmax - xmin)/dble( n1)
              x = min( xmax, max( xmin, x))
              n = n + 1
              verts( n) = cmplx( x, y, 8)
            end do
          end if
        end do
        call triangulation_gen( croot_tri, verts( 1:n), croot_epsm, shake=epsrand)
        deallocate( verts)
      ! RECTANGULAR
      else if( p .eq. 2) then
        n1 = ceiling( (xmax - xmin)/space)
        n2 = ceiling( (ymax - ymin)/space)
        nv = (n1+1)*(n2+1)
        allocate( verts( nv))
        n = 0
        do j = 0, n2
          y = ymin + dble( j)*(ymax - ymin)/dble( n2)
          do i = 0, n1
            x = xmin + dble( i)*(xmax - xmin)/dble( n1)
            n = n + 1
            verts( n) = cmplx( x, y, 8)
          end do
        end do
        call triangulation_gen( croot_tri, verts( 1:n), croot_epsm, shake=epsrand)
        deallocate( verts)
      end if
      return
    end subroutine croot_genmesh

    subroutine croot_quadrants
      integer :: i
      real(8) :: pihalf, arg

      pihalf = 2.d0*atan( 1.d0)

      if( allocated( croot_q)) deallocate( croot_q)
      allocate( croot_q( croot_tri%nv))
      croot_q = 0
      
      do i = 1, croot_tri%nv
        arg = atan2( aimag( croot_f( i)), dble( croot_f( i)))
        if( (arg .ge. 0.d0) .and. (arg .lt. pihalf)) then
          croot_q( i) = 1
        else if( (arg .ge. pihalf) .and. (arg .lt. 2.d0*pihalf)) then
          croot_q( i) = 2
        else if( (arg .eq. pihalf) .or. (arg .lt. -pihalf)) then
          croot_q( i) = 3
        else
          croot_q( i) = 4
        end if
      end do

      return
    end subroutine croot_quadrants

    subroutine croot_edges
      integer :: i, n
      integer, allocatable :: tmp(:)

      croot_emin = 1.d128
      croot_emax = 0.d0
      if( allocated( croot_e)) deallocate( croot_e)

      allocate( tmp( croot_tri%ne))
      n = 0
      do i = 1, croot_tri%ne
        ! dont add the edge when it is on the boundary of the search region
        if( min( croot_tri%e2f( 1, i), croot_tri%e2f( 2, i)) .eq. 0) cycle
        if( abs( croot_q( croot_tri%edges( 1, i)) - croot_q( croot_tri%edges( 2, i))) .eq. 2) then
          n = n + 1
          tmp( n) = i
          croot_emin = min( croot_emin, abs( croot_tri%verts( croot_tri%edges( 2, i)) - croot_tri%verts( croot_tri%edges( 1, i))))
          croot_emax = max( croot_emax, abs( croot_tri%verts( croot_tri%edges( 2, i)) - croot_tri%verts( croot_tri%edges( 1, i))))
        end if
      end do

      if( n .gt. 0) then
        allocate( croot_e( n))
        croot_e = tmp( 1:n)
      end if
      
      deallocate( tmp)
      return
    end subroutine croot_edges

    subroutine croot_regions
      integer :: i, j, k, nf, ne, nr, neir, jf, je, jeif
      complex(8) :: z
      integer, allocatable :: tmp1(:), tmp2(:,:)
      integer, allocatable :: edges(:)

      if( allocated( croot_r)) deallocate( croot_r)
      if( allocated( croot_b)) deallocate( croot_b)
      if( allocated( croot_l)) deallocate( croot_l)

      ! find all faces belonging to the candidate regions
      allocate( tmp1( croot_tri%nf))
      tmp1 = 0
      tmp1(1) = croot_tri%e2f( 1, croot_e(1))
      tmp1(2) = croot_tri%e2f( 2, croot_e(1))
      nf = 2
      do i = 2, size( croot_e, 1)
        if( .not. any( tmp1( 1:nf) .eq. croot_tri%e2f( 1, croot_e( i)))) then
          nf = nf + 1
          tmp1( nf) = croot_tri%e2f( 1, croot_e( i))
        end if
        if( .not. any( tmp1( 1:nf) .eq. croot_tri%e2f( 2, croot_e( i)))) then
          nf = nf + 1
          tmp1( nf) = croot_tri%e2f( 2, croot_e( i))
        end if
      end do
      ! extend the candidate region by ill faces
      !nr = nf
      !do i = 1, nr
      !  do j = 1, 3
      !    je = croot_tri%f2e( j, tmp1( i))
      !    if( .not. any( tmp1 .eq. croot_tri%e2f( 1, je))) then
      !      jf = croot_tri%e2f( 1, je)
      !    else if( .not. any( tmp1 .eq. croot_tri%e2f( 2, je))) then
      !      jf = croot_tri%e2f( 2, je)
      !    else
      !      cycle
      !    end if
      !    nf = nf + 1
      !    tmp1( nf) = jf
      !    !do k = 1, 3
      !    !  z = croot_tri%verts( croot_tri%faces( k, jf))/croot_tri%verts( croot_tri%faces( modulo( k, 3) + 1, jf))
      !    !  if( abs( atan2( aimag( z), dble( z))) .lt. 0.2d0) then
      !    !    nf = nf + 1
      !    !    tmp1( nf) = jf
      !    !    exit
      !    !  end if
      !    !end do
      !  end do
      !end do

      allocate( croot_r( nf))
      croot_r = tmp1( 1:nf)
      deallocate( tmp1)

      ! find all edges forming the boundaries of the canditate regions
      allocate( tmp1( 3*nf))
      do i = 1, nf
        tmp1( 3*(i-1)+1) = croot_tri%f2e( 1, croot_r( i)) 
        tmp1( 3*(i-1)+2) = croot_tri%f2e( 2, croot_r( i)) 
        tmp1( 3*(i-1)+3) = croot_tri%f2e( 3, croot_r( i)) 
      end do
      ne = 3*nf
      do i = 2, 3*nf
        if( any( tmp1( 1:(i-1)) .eq. tmp1( i))) then
          tmp1( minloc( abs( tmp1( 1:(i-1)) - tmp1( i)), 1)) = 0
          tmp1( i) = 0
          ne = ne - 2
        end if
      end do
      allocate( croot_b( ne))
      ne = 0
      do i = 1, 3*nf
        if( tmp1( i) .gt. 0) then
          ne = ne + 1
          croot_b( ne) = tmp1( i)
        end if
      end do
      deallocate( tmp1)
      allocate( tmp1( ne))
      tmp1 = croot_b

      ! split the set of edges into individual boundaries
      allocate( tmp2( ne+1, size( croot_e, 1)))
      tmp2 = 0
      nr = 1      ! #regions
      neir = 1    ! #edges in region
      je = 1      ! current boundary edge
      do while( sum( tmp1) .gt. 0)
        ! find face of edge that belongs to candidate regions
        if( any( croot_r .eq. croot_tri%e2f( 1, tmp1( je)))) then
          jf = croot_tri%e2f( 1, tmp1( je))
        else
          jf = croot_tri%e2f( 2, tmp1( je))
        end if
        ! find number of edge within this face and add next vertex
        jeif = minloc( abs( croot_tri%f2e( :, jf) - tmp1( je)), 1)
        if( jeif .eq. 1) then
          if( neir .eq. 1) tmp2( neir, nr) = croot_tri%faces( 2, jf)
          neir = neir + 1
          tmp2( neir, nr) = croot_tri%faces( 3, jf)
        else if( jeif .eq. 2) then
          if( neir .eq. 1) tmp2( neir, nr) = croot_tri%faces( 3, jf)
          neir = neir + 1
          tmp2( neir, nr) = croot_tri%faces( 1, jf)
        else
          if( neir .eq. 1) tmp2( neir, nr) = croot_tri%faces( 1, jf)
          neir = neir + 1
          tmp2( neir, nr) = croot_tri%faces( 2, jf)
        end if
        ! strike edge from list
        tmp1( je) = 0
        ! check if region was closed
        if( tmp2( neir, nr) .eq. tmp2( 1, nr)) then
          nr = nr + 1
          neir = 1
          do je = 1, ne
            if( tmp1( je) .gt. 0) exit
          end do
        else
          ! find next edge in the face
          jeif = modulo( jeif, 3) + 1
          ! check if edge belongs to boundary
          if( any( tmp1 .eq. croot_tri%f2e( jeif, jf))) then
            ! edge belongs to boundary
            je = minloc( abs( tmp1 - croot_tri%f2e( jeif, jf)), 1)
          else
            ! edge does not belong to boundary
            je = croot_tri%f2e( jeif, jf)
            do while( .not. any( tmp1 .eq. je))
              ! switch to other face belonging to this edge
              if( croot_tri%e2f( 1, je) .eq. jf) then
                jf = croot_tri%e2f( 2, je)
              else
                jf = croot_tri%e2f( 1, je)
              end if
              if( jf .eq. 0) write(*,*) je
              ! find number of edge within this face
              jeif = minloc( abs( croot_tri%f2e( :, jf) - je), 1)
              ! go to next edge
              jeif = modulo( jeif, 3) + 1
              je = croot_tri%f2e( jeif, jf)
            end do
            ! no we are back on the boundary
            je = minloc( abs( tmp1 - je), 1)
          end if
        end if
      end do

      neir = 0
      do i = 1, nr - 1
        do j = 1, ne + 1
          if( tmp2( j, i) .eq. 0) then
            exit
          else
            neir = max( neir, j)
          end if
        end do
      end do
     
      allocate( croot_l( neir, nr-1))
      croot_l = tmp2( 1:neir, 1:(nr-1))
      deallocate( tmp1, tmp2)

      return
    end subroutine croot_regions

    subroutine croot_refine
      integer :: i, j, k, jv1, jv2, je, jf, nv
      real(8) :: d1, d2
      complex(8) :: v
      complex(8), allocatable :: verts(:), vtmp(:)

      nv = 3*size( croot_r, 1) + size( croot_b, 1)
      allocate( verts( nv))
      verts = cmplx( 1.d128, 1.d128, 8)
      nv = 1
      ! loop over candidate region
      do i = 1, size( croot_r, 1)
        ! loop over edges
        do j = 1, 3
          je = j
          k = croot_tri%f2e( je, croot_r( i))
          v = 0.5d0*(croot_tri%verts( croot_tri%edges( 1, k)) + croot_tri%verts( croot_tri%edges( 2, k)))
          jv1 = minloc( abs( verts( 1:nv) - v), 1)
          ! add vertex
          if( abs( verts( jv1) - v) .gt. croot_epsm) then
            verts( nv) = v
            jv1 = nv
            nv = nv + 1
          end if
        end do
      end do
      nv = nv - 1

      ! loop over extended candidate region
      do i = 1, size( croot_b, 1)
        ! check if boundary segment is on boundary of search region (then skip)
        if( min( croot_tri%e2f( 1, croot_b( i)), croot_tri%e2f( 2, croot_b( i))) .eq. 0) cycle
        ! jf = face in extended region
        if( any( croot_r .eq. croot_tri%e2f( 1, croot_b( i)))) then
          jf = croot_tri%e2f( 2, croot_b( i))
        else
          jf = croot_tri%e2f( 1, croot_b( i))
        end if
        v = (croot_tri%verts( croot_tri%faces( 1, jf)) + &
             croot_tri%verts( croot_tri%faces( 2, jf)) + &
             croot_tri%verts( croot_tri%faces( 3, jf)))/3.0d0
        d1 = abs( croot_tri%verts( croot_tri%edges( 1, croot_tri%f2e( 1, jf))) - &
                  croot_tri%verts( croot_tri%edges( 2, croot_tri%f2e( 1, jf))))
        do j = 1, 3
          d2 = abs( croot_tri%verts( croot_tri%edges( 1, croot_tri%f2e( modulo( j, 3) + 1, jf))) - &
                    croot_tri%verts( croot_tri%edges( 2, croot_tri%f2e( modulo( j, 3) + 1, jf))))
          d1 = d1/d2
          if( (d1 .gt. 2.d0) .or. (d1 .lt. 0.5d0)) then
            nv = nv + 1
            verts( nv) = v
            exit
          end if
          d1 = d2
        end do
      end do

      ! update triangulation
      allocate( vtmp( croot_tri%nv + nv))
      vtmp( 1:croot_tri%nv) = croot_tri%verts
      vtmp( (croot_tri%nv+1):croot_tri%nv+nv) = verts( 1:nv)
      call triangulation_gen( croot_tri, vtmp, croot_epsm, shake=croot_epsm)
      deallocate( verts, vtmp)

      return
    endsubroutine croot_refine

    subroutine croot_verify( roots, mroots, nr, poles, mpoles, np, eps)
      complex(8), allocatable, intent( out) :: roots(:), poles(:)
      integer, allocatable, intent( out)    :: mroots(:), mpoles(:)
      integer, intent( out)                 :: nr, np
      real(8), optional, intent( in)        :: eps

      integer :: i, j, d, s, iv(3)
      complex(8) :: c, v(3), z, f(3), f2(3), f3
      real(8) :: w(3)
      integer, allocatable :: mrtmp(:), mptmp(:)
      complex(8), allocatable :: rtmp(:), ptmp(:)
      
      croot_epsf = 1.d-16
      if( present( eps)) croot_epsf = eps

      ! first we find all possible roots and poles
      allocate( mrtmp( size( croot_l, 2)))
      allocate( mptmp( size( croot_l, 2)))
      allocate( rtmp( size( croot_l, 2)))
      allocate( ptmp( size( croot_l, 2)))
      nr = 0
      np = 0
      outer: do i = 1, size( croot_l, 2)
        s = 0
        c = cmplx( 0.d0, 0.d0, 8)
        do j = 1, size( croot_l, 1) - 1
          if( croot_l( j+1, i) .eq. 0) exit
          d = croot_q( croot_l( j+1, i)) - croot_q( croot_l( j, i))
          if( d .eq. 3) then
            d = -1
          else if( d .eq. -3) then
            d = 1
          else if( abs( d) .eq. 2) then
            write(*,*)
            write(*,'("Error (croot_verify): Boundary segment of candidate region phase change of two quadrants encountered.")')
            cycle outer
          end if
          s = s + d
          c = c + croot_tri%verts( croot_l( j, i))
        end do
        if( s .gt. 0) then
          nr = nr + 1
          rtmp( nr) = c/dble( j-1)
          mrtmp( nr) = s/4
        else if( s .lt. 0) then
          np = np + 1
          ptmp( np) = c/dble( j-1)
          mptmp( np) = -s/4
        end if
      end do outer

      if( allocated( roots)) deallocate( roots)
      allocate( roots( nr))
      roots = rtmp( 1:nr)
      if( allocated( mroots)) deallocate( mroots)
      allocate( mroots( nr))
      mroots = mrtmp( 1:nr)
      if( allocated( poles)) deallocate( poles)
      allocate( poles( np))
      poles = ptmp( 1:np)
      if( allocated( mpoles)) deallocate( mpoles)
      allocate( mpoles( np))
      mpoles = mptmp( 1:np)

      deallocate( rtmp, ptmp, mrtmp, mptmp)

      return
    end subroutine croot_verify

    subroutine croot_tofile( fname)
      use m_getunit
      character(*), intent( in) :: fname

      integer :: un, i, j
      
      call getunit( un)
      if( allocated( croot_e)) then
        open( un, file=trim( fname)//'.EDG', action='write', status='unknown', form='formatted')
        do i = 1, size( croot_e, 1)
          write( un, '(2i)') croot_tri%edges( :, croot_e( i))
        end do
        close( un)
      end if

      if( allocated( croot_l)) then
        open( un, file=trim( fname)//'.REG', action='write', status='unknown', form='formatted')
        do i = 1, size( croot_l, 2)
          do j = 1, size( croot_l, 1) - 1
            if( croot_l( j+1, i) .gt. 0) write( un, '(2i)') croot_l( j, i), croot_l( j+1, i)
          end do
          write( un, * )
          write( un, * )
        end do
        close( un)
      end if

      return
    end subroutine croot_tofile
end module
